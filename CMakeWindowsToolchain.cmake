
set(CMAKE_SYSTEM_NAME Windows)
set(CMAKE_SYSTEM_ARCHITECTURE x64)

set(CMAKE_C_COMPILER "C:/Program Files/Microsoft Visual Studio/2022/Community/VC/Tools/MSVC/14.33.31629/bin/Hostx64/x64/cl.exe")
set(CMAKE_CXX_COMPILER "C:/Program Files/Microsoft Visual Studio/2022/Community/VC/Tools/MSVC/14.33.31629/bin/Hostx64/x64/cl.exe")

include_directories(SYSTEM
    "C:/Program Files (x86)/Windows Kits/10/Include/10.0.19041.0/shared"
    "C:/Program Files (x86)/Windows Kits/10/Include/10.0.19041.0/um"
    "C:/Program Files (x86)/Windows Kits/10/Include/10.0.19041.0/ucrt"
    "C:/Program Files/Microsoft Visual Studio/2022/Community/VC/Tools/MSVC/14.33.31629/include"
)

# set(CMAKE_CXX_FLAGS -Wall
#                     -Wextra
#                     -pedantic
#                     -Wnull-dereference
#                     -Wconversion 
#                     -Wsign-conversion 
#                     -Wswitch-enum
#                     -Wshadow
#                     )