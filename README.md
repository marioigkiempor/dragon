# Dragon Programming Language

Dragon is a WIP experimental language.
It is inspired by C, C++ and Jai.
It is intended to be simple to use by being predictable and consistent, and by providing useful error messages.

## Getting Started

### Windows

This software currently _only_ supports WSL running Ubuntu 22.04 with clang-15 and Cmake.

With all these tools installed, build the compiler by running:
``` sh
mkdir build
cd build
cmake ..
cmake --build .
./build/dragon -i ../tests/hello_word.drag
```

### Linux

Assume the same instructions as Windows, since I'm developing on WSL.

