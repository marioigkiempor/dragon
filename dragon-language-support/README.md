# Dragon Language Support for VSCode

## Usage

In the extension's root folder:

```bash
$ npm install
```

To create the package:

```bash
$ npx vsce package
```

Then install in VSCode by switching to the Extensions section on the left, then the ... at the top middle and choose "Install from VSIX..." and choose the package you just created. (Copypasta from [Jakt](https://github.com/SerenityOS/jakt/tree/main/editors/vscode))