#include <stdlib.h>

#include <iostream>
#include <array>
#include <vector>
#include <variant> // for boxed enums.

#define SDL_MAIN_HANDLED
#include "SDL.h"

#include "../thirdparty/fmt/src/format.cc"
#include "../thirdparty/fmt/src/os.cc"

#include "fmt/format.h"
#include "fmt/color.h"
#include "fmt/ranges.h"
#include "fmt/os.h"

struct Type_Info {
    std::string name;
};

std::string make_string(char *buffer, size_t size) { return std::string(buffer, size); }
const char* cstr(const std::string &str) { return str.c_str(); }
template <typename T> auto array_push(std::vector<T>& array, T item) { array.push_back(item); }

struct bool_Type {
    Type_Info type_info;
};
bool_Type dragon_bool { .type_info = { .name = "bool" } };

struct f32_Type {
    Type_Info type_info;
};
f32_Type dragon_f32 { .type_info = { .name = "f32" } };

struct f64_Type {
    Type_Info type_info;
};
f64_Type dragon_f64 { .type_info = { .name = "f64" } };

struct pointer_Type {
    Type_Info type_info;
};
pointer_Type dragon_pointer { .type_info = { .name = "pointer" } };

struct s16_Type {
    Type_Info type_info;
};
s16_Type dragon_s16 { .type_info = { .name = "s16" } };

struct s32_Type {
    Type_Info type_info;
};
s32_Type dragon_s32 { .type_info = { .name = "s32" } };

struct s64_Type {
    Type_Info type_info;
};
s64_Type dragon_s64 { .type_info = { .name = "s64" } };

struct s8_Type {
    Type_Info type_info;
};
s8_Type dragon_s8 { .type_info = { .name = "s8" } };

struct string_Type {
    Type_Info type_info;
};
string_Type dragon_string { .type_info = { .name = "string" } };

struct type_info_Type {
    Type_Info type_info;
};
type_info_Type dragon_type_info { .type_info = { .name = "type_info" } };

struct u16_Type {
    Type_Info type_info;
};
u16_Type dragon_u16 { .type_info = { .name = "u16" } };

struct u32_Type {
    Type_Info type_info;
};
u32_Type dragon_u32 { .type_info = { .name = "u32" } };

struct u64_Type {
    Type_Info type_info;
};
u64_Type dragon_u64 { .type_info = { .name = "u64" } };

struct u8_Type {
    Type_Info type_info;
};
u8_Type dragon_u8 { .type_info = { .name = "u8" } };

struct void_Type {
    Type_Info type_info;
};
void_Type dragon_void { .type_info = { .name = "void" } };

enum class Compilation_Result {OK, COULDNT_OPEN_FILE, LEXING_FAILED, PARSING_FAILED, TYPECHECKING_FAILED, CPP_CODEGEN_FAILED, };
template <> struct fmt::formatter<Compilation_Result> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(const Compilation_Result &a, FormatContext &ctx) {
    switch(a) {
        case Compilation_Result::OK: fmt::format_to(ctx.out(), "Compilation_Result.OK"); break;
        case Compilation_Result::COULDNT_OPEN_FILE: fmt::format_to(ctx.out(), "Compilation_Result.COULDNT_OPEN_FILE"); break;
        case Compilation_Result::LEXING_FAILED: fmt::format_to(ctx.out(), "Compilation_Result.LEXING_FAILED"); break;
        case Compilation_Result::PARSING_FAILED: fmt::format_to(ctx.out(), "Compilation_Result.PARSING_FAILED"); break;
        case Compilation_Result::TYPECHECKING_FAILED: fmt::format_to(ctx.out(), "Compilation_Result.TYPECHECKING_FAILED"); break;
        case Compilation_Result::CPP_CODEGEN_FAILED: fmt::format_to(ctx.out(), "Compilation_Result.CPP_CODEGEN_FAILED"); break;
        default: break;
    }
    return ctx.out();
  }
};
enum class TokenKind {INVALID, END_OF_FILE, ALIAS, AS, DEFER, ELSE, ENUM, EXTERNAL, FOR, FALSE, IF, IS, INCLUDE, MATCH, RETURN, STRUCT, TRUE, WHILE, STRING_LITERAL, INTEGER_LITERAL, FLOAT_LITERAL, IDENTIFIER, OPEN_PAREN, CLOSE_PAREN, OPEN_CURLY, CLOSE_CURLY, OPEN_SQUARE, CLOSE_SQUARE, EQ, COMMA, SEMI, COLON, EQEQ, NOTEQ, LT, GT, LE, GE, ADD, SUB, MUL, DIV, MOD, DOT, DOTDOT, AND, ANDAND, OR, OROR, ADD_EQ, SUB_EQ, MUL_EQ, DIV_EQ, MOD_EQ, AND_EQ, OR_EQ, BANG, POINTER, DEREFERENCE, };
template <> struct fmt::formatter<TokenKind> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(const TokenKind &a, FormatContext &ctx) {
    switch(a) {
        case TokenKind::INVALID: fmt::format_to(ctx.out(), "TokenKind.INVALID"); break;
        case TokenKind::END_OF_FILE: fmt::format_to(ctx.out(), "TokenKind.END_OF_FILE"); break;
        case TokenKind::ALIAS: fmt::format_to(ctx.out(), "TokenKind.ALIAS"); break;
        case TokenKind::AS: fmt::format_to(ctx.out(), "TokenKind.AS"); break;
        case TokenKind::DEFER: fmt::format_to(ctx.out(), "TokenKind.DEFER"); break;
        case TokenKind::ELSE: fmt::format_to(ctx.out(), "TokenKind.ELSE"); break;
        case TokenKind::ENUM: fmt::format_to(ctx.out(), "TokenKind.ENUM"); break;
        case TokenKind::EXTERNAL: fmt::format_to(ctx.out(), "TokenKind.EXTERNAL"); break;
        case TokenKind::FOR: fmt::format_to(ctx.out(), "TokenKind.FOR"); break;
        case TokenKind::FALSE: fmt::format_to(ctx.out(), "TokenKind.FALSE"); break;
        case TokenKind::IF: fmt::format_to(ctx.out(), "TokenKind.IF"); break;
        case TokenKind::IS: fmt::format_to(ctx.out(), "TokenKind.IS"); break;
        case TokenKind::INCLUDE: fmt::format_to(ctx.out(), "TokenKind.INCLUDE"); break;
        case TokenKind::MATCH: fmt::format_to(ctx.out(), "TokenKind.MATCH"); break;
        case TokenKind::RETURN: fmt::format_to(ctx.out(), "TokenKind.RETURN"); break;
        case TokenKind::STRUCT: fmt::format_to(ctx.out(), "TokenKind.STRUCT"); break;
        case TokenKind::TRUE: fmt::format_to(ctx.out(), "TokenKind.TRUE"); break;
        case TokenKind::WHILE: fmt::format_to(ctx.out(), "TokenKind.WHILE"); break;
        case TokenKind::STRING_LITERAL: fmt::format_to(ctx.out(), "TokenKind.STRING_LITERAL"); break;
        case TokenKind::INTEGER_LITERAL: fmt::format_to(ctx.out(), "TokenKind.INTEGER_LITERAL"); break;
        case TokenKind::FLOAT_LITERAL: fmt::format_to(ctx.out(), "TokenKind.FLOAT_LITERAL"); break;
        case TokenKind::IDENTIFIER: fmt::format_to(ctx.out(), "TokenKind.IDENTIFIER"); break;
        case TokenKind::OPEN_PAREN: fmt::format_to(ctx.out(), "TokenKind.OPEN_PAREN"); break;
        case TokenKind::CLOSE_PAREN: fmt::format_to(ctx.out(), "TokenKind.CLOSE_PAREN"); break;
        case TokenKind::OPEN_CURLY: fmt::format_to(ctx.out(), "TokenKind.OPEN_CURLY"); break;
        case TokenKind::CLOSE_CURLY: fmt::format_to(ctx.out(), "TokenKind.CLOSE_CURLY"); break;
        case TokenKind::OPEN_SQUARE: fmt::format_to(ctx.out(), "TokenKind.OPEN_SQUARE"); break;
        case TokenKind::CLOSE_SQUARE: fmt::format_to(ctx.out(), "TokenKind.CLOSE_SQUARE"); break;
        case TokenKind::EQ: fmt::format_to(ctx.out(), "TokenKind.EQ"); break;
        case TokenKind::COMMA: fmt::format_to(ctx.out(), "TokenKind.COMMA"); break;
        case TokenKind::SEMI: fmt::format_to(ctx.out(), "TokenKind.SEMI"); break;
        case TokenKind::COLON: fmt::format_to(ctx.out(), "TokenKind.COLON"); break;
        case TokenKind::EQEQ: fmt::format_to(ctx.out(), "TokenKind.EQEQ"); break;
        case TokenKind::NOTEQ: fmt::format_to(ctx.out(), "TokenKind.NOTEQ"); break;
        case TokenKind::LT: fmt::format_to(ctx.out(), "TokenKind.LT"); break;
        case TokenKind::GT: fmt::format_to(ctx.out(), "TokenKind.GT"); break;
        case TokenKind::LE: fmt::format_to(ctx.out(), "TokenKind.LE"); break;
        case TokenKind::GE: fmt::format_to(ctx.out(), "TokenKind.GE"); break;
        case TokenKind::ADD: fmt::format_to(ctx.out(), "TokenKind.ADD"); break;
        case TokenKind::SUB: fmt::format_to(ctx.out(), "TokenKind.SUB"); break;
        case TokenKind::MUL: fmt::format_to(ctx.out(), "TokenKind.MUL"); break;
        case TokenKind::DIV: fmt::format_to(ctx.out(), "TokenKind.DIV"); break;
        case TokenKind::MOD: fmt::format_to(ctx.out(), "TokenKind.MOD"); break;
        case TokenKind::DOT: fmt::format_to(ctx.out(), "TokenKind.DOT"); break;
        case TokenKind::DOTDOT: fmt::format_to(ctx.out(), "TokenKind.DOTDOT"); break;
        case TokenKind::AND: fmt::format_to(ctx.out(), "TokenKind.AND"); break;
        case TokenKind::ANDAND: fmt::format_to(ctx.out(), "TokenKind.ANDAND"); break;
        case TokenKind::OR: fmt::format_to(ctx.out(), "TokenKind.OR"); break;
        case TokenKind::OROR: fmt::format_to(ctx.out(), "TokenKind.OROR"); break;
        case TokenKind::ADD_EQ: fmt::format_to(ctx.out(), "TokenKind.ADD_EQ"); break;
        case TokenKind::SUB_EQ: fmt::format_to(ctx.out(), "TokenKind.SUB_EQ"); break;
        case TokenKind::MUL_EQ: fmt::format_to(ctx.out(), "TokenKind.MUL_EQ"); break;
        case TokenKind::DIV_EQ: fmt::format_to(ctx.out(), "TokenKind.DIV_EQ"); break;
        case TokenKind::MOD_EQ: fmt::format_to(ctx.out(), "TokenKind.MOD_EQ"); break;
        case TokenKind::AND_EQ: fmt::format_to(ctx.out(), "TokenKind.AND_EQ"); break;
        case TokenKind::OR_EQ: fmt::format_to(ctx.out(), "TokenKind.OR_EQ"); break;
        case TokenKind::BANG: fmt::format_to(ctx.out(), "TokenKind.BANG"); break;
        case TokenKind::POINTER: fmt::format_to(ctx.out(), "TokenKind.POINTER"); break;
        case TokenKind::DEREFERENCE: fmt::format_to(ctx.out(), "TokenKind.DEREFERENCE"); break;
        default: break;
    }
    return ctx.out();
  }
};
enum class LexCode {OK, EMPTY_FILE, INVALID_TOKEN, };
template <> struct fmt::formatter<LexCode> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(const LexCode &a, FormatContext &ctx) {
    switch(a) {
        case LexCode::OK: fmt::format_to(ctx.out(), "LexCode.OK"); break;
        case LexCode::EMPTY_FILE: fmt::format_to(ctx.out(), "LexCode.EMPTY_FILE"); break;
        case LexCode::INVALID_TOKEN: fmt::format_to(ctx.out(), "LexCode.INVALID_TOKEN"); break;
        default: break;
    }
    return ctx.out();
  }
};
enum class ExpressionAstNode {};
template <> struct fmt::formatter<ExpressionAstNode> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(const ExpressionAstNode &a, FormatContext &ctx) {
    switch(a) {
        default: break;
    }
    return ctx.out();
  }
};
enum class StatementAstNode {};
template <> struct fmt::formatter<StatementAstNode> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(const StatementAstNode &a, FormatContext &ctx) {
    switch(a) {
        default: break;
    }
    return ctx.out();
  }
};
struct AstNode_Program { 
    std::vector<StatementAstNode> statements;
};
struct AstNode_Statement { 
    StatementAstNode statement;
};
struct AstNode_Expression { 
    ExpressionAstNode expression;
};
using AstNode = std::variant<AstNode_Program,AstNode_Statement,AstNode_Expression>;

template<> struct fmt::formatter<AstNode> {
    constexpr auto parse(fmt::format_parse_context& ctx) { return ctx.end(); }
    template<typename FormatContext> auto format(const AstNode& v, FormatContext& ctx) {
        if (auto* x = std::get_if<AstNode_Program>(&v)) { return fmt::format_to(ctx.out(), "AstNode.Program(statements = {})",x->statements); }
        if (auto* x = std::get_if<AstNode_Statement>(&v)) { return fmt::format_to(ctx.out(), "AstNode.Statement(statement = {})",x->statement); }
        if (auto* x = std::get_if<AstNode_Expression>(&v)) { return fmt::format_to(ctx.out(), "AstNode.Expression(expression = {})",x->expression); }
        return fmt::format_to(ctx.out(), "AstNode.INVALID");    };
};

enum class ParseCode {OK, Empty_Tokens, Had_Errors, Couldnt_Open_Included_File, };
template <> struct fmt::formatter<ParseCode> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(const ParseCode &a, FormatContext &ctx) {
    switch(a) {
        case ParseCode::OK: fmt::format_to(ctx.out(), "ParseCode.OK"); break;
        case ParseCode::Empty_Tokens: fmt::format_to(ctx.out(), "ParseCode.Empty_Tokens"); break;
        case ParseCode::Had_Errors: fmt::format_to(ctx.out(), "ParseCode.Had_Errors"); break;
        case ParseCode::Couldnt_Open_Included_File: fmt::format_to(ctx.out(), "ParseCode.Couldnt_Open_Included_File"); break;
        default: break;
    }
    return ctx.out();
  }
};
struct Compilation_Options {
    Type_Info type_info;
    bool only_lex;
    std::string input_filepath;
    std::string exe_output_filepath;
    std::string cpp_output_filepath;
    bool keep_cpp_file;
    bool keep_junk;
};

template <>
struct fmt::formatter<Compilation_Options> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(const Compilation_Options &a, FormatContext &ctx) const {
    fmt::format_to(ctx.out(), "Compilation_Options {{ ");
    fmt::format_to(ctx.out(), "only_lex = {}", a.only_lex);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "input_filepath = {}", a.input_filepath);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "exe_output_filepath = {}", a.exe_output_filepath);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "cpp_output_filepath = {}", a.cpp_output_filepath);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "keep_cpp_file = {}", a.keep_cpp_file);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "keep_junk = {}", a.keep_junk);
    fmt::format_to(ctx.out(), " }}");
    return ctx.out();
  }
};
struct FileLoc {
    Type_Info type_info;
    std::string path;
    uint64_t row;
    uint64_t col;
};

template <>
struct fmt::formatter<FileLoc> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(const FileLoc &a, FormatContext &ctx) const {
    fmt::format_to(ctx.out(), "FileLoc {{ ");
    fmt::format_to(ctx.out(), "path = {}", a.path);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "row = {}", a.row);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "col = {}", a.col);
    fmt::format_to(ctx.out(), " }}");
    return ctx.out();
  }
};
struct TokenInfo {
    Type_Info type_info;
    TokenKind kind;
    uint32_t flags;
    std::string name;
};

template <>
struct fmt::formatter<TokenInfo> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(const TokenInfo &a, FormatContext &ctx) const {
    fmt::format_to(ctx.out(), "TokenInfo {{ ");
    fmt::format_to(ctx.out(), "kind = {}", a.kind);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "flags = {}", a.flags);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "name = {}", a.name);
    fmt::format_to(ctx.out(), " }}");
    return ctx.out();
  }
};
struct Token {
    Type_Info type_info;
    FileLoc file_loc;
    TokenInfo info;
    std::string contents;
};

template <>
struct fmt::formatter<Token> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(const Token &a, FormatContext &ctx) const {
    fmt::format_to(ctx.out(), "Token {{ ");
    fmt::format_to(ctx.out(), "file_loc = {}", a.file_loc);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "info = {}", a.info);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "contents = {}", a.contents);
    fmt::format_to(ctx.out(), " }}");
    return ctx.out();
  }
};
struct LexResult {
    Type_Info type_info;
    LexCode code;
    std::vector<Token> tokens;
};

template <>
struct fmt::formatter<LexResult> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(const LexResult &a, FormatContext &ctx) const {
    fmt::format_to(ctx.out(), "LexResult {{ ");
    fmt::format_to(ctx.out(), "code = {}", a.code);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "tokens = {}", a.tokens);
    fmt::format_to(ctx.out(), " }}");
    return ctx.out();
  }
};
struct Lexer {
    Type_Info type_info;
    FileLoc file_loc;
    std::string content;
    uint64_t character_index;
    char character;
};

template <>
struct fmt::formatter<Lexer> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(const Lexer &a, FormatContext &ctx) const {
    fmt::format_to(ctx.out(), "Lexer {{ ");
    fmt::format_to(ctx.out(), "file_loc = {}", a.file_loc);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "content = {}", a.content);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "character_index = {}", a.character_index);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "character = {}", a.character);
    fmt::format_to(ctx.out(), " }}");
    return ctx.out();
  }
};
struct Ast {
    Type_Info type_info;
    std::vector<AstNode> nodes;
};

template <>
struct fmt::formatter<Ast> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(const Ast &a, FormatContext &ctx) const {
    fmt::format_to(ctx.out(), "Ast {{ ");
    fmt::format_to(ctx.out(), "nodes = {}", a.nodes);
    fmt::format_to(ctx.out(), " }}");
    return ctx.out();
  }
};
struct Parser {
    Type_Info type_info;
    uint64_t token_index;
    Token current_token;
    Token previous_token;
    Token next_token;
    int32_t scope;
    bool is_on_rhs_of_dot;
    bool had_error;
    bool panic_mode;
};

template <>
struct fmt::formatter<Parser> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(const Parser &a, FormatContext &ctx) const {
    fmt::format_to(ctx.out(), "Parser {{ ");
    fmt::format_to(ctx.out(), "token_index = {}", a.token_index);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "current_token = {}", a.current_token);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "previous_token = {}", a.previous_token);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "next_token = {}", a.next_token);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "scope = {}", a.scope);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "is_on_rhs_of_dot = {}", a.is_on_rhs_of_dot);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "had_error = {}", a.had_error);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "panic_mode = {}", a.panic_mode);
    fmt::format_to(ctx.out(), " }}");
    return ctx.out();
  }
};
struct ParseResult {
    Type_Info type_info;
    ParseCode code;
    Ast ast;
};

template <>
struct fmt::formatter<ParseResult> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(const ParseResult &a, FormatContext &ctx) const {
    fmt::format_to(ctx.out(), "ParseResult {{ ");
    fmt::format_to(ctx.out(), "code = {}", a.code);
    fmt::format_to(ctx.out(), ", ");
    fmt::format_to(ctx.out(), "ast = {}", a.ast);
    fmt::format_to(ctx.out(), " }}");
    return ctx.out();
  }
};
auto TOKEN_FLAG_KEYWORD = (uint32_t)1;auto TOKEN_FLAG_SYMBOL = (uint32_t)2;auto TOKEN_FLAG_BINARY_OPERATOR = (uint32_t)4;auto TOKEN_FLAG_UNARY_OPERATOR = (uint32_t)8;auto TOKEN_FLAG_COMPARATOR = (uint32_t)16;auto TOKEN_FLAG_ASSIGNMENT = (uint32_t)32;void usage(std::string program_name);
float random_f32_between_0_and_1();
int32_t random_s32_in_range(int32_t min, int32_t max);
Compilation_Result compile_file(Compilation_Options options);
LexResult lex_file(std::string source_string, std::string source_filepath);
Token lexer_next_token(Lexer* lexer);
void lexer_advance(Lexer* lexer);
Token advance_with_symbol(Lexer* lexer, uint32_t n, TokenKind token_kind);
Token lexer_eat_identifier(Lexer* lexer);
char lexer_peek(Lexer* lexer, uint32_t n);
Token lexer_eat_number(Lexer* lexer);
Token lexer_eat_string_literal(Lexer* lexer);
void next_character_on_this_line(Lexer* lexer);
void skip_whitespace(Lexer* lexer);
bool is_space(char c);
bool is_newline(char c);
bool is_digit(char c);
bool is_alnum(char c);
bool is_valid_identifier_character(char c);
bool is_keyword(std::string word, TokenKind* out_kind);
Token make_token(TokenKind kind, std::string contents, FileLoc file_loc);
Token make_symbol_token(TokenKind kind, FileLoc file_loc);
Token make_variable_token(TokenKind kind, std::string contents, FileLoc file_loc);
TokenInfo get_token_info(TokenKind kind);
bool is_symbol_token(TokenKind kind);

template <typename S, typename... Args>
void dragon_builtin_print_proc(const S& format_string, Args &&...args) {
  fmt::vprint(format_string, fmt::make_format_args(args...));
  fmt::print("\n");
}

void usage (std::string program_name) {
{
dragon_builtin_print_proc("Usage: {} <source-file>", program_name);
}
}

int main(int _argc, char *_argv[]) {
    std::vector<std::string> args;
    for (auto i = 0; i < _argc; ++i) args.push_back(_argv[i]);

{
if ( ((args.size())<2) )
{
usage(args [ 0 ]);
return -static_cast< int32_t > (1 );
}

auto source_filepath = args [ 1 ];
dragon_builtin_print_proc("Selfhost compiling {}", source_filepath);
Compilation_Options compilation_options {};
(compilation_options.input_filepath) = source_filepath;
auto compilation_result = compile_file(compilation_options);
dragon_builtin_print_proc("Compiled {} with compilation result: {}", compilation_options.input_filepath, compilation_result);
return static_cast< int32_t > (0 );
}
}

float random_f32_between_0_and_1 () {
{
auto rand_max_f32 = static_cast< float > (RAND_MAX );
return static_cast< float > ((rand()/rand_max_f32) );
}
}

int32_t random_s32_in_range (int32_t min, int32_t max) {
{
return static_cast< int32_t > ((rand()%(max+1-min)+min) );
}
}

Compilation_Result compile_file (Compilation_Options options) {
{
auto filepath_cstr = cstr(options.input_filepath);
auto source_file = fopen(filepath_cstr, "r");
fseek(source_file, 0, SEEK_END);
auto size = ftell(source_file);
fseek(source_file, 0, SEEK_SET);
auto buffer = static_cast< char* > (malloc(size) );
fread(buffer, 8, size, source_file);
fclose(source_file);
auto source_string = make_string(buffer, size);
auto lex_result = lex_file(source_string, options.input_filepath);
free(buffer);
return Compilation_Result::OK;
}
}

LexResult lex_file (std::string source_string, std::string source_filepath) {
{
LexResult lex_result {};
Lexer lexer {};
(lexer.content) = source_string;
(lexer.character) = (lexer.content) [ 0 ];
((lexer.file_loc).path) = source_filepath;
auto token = lexer_next_token(&lexer);
array_push(lex_result.tokens, token);
while ( ((token.info).kind)!=TokenKind::END_OF_FILE ) {
token = lexer_next_token(&lexer);
array_push(lex_result.tokens, token);
if ( ((token.info).kind)==TokenKind::INVALID )
{
(lex_result.code) = LexCode::INVALID_TOKEN;
dragon_builtin_print_proc("INVALID TOKEN ENCOUNTERED.\nTokens: {}", fmt::join(lex_result.tokens, ", "));
return lex_result;
}

}

(lex_result.code) = LexCode::OK;
dragon_builtin_print_proc("----------");
dragon_builtin_print_proc("{} tokens parsed.", (lex_result.tokens).size());
dragon_builtin_print_proc("{}", fmt::join(lex_result.tokens, ", "));
dragon_builtin_print_proc("----------");
return lex_result;
}
}

Token lexer_next_token (Lexer* lexer) {
{
skip_whitespace(lexer);
dragon_builtin_print_proc("Lexer next token is at: {}", lexer->character);
Token result {};
(result.file_loc) = (lexer->file_loc);
if ( (lexer->character_index)>=(((lexer->content).length())-1) )
return make_symbol_token(TokenKind::END_OF_FILE, lexer->file_loc);

if ( lexer_peek(lexer, 0)==47&&lexer_peek(lexer, 1) )
{
while ( lexer_peek(lexer, 0)==47&&lexer_peek(lexer, 1) ) {
while ( !is_newline(lexer->character) ) next_character_on_this_line(lexer);

skip_whitespace(lexer);
}

}

if ( is_digit(lexer->character) )
lexer_eat_number(lexer);

if ( is_valid_identifier_character(lexer->character) )
{
auto identifier = lexer_eat_identifier(lexer);
TokenKind keyword_token_kind {};
if ( is_keyword(identifier.contents, &keyword_token_kind) )
{
(result.info) = get_token_info(keyword_token_kind);
return result;
}

return identifier;
}

if ( (lexer->character)==34 )
return lexer_eat_string_literal(lexer);

if ( (lexer->character)==0 )
{
lexer_advance(lexer);
return make_symbol_token(TokenKind::END_OF_FILE, lexer->file_loc);
}

if ( (lexer->character)==59 )
return advance_with_symbol(lexer, 1, TokenKind::SEMI);

if ( (lexer->character)==58 )
return advance_with_symbol(lexer, 1, TokenKind::COLON);

if ( (lexer->character)==44 )
return advance_with_symbol(lexer, 1, TokenKind::COMMA);

if ( (lexer->character)==123 )
return advance_with_symbol(lexer, 1, TokenKind::OPEN_CURLY);

if ( (lexer->character)==125 )
return advance_with_symbol(lexer, 1, TokenKind::CLOSE_CURLY);

if ( (lexer->character)==40 )
return advance_with_symbol(lexer, 1, TokenKind::OPEN_PAREN);

if ( (lexer->character)==41 )
return advance_with_symbol(lexer, 1, TokenKind::CLOSE_PAREN);

if ( (lexer->character)==91 )
return advance_with_symbol(lexer, 1, TokenKind::OPEN_SQUARE);

if ( (lexer->character)==93 )
return advance_with_symbol(lexer, 1, TokenKind::CLOSE_SQUARE);

if ( (lexer->character)==94 )
return advance_with_symbol(lexer, 1, TokenKind::POINTER);

if ( (lexer->character)==64 )
return advance_with_symbol(lexer, 1, TokenKind::DEREFERENCE);

if ( (lexer->character)==33 )
{
if ( (lexer->content) [ (lexer->character_index)+1 ]==61 )
return advance_with_symbol(lexer, 2, TokenKind::NOTEQ);

return advance_with_symbol(lexer, 1, TokenKind::BANG);
}

if ( (lexer->character)==61 )
{
if ( (lexer->content) [ (lexer->character_index)+1 ]==61 )
return advance_with_symbol(lexer, 2, TokenKind::EQEQ);

return advance_with_symbol(lexer, 1, TokenKind::EQ);
}

if ( (lexer->character)==62 )
{
if ( (lexer->content) [ (lexer->character_index)+1 ]==61 )
return advance_with_symbol(lexer, 2, TokenKind::GE);

return advance_with_symbol(lexer, 1, TokenKind::GT);
}

if ( (lexer->character)==60 )
{
if ( (lexer->content) [ (lexer->character_index)+1 ]==61 )
return advance_with_symbol(lexer, 2, TokenKind::LE);

return advance_with_symbol(lexer, 1, TokenKind::LT);
}

if ( (lexer->character)==43 )
{
if ( (lexer->content) [ (lexer->character_index)+1 ]==61 )
return advance_with_symbol(lexer, 2, TokenKind::ADD_EQ);

return advance_with_symbol(lexer, 1, TokenKind::ADD);
}

if ( (lexer->character)==45 )
{
if ( (lexer->content) [ (lexer->character_index)+1 ]==61 )
return advance_with_symbol(lexer, 2, TokenKind::SUB_EQ);

return advance_with_symbol(lexer, 1, TokenKind::SUB);
}

if ( (lexer->character)==42 )
{
if ( (lexer->content) [ (lexer->character_index)+1 ]==61 )
return advance_with_symbol(lexer, 2, TokenKind::MUL_EQ);

return advance_with_symbol(lexer, 1, TokenKind::MUL);
}

if ( (lexer->character)==47 )
{
if ( (lexer->content) [ (lexer->character_index)+1 ]==61 )
return advance_with_symbol(lexer, 2, TokenKind::DIV_EQ);

return advance_with_symbol(lexer, 1, TokenKind::DIV);
}

if ( (lexer->character)==37 )
{
if ( (lexer->content) [ (lexer->character_index)+1 ]==61 )
return advance_with_symbol(lexer, 2, TokenKind::MOD_EQ);

return advance_with_symbol(lexer, 1, TokenKind::MOD);
}

if ( (lexer->character)==46 )
{
if ( (lexer->content) [ (lexer->character_index)+1 ]==46 )
return advance_with_symbol(lexer, 2, TokenKind::DOTDOT);

return advance_with_symbol(lexer, 1, TokenKind::DOT);
}

if ( (lexer->character)==124 )
{
if ( (lexer->content) [ (lexer->character_index)+1 ]==124 )
return advance_with_symbol(lexer, 2, TokenKind::OROR);

if ( (lexer->content) [ (lexer->character_index)+1 ]==61 )
return advance_with_symbol(lexer, 2, TokenKind::OR_EQ);

return advance_with_symbol(lexer, 1, TokenKind::OR);
}

if ( (lexer->character)==38 )
{
if ( (lexer->content) [ (lexer->character_index)+1 ]==38 )
return advance_with_symbol(lexer, 2, TokenKind::ANDAND);

if ( (lexer->content) [ (lexer->character_index)+1 ]==61 )
return advance_with_symbol(lexer, 2, TokenKind::AND_EQ);

return advance_with_symbol(lexer, 1, TokenKind::AND);
}

dragon_builtin_print_proc("Couldn't parse token that starts with `{}`", lexer_peek(lexer, 0));
return result;
}
}

void lexer_advance (Lexer* lexer) {
{
next_character_on_this_line(lexer);
while ( (is_newline(lexer->character)) ) {
(lexer->character_index) += 1;
(lexer->character) = (lexer->content) [ lexer->character_index ];
((lexer->file_loc).col) = 0;
((lexer->file_loc).row) += 1;
}

}
}

Token advance_with_symbol (Lexer* lexer, uint32_t n, TokenKind token_kind) {
{
auto file_loc = lexer->file_loc;
for (auto it = 0 ; it < n ; ++it) lexer_advance(lexer);

return make_symbol_token(token_kind, file_loc);
}
}

Token lexer_eat_identifier (Lexer* lexer) {
{
auto file_loc = lexer->file_loc;
std::string identifier {};
while ( is_valid_identifier_character(lexer->character) ) {
identifier += lexer->character;
lexer_advance(lexer);
}

return make_variable_token(TokenKind::IDENTIFIER, identifier, file_loc);
}
}

char lexer_peek (Lexer* lexer, uint32_t n) {
{
if ( (lexer->character_index)+n<((lexer->content).length())-1 )
{
return static_cast< char > (((lexer->content) [ (lexer->character_index)+n ]) );
}

return static_cast< char > (0 );
}
}

Token lexer_eat_number (Lexer* lexer) {
{
auto file_loc = lexer->file_loc;
std::string the_number_as_string {};
while ( is_digit(lexer->character) ) {
the_number_as_string += lexer_peek(lexer, 0);
lexer_advance(lexer);
}

return make_variable_token(TokenKind::INTEGER_LITERAL, the_number_as_string, file_loc);
}
}

Token lexer_eat_string_literal (Lexer* lexer) {
{
auto file_loc = lexer->file_loc;
lexer_advance(lexer);
std::string string_literal {};
while ( (lexer->character)!=34 ) {
string_literal += lexer_peek(lexer, 0);
lexer_advance(lexer);
}

lexer_advance(lexer);
return make_variable_token(TokenKind::STRING_LITERAL, string_literal, file_loc);
}
}

void next_character_on_this_line (Lexer* lexer) {
{
if ( !is_newline(lexer->character)&&(lexer->character)!=0&&((lexer->character_index)+1)<((lexer->content).length()) )
{
(lexer->character_index) += 1;
(lexer->character) = (lexer->content) [ lexer->character_index ];
((lexer->file_loc).col) += 1;
}

}
}

void skip_whitespace (Lexer* lexer) {
{
while ( is_space(lexer->character) ) lexer_advance(lexer);

}
}

bool is_space (char c) {
{
return static_cast< bool > ((is_newline(c)||(c==32)) );
}
}

bool is_newline (char c) {
{
return static_cast< bool > (((c==10)||(c==13)) );
}
}

bool is_digit (char c) {
{
return (c>=48&&c<=57);
}
}

bool is_alnum (char c) {
{
return static_cast< bool > (((c>=48&&c<=57)||(c>=65&&c<=90)||(c>=97&&c<=122)) );
}
}

bool is_valid_identifier_character (char c) {
{
return static_cast< bool > ((is_alnum(c)||c==95) );
}
}

bool is_keyword (std::string word, TokenKind* out_kind) {
{
if ( word=="alias" )
{
(*out_kind) = TokenKind::ALIAS;
return true;
}

if ( word=="as" )
{
(*out_kind) = TokenKind::AS;
return true;
}

if ( word=="defer" )
{
(*out_kind) = TokenKind::DEFER;
return true;
}

if ( word=="else" )
{
(*out_kind) = TokenKind::ELSE;
return true;
}

if ( word=="enum" )
{
(*out_kind) = TokenKind::ENUM;
return true;
}

if ( word=="external" )
{
(*out_kind) = TokenKind::EXTERNAL;
return true;
}

if ( word=="false" )
{
(*out_kind) = TokenKind::FALSE;
return true;
}

if ( word=="for" )
{
(*out_kind) = TokenKind::FOR;
return true;
}

if ( word=="if" )
{
(*out_kind) = TokenKind::IF;
return true;
}

if ( word=="is" )
{
(*out_kind) = TokenKind::IS;
return true;
}

if ( word=="include" )
{
(*out_kind) = TokenKind::INCLUDE;
return true;
}

if ( word=="match" )
{
(*out_kind) = TokenKind::MATCH;
return true;
}

if ( word=="return" )
{
(*out_kind) = TokenKind::RETURN;
return true;
}

if ( word=="struct" )
{
(*out_kind) = TokenKind::STRUCT;
return true;
}

if ( word=="true" )
{
(*out_kind) = TokenKind::TRUE;
return true;
}

if ( word=="while" )
{
(*out_kind) = TokenKind::WHILE;
return true;
}

(*out_kind) = TokenKind::INVALID;
return false;
}
}

Token make_token (TokenKind kind, std::string contents, FileLoc file_loc) {
{
auto info = get_token_info(kind);
Token result {};
(result.file_loc) = file_loc;
(result.info) = info;
(result.contents) = contents;
return result;
}
}

Token make_symbol_token (TokenKind kind, FileLoc file_loc) {
{
if ( !is_symbol_token(kind) )
{
dragon_builtin_print_proc("ERROR: {} is not a symbol token kind.", kind);
exit(1);
}

return make_token(kind, "", file_loc);
}
}

Token make_variable_token (TokenKind kind, std::string contents, FileLoc file_loc) {
{
auto info = get_token_info(kind);
return make_token(kind, contents, file_loc);
}
}

TokenInfo get_token_info (TokenKind kind) {
{
TokenInfo info {};
(info.kind) = kind;
switch (kind) {
case TokenKind::INVALID: {
{
(info.flags) = (uint32_t) 0;
(info.name) = "invalid";
}
} break;
case TokenKind::END_OF_FILE: {
{
(info.flags) = TOKEN_FLAG_SYMBOL;
(info.name) = "end of file";
}
} break;
case TokenKind::ALIAS: {
{
(info.flags) = TOKEN_FLAG_KEYWORD;
(info.name) = "alias";
}
} break;
case TokenKind::AS: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_KEYWORD|TOKEN_FLAG_UNARY_OPERATOR;
(info.name) = "as";
}
} break;
case TokenKind::DEFER: {
{
(info.flags) = TOKEN_FLAG_KEYWORD;
(info.name) = "defer";
}
} break;
case TokenKind::ELSE: {
{
(info.flags) = TOKEN_FLAG_KEYWORD;
(info.name) = "else";
}
} break;
case TokenKind::ENUM: {
{
(info.flags) = TOKEN_FLAG_KEYWORD;
(info.name) = "enum";
}
} break;
case TokenKind::EXTERNAL: {
{
(info.flags) = TOKEN_FLAG_KEYWORD;
(info.name) = "external";
}
} break;
case TokenKind::FOR: {
{
(info.flags) = TOKEN_FLAG_KEYWORD;
(info.name) = "for";
}
} break;
case TokenKind::FALSE: {
{
(info.flags) = TOKEN_FLAG_KEYWORD;
(info.name) = "false";
}
} break;
case TokenKind::IF: {
{
(info.flags) = TOKEN_FLAG_KEYWORD;
(info.name) = "if";
}
} break;
case TokenKind::IS: {
{
(info.flags) = TOKEN_FLAG_KEYWORD;
(info.name) = "is";
}
} break;
case TokenKind::INCLUDE: {
{
(info.flags) = TOKEN_FLAG_KEYWORD;
(info.name) = "include";
}
} break;
case TokenKind::MATCH: {
{
(info.flags) = TOKEN_FLAG_KEYWORD;
(info.name) = "match";
}
} break;
case TokenKind::RETURN: {
{
(info.flags) = TOKEN_FLAG_KEYWORD;
(info.name) = "return";
}
} break;
case TokenKind::STRUCT: {
{
(info.flags) = TOKEN_FLAG_KEYWORD;
(info.name) = "struct";
}
} break;
case TokenKind::TRUE: {
{
(info.flags) = TOKEN_FLAG_KEYWORD;
(info.name) = "true";
}
} break;
case TokenKind::WHILE: {
{
(info.flags) = TOKEN_FLAG_KEYWORD;
(info.name) = "while";
}
} break;
case TokenKind::STRING_LITERAL: {
{
(info.flags) = (uint32_t) 0;
(info.name) = "";
}
} break;
case TokenKind::INTEGER_LITERAL: {
{
(info.flags) = (uint32_t) 0;
(info.name) = "";
}
} break;
case TokenKind::FLOAT_LITERAL: {
{
(info.flags) = (uint32_t) 0;
(info.name) = "0";
}
} break;
case TokenKind::IDENTIFIER: {
{
(info.flags) = (uint32_t) 0;
(info.name) = "0";
}
} break;
case TokenKind::OPEN_PAREN: {
{
(info.flags) = TOKEN_FLAG_SYMBOL;
(info.name) = "(";
}
} break;
case TokenKind::CLOSE_PAREN: {
{
(info.flags) = TOKEN_FLAG_SYMBOL;
(info.name) = ")";
}
} break;
case TokenKind::OPEN_CURLY: {
{
(info.flags) = TOKEN_FLAG_SYMBOL;
(info.name) = "{";
}
} break;
case TokenKind::CLOSE_CURLY: {
{
(info.flags) = TOKEN_FLAG_SYMBOL;
(info.name) = "}";
}
} break;
case TokenKind::OPEN_SQUARE: {
{
(info.flags) = TOKEN_FLAG_SYMBOL;
(info.name) = "[";
}
} break;
case TokenKind::CLOSE_SQUARE: {
{
(info.flags) = TOKEN_FLAG_SYMBOL;
(info.name) = "]";
}
} break;
case TokenKind::EQ: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_ASSIGNMENT;
(info.name) = "=";
}
} break;
case TokenKind::ADD_EQ: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_ASSIGNMENT;
(info.name) = "+=";
}
} break;
case TokenKind::SUB_EQ: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_ASSIGNMENT;
(info.name) = "-=";
}
} break;
case TokenKind::MUL_EQ: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_ASSIGNMENT;
(info.name) = "*=";
}
} break;
case TokenKind::DIV_EQ: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_ASSIGNMENT;
(info.name) = "/=";
}
} break;
case TokenKind::MOD_EQ: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_ASSIGNMENT;
(info.name) = "%=";
}
} break;
case TokenKind::AND_EQ: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_ASSIGNMENT;
(info.name) = "&=";
}
} break;
case TokenKind::OR_EQ: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_ASSIGNMENT;
(info.name) = "|=";
}
} break;
case TokenKind::COMMA: {
{
(info.flags) = TOKEN_FLAG_SYMBOL;
(info.name) = ",";
}
} break;
case TokenKind::SEMI: {
{
(info.flags) = TOKEN_FLAG_SYMBOL;
(info.name) = ";";
}
} break;
case TokenKind::COLON: {
{
(info.flags) = TOKEN_FLAG_SYMBOL;
(info.name) = ":";
}
} break;
case TokenKind::EQEQ: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_BINARY_OPERATOR|TOKEN_FLAG_COMPARATOR;
(info.name) = "==";
}
} break;
case TokenKind::NOTEQ: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_BINARY_OPERATOR|TOKEN_FLAG_COMPARATOR;
(info.name) = "!=";
}
} break;
case TokenKind::LT: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_BINARY_OPERATOR|TOKEN_FLAG_COMPARATOR;
(info.name) = "<";
}
} break;
case TokenKind::GT: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_BINARY_OPERATOR|TOKEN_FLAG_COMPARATOR;
(info.name) = ">";
}
} break;
case TokenKind::LE: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_BINARY_OPERATOR|TOKEN_FLAG_COMPARATOR;
(info.name) = "<=";
}
} break;
case TokenKind::GE: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_BINARY_OPERATOR|TOKEN_FLAG_COMPARATOR;
(info.name) = ">=";
}
} break;
case TokenKind::ADD: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_BINARY_OPERATOR;
(info.name) = "+";
}
} break;
case TokenKind::SUB: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_BINARY_OPERATOR;
(info.name) = "-";
}
} break;
case TokenKind::MUL: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_BINARY_OPERATOR;
(info.name) = "*";
}
} break;
case TokenKind::DIV: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_BINARY_OPERATOR;
(info.name) = "/";
}
} break;
case TokenKind::MOD: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_BINARY_OPERATOR;
(info.name) = "%";
}
} break;
case TokenKind::DOT: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_BINARY_OPERATOR;
(info.name) = ".";
}
} break;
case TokenKind::DOTDOT: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_BINARY_OPERATOR;
(info.name) = "..";
}
} break;
case TokenKind::AND: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_BINARY_OPERATOR;
(info.name) = "&";
}
} break;
case TokenKind::ANDAND: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_BINARY_OPERATOR;
(info.name) = "&&";
}
} break;
case TokenKind::OR: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_BINARY_OPERATOR;
(info.name) = "|";
}
} break;
case TokenKind::OROR: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_BINARY_OPERATOR;
(info.name) = "||";
}
} break;
case TokenKind::BANG: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_UNARY_OPERATOR;
(info.name) = "!";
}
} break;
case TokenKind::POINTER: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_UNARY_OPERATOR;
(info.name) = "^";
}
} break;
case TokenKind::DEREFERENCE: {
{
(info.flags) = (uint32_t) TOKEN_FLAG_SYMBOL|TOKEN_FLAG_UNARY_OPERATOR;
(info.name) = "@";
}
} break;
default: {{
dragon_builtin_print_proc("ERROR: Invalid token kind in get_token_info().");
}
} break;
}
return info;
}
}

bool is_symbol_token (TokenKind kind) {
{
auto info = get_token_info(kind);
return static_cast< bool > (((info.flags)|TOKEN_FLAG_SYMBOL) );
}
}

