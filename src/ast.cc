
#include "ast.h"

#include <cstddef>
#include <memory>
#include <stdarg.h>
#include <stdbool.h>

#include "typechecker.h"
#include "logging.h"

AstNode::~AstNode() { }
ExpressionAstNode::~ExpressionAstNode() { }
StatementAstNode::~StatementAstNode() { }
ForLoop::~ForLoop() { }

Ref<AstNode> StatementAstNode::copy() { return InvalidAstNode::make(this->start_token); }
usize StatementAstNode::source_length() { return 0; }
Ref<AstNode> ExpressionAstNode::copy() { return InvalidAstNode::make(this->start_token); }
Ref<AstNode> LiteralExpression::copy() { return InvalidAstNode::make(this->start_token); }


bool is_statement(Ref<AstNode> node) { return node->node_kind == AstNode::Kind::STATEMENT; }
bool is_expression(Ref<AstNode> node) { return node->node_kind == AstNode::Kind::EXPRESSION; }


