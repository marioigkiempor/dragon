#ifndef DRAGON_AST_H_
#define DRAGON_AST_H_

#include <cstdint>
#include <cstdio>
#include <exception>
#include <memory>

#include "defines.h"

#include "fmt/core.h"
#include "lexer.h"
#include "types.h"
#include "logging.h"

struct AstNode {
  enum class Kind {
    INVALID = 0,
    PROGRAM,
    STATEMENT,
    EXPRESSION,
  };

  AstNode::AstNode::Kind node_kind = AstNode::Kind::INVALID;

  usize index = 0;
  u32 flags = 0;

  Token start_token = {};

  bool type_checked = false;
  bool had_type_error = false;

  Vec<Ref<AstNode>> dependency_nodes = {};
  bool dependencies_resolved = false;

  AstNode(Kind node_kind, Token start_token)
  : node_kind(node_kind)
  , start_token(start_token) {
  }

  AstNode() = default;
  AstNode(const AstNode&) = default;
  AstNode(AstNode&&) = default;

  // AstNode(const AstNode& other)
  // : node_kind(other.node_kind)
  // , index(other.index)
  // , flags(other.flags)
  // , start_token(other.start_token)
  // , type_checked(other.type_checked)
  // , had_type_error(other.had_type_error)
  // , dependency_nodes(other.dependency_nodes)
  // , dependencies_resolved(other.dependencies_resolved) {
  // }

  virtual Ref<AstNode> copy() = 0;
  virtual usize source_length() = 0;

  virtual ~AstNode() = 0;
};

struct InvalidAstNode : public AstNode {
  InvalidAstNode(Token start_token) : AstNode(AstNode::Kind::INVALID, start_token) { }
  static Ref<AstNode> make(Token start_token) { return std::make_shared<InvalidAstNode>(start_token); }
  virtual Ref<AstNode> copy() override { return InvalidAstNode::make(this->start_token); }
  virtual usize source_length() override { return 0; }
  ~InvalidAstNode() = default;
};

struct ExpressionAstNode : public AstNode {
  enum class Kind {
    INVALID = 0,
    LITERAL,
    GROUPING,
    CAST,
    BINARY,
    UNARY,
    VARIABLE_REFERENCE,
    ARRAY_REFERENCE,
    ARRAY_LITERAL,
    PROCEDURE_CALL,
    ASSIGNMENT,
  };

  ExpressionAstNode::Kind expression_kind = ExpressionAstNode::Kind::INVALID;

  Ref<Type> type;

  ExpressionAstNode(Token start_token)
  : AstNode(AstNode::Kind::EXPRESSION, start_token) {
    this->expression_kind = ExpressionAstNode::Kind::INVALID;
  }

  ExpressionAstNode(ExpressionAstNode::Kind expression_kind, Token start_token)
  : AstNode(AstNode::Kind::EXPRESSION, start_token)
  , expression_kind(expression_kind) {
  }

  // ExpressionAstNode(const ExpressionAstNode& other)
  // : AstNode(other)
  // , expression_kind(other.expression_kind)
  // , type(other.type) {
  // }

  virtual Ref<AstNode> copy() = 0;
  virtual usize source_length() = 0;

  virtual ~ExpressionAstNode() = 0;

  virtual bool is_invalid() { return false; }
};

struct InvalidExpression : public ExpressionAstNode {
  InvalidExpression(Token start_token)
  : ExpressionAstNode(ExpressionAstNode::Kind::INVALID, start_token) {
  }

  static Ref<ExpressionAstNode> make(Token start_token) { return std::make_shared<InvalidExpression>(start_token); }

  Ref<AstNode> copy() override { return InvalidExpression::make(this->start_token); }
  virtual usize source_length() override { return  0; }

  bool is_invalid() override { return true; }
};

struct LiteralExpression : public ExpressionAstNode {
  enum class Kind {
    INVALID = 0,
    U64,
    F64,
    BOOLEAN,
    STRING,
  };

  LiteralExpression::Kind literal_kind = LiteralExpression::Kind::INVALID;
  Token literal_token;

  LiteralExpression(Token start_token, LiteralExpression::Kind literal_kind)
  : ExpressionAstNode(ExpressionAstNode::Kind::LITERAL, start_token)
  , literal_kind(literal_kind)
  , literal_token(start_token) {
  }

  // LiteralExpression(const LiteralExpression& other)
  // : ExpressionAstNode(other)
  // , literal_kind(other.literal_kind)
  // , literal_token(other.literal_token) {
  // }

  virtual Ref<AstNode> copy() override;
  virtual usize source_length() override { return  literal_token.length(); }
  virtual ~LiteralExpression() { }
};

struct BooleanLiteral : public LiteralExpression {
  bool value;

  BooleanLiteral(Token token)
  : LiteralExpression(token, LiteralExpression::Kind::BOOLEAN)
  , value(string_view_to_bool(token.contents)) {
  }

  // BooleanLiteral(const BooleanLiteral& other)
  // : LiteralExpression(other)
  // , value(other.value) {
  // }

  static Ref<ExpressionAstNode> make(Token token) { return std::make_shared<BooleanLiteral> (token); }

  Ref<AstNode> copy() { return BooleanLiteral::make(this->literal_token); }
};

struct FloatLiteral : public LiteralExpression {
  f64 value;

  FloatLiteral(Token token)
  : LiteralExpression(token, LiteralExpression::Kind::F64) 
  , value(string_view_to_f64(token.contents)) {
  }

  // FloatLiteral(const FloatLiteral& other)
  // : LiteralExpression(other)
  // , value(other.value) {
  // }

  static Ref<ExpressionAstNode> make(Token token) { return std::make_shared<FloatLiteral> (token); }

  Ref<AstNode> copy() { return FloatLiteral::make(this->literal_token); }
};

struct IntegerLiteral : public LiteralExpression {
  u64 value;

  IntegerLiteral(Token token)
  : LiteralExpression(token, LiteralExpression::Kind::U64)
  , value(string_view_to_u64(token.contents)) {
  }

  // IntegerLiteral(const IntegerLiteral& other)
  // : LiteralExpression(other)
  // , value(other.value) {
  // }

  static Ref<ExpressionAstNode> make(Token token) { return std::make_shared<IntegerLiteral> (token); }

  Ref<AstNode> copy() { return IntegerLiteral::make(this->literal_token); }
};

struct StringLiteral : public LiteralExpression {
  std::string value;

  StringLiteral(Token token)
  : LiteralExpression(token, LiteralExpression::Kind::STRING)
  , value(token.contents) {
  }

  // StringLiteral(const StringLiteral& other)
  // : LiteralExpression(other)
  // , value(other.value) {
  // }

  static Ref<ExpressionAstNode> make(Token token) {
    return std::make_shared<StringLiteral> (token);
  }

  Ref<AstNode> copy() { return StringLiteral::make(this->literal_token); }
};


struct GroupingExpression : public ExpressionAstNode {
  Ref<ExpressionAstNode> grouped_expression = nullptr;
  Token open_paren = {};
  Token close_paren = {};

  GroupingExpression(Token open_paren, Ref<ExpressionAstNode> grouped_node, Token close_paren)
  : ExpressionAstNode(ExpressionAstNode::Kind::GROUPING, open_paren)
  , grouped_expression(grouped_node)
  , open_paren(open_paren)
  , close_paren(close_paren) {
  }

  // GroupingExpression(const GroupingExpression& other)
  // : ExpressionAstNode(other)
  // , grouped_expression(other.grouped_expression) {
  // }

  static Ref<ExpressionAstNode> make(Token open_paren, Ref<ExpressionAstNode> grouped_node, Token close_paren) {
    return std::make_shared<GroupingExpression>(open_paren, grouped_node, close_paren);
  }

  Ref<AstNode> copy() override { return GroupingExpression::make(this->open_paren, this->grouped_expression, this->close_paren); }
  virtual usize source_length() override { return this->close_paren.file_loc.byte_offset - this->open_paren.file_loc.byte_offset; }
};

struct UnaryExpression : public ExpressionAstNode {
  Token operatorr = {};
  Ref<ExpressionAstNode> operand_node = nullptr;

  UnaryExpression(Token start_token, Token operatorr, Ref<ExpressionAstNode> operand_node)
  : ExpressionAstNode(ExpressionAstNode::Kind::UNARY, start_token)
  , operatorr(operatorr)
  , operand_node(operand_node) {
  }

  // UnaryExpression(const UnaryExpression& other)
  // : ExpressionAstNode(other)
  // , operatorr(other.operatorr)
  // , operand_node(other.operand_node) {
  // }

  static Ref<ExpressionAstNode> make(Token start_token, Token operatorr, Ref<ExpressionAstNode> operand_node) {
    return std::make_shared<UnaryExpression>(start_token, operatorr, operand_node);
  }

  Ref<AstNode> copy() override { return UnaryExpression::make(this->start_token, this->operatorr, this->operand_node); }

  virtual usize source_length() override { 
    auto operand_length = operand_node->source_length();
    auto operand_end = operand_node->start_token.file_loc.byte_offset + operand_length;
    return operand_end - operatorr.file_loc.byte_offset;
  }
};

struct CastExpression : public ExpressionAstNode {
  Ref<ExpressionAstNode> casted_node = nullptr;

  Vec<Token> type_tokens = {};

  CastExpression(Token start_token, Ref<ExpressionAstNode> casted_node, Vec<Token> type_tokens)
  : ExpressionAstNode(ExpressionAstNode::Kind::CAST, start_token)
  , casted_node(casted_node)
  , type_tokens(type_tokens) {
  }

  // CastExpression(const CastExpression& other) 
  // : ExpressionAstNode(other)
  // , casted_node(other.casted_node)
  // , type_tokens(other.type_tokens) {
  // }

  static Ref<ExpressionAstNode> make(Token start_token, Ref<ExpressionAstNode> casted_node, Vec<Token> type_tokens) {
    return std::make_shared<CastExpression>(start_token, casted_node, type_tokens);
  }

  Ref<AstNode> copy() override { return CastExpression::make(this->start_token, this->casted_node, this->type_tokens); }

  virtual usize source_length() override {
    auto& last_type_token = type_tokens[type_tokens.size()-1];
    auto last_type_token_end = last_type_token.file_loc.byte_offset + last_type_token.length();
    return last_type_token_end - start_token.file_loc.byte_offset;
  }
};

struct BinaryExpression : public ExpressionAstNode {
  Ref<ExpressionAstNode> lhs_expression = nullptr;
  Ref<ExpressionAstNode> rhs_expression = nullptr;
  Token operatorr = {};

  bool is_struct_member_reference = false;
  bool is_enum_member_reference   = false;

  BinaryExpression(Token start_token, Ref<ExpressionAstNode> lhs_node, Token operatorr, Ref<ExpressionAstNode> rhs_node, bool is_struct_member_reference = false, bool is_enum_member_reference = false)
  : ExpressionAstNode(ExpressionAstNode::Kind::BINARY, start_token)
  , lhs_expression(lhs_node)
  , rhs_expression(rhs_node)
  , operatorr(operatorr)
  , is_struct_member_reference(is_struct_member_reference)
  , is_enum_member_reference(is_enum_member_reference) {
  }

  // BinaryExpression(const BinaryExpression& other) 
  // : ExpressionAstNode(other)
  // , lhs_expression(other.lhs_expression)
  // , rhs_expression(other.rhs_expression) 
  // , operatorr(other.operatorr) {
  // }

  static Ref<ExpressionAstNode> make(Token start_token, Ref<ExpressionAstNode> lhs_node, Token operatorr, Ref<ExpressionAstNode> rhs_node) {
    return std::make_shared<BinaryExpression>(start_token, lhs_node, operatorr, rhs_node);
  }

  Ref<AstNode> copy() override { return BinaryExpression::make(this->start_token, this->lhs_expression, this->operatorr, this->rhs_expression); }

  virtual usize source_length() override {
    auto start = lhs_expression->start_token.file_loc.byte_offset;
    auto end = rhs_expression->start_token.file_loc.byte_offset + rhs_expression->source_length();
    return end - start;
  }
};

// @Todo: Variable, struct and array references should be able
// be used with expressions: eg (a + 2)[2]
struct VariableReferenceExpression : public ExpressionAstNode {
  Token name = {};
  bool is_struct_member_reference = false;
  bool is_type_info_reference = false;

  VariableReferenceExpression(Token start_token, bool is_struct_member_reference = false, bool is_type_info_reference = false)
  : ExpressionAstNode(ExpressionAstNode::Kind::VARIABLE_REFERENCE, start_token)
  , name(start_token)
  , is_struct_member_reference(is_struct_member_reference)
  , is_type_info_reference(is_type_info_reference) {
  }

  // VariableReferenceExpression(const VariableReferenceExpression& other)
  // : ExpressionAstNode(other)
  // , name(other.start_token)
  // , is_struct_member_reference(other.is_struct_member_reference)
  // , is_type_info_reference(other.is_type_info_reference) {
  // }

  static Ref<ExpressionAstNode> make(Token start_token, bool is_struct_member_reference = false, bool is_type_info_reference = false) {
    return std::make_shared<VariableReferenceExpression>(start_token, is_struct_member_reference, is_type_info_reference);
  }

  Ref<AstNode> copy() override { return VariableReferenceExpression::make(this->start_token, this->is_struct_member_reference, this->is_type_info_reference); }

  virtual usize source_length() override {
    return name.length();
  }
};

struct ArrayReferenceExpression : public ExpressionAstNode {
  Ref<ExpressionAstNode> array_expression = nullptr;
  Ref<ExpressionAstNode> index_expression = nullptr;
  Token open_square = {};
  Token close_square = {};

  ArrayReferenceExpression(Token start_token, Ref<ExpressionAstNode> array_expression, Token open_square, Ref<ExpressionAstNode> index_expression, Token close_square)
  : ExpressionAstNode(ExpressionAstNode::Kind::ARRAY_REFERENCE, start_token)
  , array_expression(array_expression)
  , index_expression(index_expression)
  , open_square(open_square)
  , close_square(close_square) {
  }

  // ArrayReferenceExpression(const ArrayReferenceExpression& other)
  // : ExpressionAstNode(other)
  // , array_expression(other.array_expression)
  // , index_expression(other.index_expression) {
  // }

  static Ref<ExpressionAstNode> make(Token start_token, Ref<ExpressionAstNode> array_expression, Token open_square, Ref<ExpressionAstNode> index_expression, Token close_square) {
    return std::make_shared<ArrayReferenceExpression>(start_token, array_expression, open_square, index_expression, close_square);
  }

  Ref<AstNode> copy() override { return ArrayReferenceExpression::make(this->start_token, this->array_expression, this->open_square, this->index_expression, this->close_square); }
  virtual usize source_length() override {
    return close_square.file_loc.byte_offset - start_token.file_loc.byte_offset;
  }
};

struct AssignmentExpression : public ExpressionAstNode {
  Ref<ExpressionAstNode> lhs_node = nullptr;
  Ref<ExpressionAstNode> rhs_node = nullptr;
  Token operatorr;

  AssignmentExpression(Token start_token, Ref<ExpressionAstNode> lhs_node, Token operator_token, Ref<ExpressionAstNode> rhs_node)
  : ExpressionAstNode(ExpressionAstNode::Kind::ASSIGNMENT, start_token)
  , lhs_node(lhs_node)
  , rhs_node(rhs_node)
  , operatorr(operator_token) {
  }

  // AssignmentExpression(const AssignmentExpression& other)
  // : ExpressionAstNode(other)
  // , lhs_node(other.lhs_node)
  // , rhs_node(other.rhs_node)
  // , operatorr(other.operatorr) {
  // }

  static Ref<ExpressionAstNode> make(Token start_token, Ref<ExpressionAstNode> lhs_node, Token operator_token, Ref<ExpressionAstNode> rhs_node) {
    return std::make_shared<AssignmentExpression>(start_token, lhs_node, operator_token, rhs_node);
  }

  Ref<AstNode> copy() override { return AssignmentExpression::make(this->start_token, this->lhs_node, this->operatorr, this->rhs_node); }

  virtual usize source_length() override {
    auto start = lhs_node->start_token.file_loc.byte_offset;
    auto end = rhs_node->start_token.file_loc.byte_offset + rhs_node->source_length();
    return end - start;
  }
};

struct ProcedureCallExpression : public ExpressionAstNode {
  Ref<ExpressionAstNode> called_node = nullptr;
  Vec<Ref<ExpressionAstNode>> argument_nodes = {};
  Token open_paren = {};
  Token close_paren = {};

  ProcedureCallExpression(Token start_token, Ref<ExpressionAstNode> called_node, Token open_paren, Vec<Ref<ExpressionAstNode>> argument_nodes, Token close_paren)
  : ExpressionAstNode(ExpressionAstNode::Kind::PROCEDURE_CALL, start_token)
  , called_node(called_node)
  , argument_nodes(argument_nodes)
  , open_paren(open_paren)
  , close_paren(close_paren) {
  }

  // ProcedureCallExpression(const ProcedureCallExpression& other)
  // : ExpressionAstNode(other)
  // , called_node(other.called_node)
  // , argument_nodes(other.argument_nodes) {
  // }

  static Ref<ExpressionAstNode> make(Token start_token, Ref<ExpressionAstNode> called_node, Token open_paren, Vec<Ref<ExpressionAstNode>> argument_nodes, Token close_paren) {
    return std::make_shared<ProcedureCallExpression>(start_token, called_node, open_paren, argument_nodes, close_paren);
  }

  Ref<AstNode> copy() override { return ProcedureCallExpression::make(this->start_token, this->called_node, this->open_paren, this->argument_nodes, this->close_paren); }

  virtual usize source_length() override {
    return ((this->close_paren.file_loc.byte_offset + this->close_paren.length()) - this->start_token.file_loc.byte_offset);
  }
};

struct ArrayLiteralExpression : public ExpressionAstNode {
  Vec<Ref<ExpressionAstNode>> element_nodes = {};
  Token open_square = {};
  Token close_square = {};

  ArrayLiteralExpression(Token open_square, Vec<Ref<ExpressionAstNode>> element_nodes, Token close_square)
  : ExpressionAstNode(ExpressionAstNode::Kind::ARRAY_LITERAL, open_square)
  , element_nodes(element_nodes)
  , open_square(open_square)
  , close_square(close_square) {
  }

  // ArrayLiteralExpression(const ArrayLiteralExpression& other)
  // : ExpressionAstNode(other)
  // , element_nodes(other.element_nodes) {
  // }

  static Ref<ExpressionAstNode> make(Token open_square, Vec<Ref<ExpressionAstNode>> element_nodes, Token close_square) {
    return std::make_shared<ArrayLiteralExpression>(open_square, element_nodes, close_square);
  }

  Ref<AstNode> copy() override { return ArrayLiteralExpression::make(this->open_square, this->element_nodes, this->close_square); }

  virtual usize source_length() override {
    return ((this->close_square.file_loc.byte_offset + this->close_square.length()) - this->open_square.file_loc.byte_offset);
  }
};

struct StatementAstNode : public AstNode {
  enum Kind {
    INVALID = 0,
    CODE_BLOCK,
    EXPRESSION,
    IF,
    FOR,
    BREAK,
    WHILE,
    RETURN,
    VARIABLE_DECLARATION,
    PROCEDURE_DECLARATION,
    STRUCT_DECLARATION,
    ENUM_DECLARATION,
    TYPE_ALIAS,
    INCLUDE,
    DEFER,
    MATCH,
  };

  StatementAstNode::Kind statement_kind = StatementAstNode::Kind::INVALID;

  StatementAstNode(StatementAstNode::Kind statement_kind, Token start_token)
  : AstNode(AstNode::Kind::STATEMENT, start_token)
  , statement_kind(statement_kind) {
  }

  // StatementAstNode(const StatementAstNode& other)
  // : AstNode(other)
  // , statement_kind(other.statement_kind) {
  // }

  virtual ~StatementAstNode() = 0;
  virtual Ref<AstNode> copy()override;
  virtual usize source_length() override;
};

struct InvalidStatement : public StatementAstNode {
  InvalidStatement(Token start_token)
  : StatementAstNode(StatementAstNode::Kind::INVALID, start_token) {
  }

  static Ref<InvalidStatement> make(Token start_token) {
    return std::make_shared<InvalidStatement>(start_token);
  }

  Ref<AstNode> copy() override { return InvalidStatement::make(this->start_token); }
  virtual usize source_length() override { return 0; };
};

struct BreakStatement : public StatementAstNode {
  BreakStatement(Token start_token)
  : StatementAstNode(StatementAstNode::Kind::BREAK, start_token) {
  }

  static Ref<BreakStatement> make(Token start_token) {
    return std::make_shared<BreakStatement>(start_token);
  }

  Ref<AstNode> copy() override { return BreakStatement::make(this->start_token); }
  virtual usize source_length() override { return start_token.length(); };
};


struct ExpressionStatement : public StatementAstNode {
  Ref<ExpressionAstNode> expression_node = nullptr;

  ExpressionStatement(Token start_token, Ref<ExpressionAstNode> expression_node)
  : StatementAstNode(StatementAstNode::Kind::EXPRESSION, start_token)
  , expression_node(expression_node) {
  }

  // ExpressionStatement(const ExpressionStatement& other)
  // : StatementAstNode(other)
  // , expression_node(other.expression_node) {
  // }

  static Ref<ExpressionStatement> make(Token start_token, Ref<ExpressionAstNode> expression_node) {
    return std::make_shared<ExpressionStatement> (start_token, expression_node);
  }

  Ref<AstNode> copy() override { return ExpressionStatement::make(this->start_token, this->expression_node); }
  virtual usize source_length() override { return expression_node->source_length(); };
};

struct DeferStatement : public StatementAstNode {
  Token defer_keyword = {};
  Ref<StatementAstNode> deferred_node = nullptr;

  DeferStatement(Token defer_keyword, Ref<StatementAstNode> deferred_node)
  : StatementAstNode(StatementAstNode::Kind::DEFER, defer_keyword)
  , defer_keyword(defer_keyword)
  , deferred_node(deferred_node) {
  }

  // DeferStatement(const DeferStatement& other)
  // : StatementAstNode(other)
  // , defer_keyword(other.defer_keyword)
  // , deferred_node(other.deferred_node) {
  // }

  static Ref<DeferStatement> make(Token start_token, Ref<StatementAstNode> deferred_node) {
    return std::make_shared<DeferStatement>(start_token, deferred_node);
  }

  Ref<AstNode> copy() override { return DeferStatement::make(this->start_token, this->deferred_node); }
  virtual usize source_length() override {
    auto start = defer_keyword.file_loc.byte_offset;
    auto end = deferred_node->start_token.file_loc.byte_offset + deferred_node->source_length();
    return end-start;
  }
};

struct TypeAliasStatement : public StatementAstNode {
  Token type_alias_keyword = {};
  Token name = {};

  Vec<Token> type_tokens = {};
  Ref<Type> type = nullptr;

  TypeAliasStatement(Token type_alias_keyword, Token name, Vec<Token> type_tokens = {})
  : StatementAstNode(StatementAstNode::Kind::TYPE_ALIAS, type_alias_keyword)
  , type_alias_keyword(type_alias_keyword)
  , name(name)
  , type_tokens(type_tokens) {
  }

  // TypeAliasStatement(const TypeAliasStatement& other) 
  // : StatementAstNode(other)
  // , type_alias_keyword(other.type_alias_keyword)
  // , name(other.name)
  // , type_tokens(other.type_tokens)
  // , type(other.type) {
  // }

  static Ref<TypeAliasStatement> make(Token type_alias_keyword, Token name, Vec<Token> type_tokens = {}) {
    return std::make_shared<TypeAliasStatement> (type_alias_keyword, name, type_tokens);
  }

  Ref<AstNode> copy() override { return TypeAliasStatement::make(this->type_alias_keyword, this->name, this->type_tokens); }
  virtual usize source_length() override {
    auto start = type_alias_keyword.file_loc.byte_offset;
    auto end = type_tokens[type_tokens.size()-1].file_loc.byte_offset + type_tokens[type_tokens.size()-1].length();
    return end-start;
  }
};

struct CodeBlockStatement : public StatementAstNode {
  Token open_curly = {};
  Vec<Ref<StatementAstNode>> statement_nodes = {};
  Vec<Ref<StatementAstNode>> deferred_statement_nodes = {};
  Token close_curly = {};

  CodeBlockStatement(Token open_curly, Vec<Ref<StatementAstNode>> statement_nodes, Vec<Ref<StatementAstNode>> deferred_statement_nodes, Token close_curly)
  : StatementAstNode(StatementAstNode::Kind::CODE_BLOCK, open_curly)
  , open_curly(open_curly)
  , statement_nodes(statement_nodes)
  , deferred_statement_nodes(deferred_statement_nodes)
  , close_curly(close_curly) {
  }

  // CodeBlockStatement(const CodeBlockStatement& other) 
  // : StatementAstNode(other)
  // , open_curly(other.open_curly)
  // , statement_nodes(other.statement_nodes)
  // , deferred_statement_nodes(other.deferred_statement_nodes)
  // , close_curly(other.close_curly) {
  // }

  static Ref<StatementAstNode> make(Token open_curly, Vec<Ref<StatementAstNode>> statement_nodes, Vec<Ref<StatementAstNode>> deferred_statement_nodes, Token close_curly) {
    return std::make_shared<CodeBlockStatement>(open_curly, statement_nodes, deferred_statement_nodes, close_curly);
  }

  Ref<AstNode> copy() override { return CodeBlockStatement::make(this->open_curly, this->statement_nodes, this->deferred_statement_nodes, this->close_curly); }
  virtual usize source_length() override {
    auto start = open_curly.file_loc.byte_offset;
    auto end = close_curly.file_loc.byte_offset + close_curly.length();
    return end-start;
  }
};

struct VariableDeclaration : public StatementAstNode {
  Token name_token = {};
  bool is_external = false;

  Vec<Token> type_tokens = {};
  Ref<Type> type = nullptr;

  Ref<ExpressionAstNode> value = nullptr;

  VariableDeclaration(Token start_token, Token name, Vec<Token> type_tokens, Ref<ExpressionAstNode> value, bool is_external = false)
  : StatementAstNode(StatementAstNode::Kind::VARIABLE_DECLARATION, start_token)
  , name_token(name)
  , is_external(is_external)
  , type_tokens(type_tokens)
  , value(value) {
  }

  static Ref<VariableDeclaration> make(Token start_token, Token name, Vec<Token> type_tokens, Ref<ExpressionAstNode> value, bool is_external = false) {
    return std::make_shared<VariableDeclaration>(start_token, name, type_tokens, value, is_external);
  }

  Ref<AstNode> copy() override {
    return VariableDeclaration::make(this->start_token, this->name_token, this->type_tokens, this->value, this->is_external);
  }

  virtual usize source_length() override {
    // @Todo: this is not the whole declaration.
    return name_token.length();
  }
};

struct ProcedureDeclaration : public StatementAstNode {
  bool is_external = false;

  Token name_token = {};

  Token open_paren = {};
  Vec<Ref<VariableDeclaration>> arguments = {};
  Token close_paren = {};

  Vec<Token> return_type_tokens = {};

  Ref<Type> type = nullptr;

  Ref<CodeBlockStatement> body = nullptr;

  ProcedureDeclaration(Token name, Vec<Ref<VariableDeclaration>> arguments, Vec<Token> return_type_tokens, Ref<CodeBlockStatement> body, bool is_external = false)
  : StatementAstNode(StatementAstNode::Kind::PROCEDURE_DECLARATION, name)
  , is_external(is_external)
  , name_token(name)
  , arguments(arguments)
  , return_type_tokens(return_type_tokens)
  , body(body) {
    
  }

  static Ref<ProcedureDeclaration> make(Token name, Vec<Ref<VariableDeclaration>> arguments, Vec<Token> return_type_tokens, Ref<CodeBlockStatement> body, bool is_external = false) {
    return std::make_shared<ProcedureDeclaration>(name, arguments, return_type_tokens, body, is_external);
  }

  Ref<AstNode> copy() override {
    return ProcedureDeclaration::make(this->name_token, this->arguments, this->return_type_tokens, this->body, this->is_external);
  }

  virtual usize source_length() override {
    auto start = this->start_token.file_loc.byte_offset;
    usize end = start;

    if (this->return_type_tokens.empty()) end = this->close_paren.file_loc.byte_offset + this->close_paren.length();
    else end = this->return_type_tokens[this->return_type_tokens.size()-1].file_loc.byte_offset + this->return_type_tokens[this->return_type_tokens.size()-1].length();

    return end-start;
  }
};

struct StructDeclaration : public StatementAstNode {
  Token name_token = {};

  bool is_external = false;

  Vec<Ref<VariableDeclaration>> members = {};
  
  Ref<Type> type = nullptr;

  StructDeclaration(Token start_token, Token name, Vec<Ref<VariableDeclaration>> members, bool is_external = false)
  : StatementAstNode(StatementAstNode::Kind::STRUCT_DECLARATION, start_token)
  , name_token(name)
  , is_external(is_external)
  , members(members) {
  }

  static Ref<StructDeclaration> make(Token start_token, Token name, Vec<Ref<VariableDeclaration>> members, bool is_external = false) {
    return std::make_shared<StructDeclaration> (start_token, name, members, is_external);
  }

  Ref<AstNode> copy() override {
    return StructDeclaration::make(this->start_token, this->name_token, this->members, this->is_external);
  }

  virtual usize source_length() override {
    // @Todo: this is not the whole declaration.
    return name_token.length();
  }
};

struct EnumDeclaration : public StatementAstNode {
  Token name_token = {};

  bool is_external = false;

  Vec<Token> type_tokens = {};
  Ref<Type> type = nullptr;

  EnumDeclaration(Token start_token, Token name, Vec<Token> type_tokens, bool is_external = false)
  : StatementAstNode(StatementAstNode::Kind::ENUM_DECLARATION, start_token)
  , name_token(name)
  , is_external(is_external)
  , type_tokens(type_tokens) { 
  }

  static Ref<EnumDeclaration> make(Token start_token, Token name, Vec<Token> type_tokens, bool is_external = false) {
    return std::make_shared<EnumDeclaration> (start_token, name, type_tokens, is_external);
  }

  Ref<AstNode> copy() override {
    return EnumDeclaration::make(this->start_token, this->name_token, this->type_tokens, this->is_external);
  }

  virtual usize source_length() override {
    // @Todo: this is not the whole declaration.
    return name_token.length();
  }
};

struct MatchStatement : public StatementAstNode {
  struct Case {
    Ref<ExpressionAstNode>  value = nullptr;
    Ref<CodeBlockStatement> then  = nullptr;

    Case(Ref<ExpressionAstNode> value, Ref<CodeBlockStatement> then)
    : value(value)
    , then(then) {
    }
  };
  Ref<ExpressionAstNode> match_on = nullptr;
  Ref<CodeBlockStatement> else_case = nullptr;
  Vec<MatchStatement::Case> cases = {};

  MatchStatement(Token start_token, Ref<ExpressionAstNode> match_on, Ref<CodeBlockStatement> else_case, Vec<MatchStatement::Case> cases = {})
  : StatementAstNode(StatementAstNode::Kind::MATCH, start_token) 
  , match_on(match_on)
  , else_case(else_case) 
  , cases(cases) {
  }

  // MatchStatement(const MatchStatement& other) 
  // : StatementAstNode(other)
  // , match_on(other.match_on)
  // , else_case(other.else_case)
  // , cases(other.cases) {
  // }

  static Ref<MatchStatement> make(Token start_token, Ref<ExpressionAstNode> match_on, Ref<CodeBlockStatement> else_case, Vec<MatchStatement::Case> cases = {}) {
    return std::make_shared<MatchStatement>(start_token, match_on, else_case, cases);
  }

  Ref<AstNode> copy() override { return MatchStatement::make(this->start_token, this->match_on, this->else_case, this->cases); }

  virtual usize source_length() override {
    // @Todo: this is not the whole statement.
    return match_on->source_length();
  }
};

struct IfStatement : public StatementAstNode {
  Ref<ExpressionAstNode> condition_node = nullptr;
  Ref<StatementAstNode>  body_node      = nullptr;
  Ref<StatementAstNode>  else_node      = nullptr;

  IfStatement(Token start_token, Ref<ExpressionAstNode> condition_node, Ref<StatementAstNode> body_node, Ref<StatementAstNode> else_node = nullptr)
  : StatementAstNode(StatementAstNode::Kind::IF, start_token) 
  , condition_node(condition_node)
  , body_node(body_node)
  , else_node(else_node) {
  }

  // IfStatement(const IfStatement& other) 
  // : StatementAstNode(other)
  // , condition_node(other.condition_node)
  // , body_node(other.body_node)
  // , else_node(other.else_node) {
  // }

  static Ref<IfStatement> make(Token start_token, Ref<ExpressionAstNode> condition_node, Ref<StatementAstNode> body_node, Ref<StatementAstNode> else_node = nullptr) {
    return std::make_shared<IfStatement>(start_token, condition_node, body_node, else_node);
  }

  Ref<AstNode> copy() override { return IfStatement::make(this->start_token, this->condition_node, this->body_node, this->else_node); }

  virtual usize source_length() override {
    // @Todo: this is not the whole statement.
    return condition_node->source_length();
  }
};

struct WhileLoop : public StatementAstNode {
  Ref<ExpressionAstNode> condition_node = nullptr;
  Ref<StatementAstNode>  body_node      = nullptr;

  WhileLoop(Token start_token, Ref<ExpressionAstNode> condition_node, Ref<StatementAstNode> body_node)
  : StatementAstNode(StatementAstNode::Kind::WHILE, start_token)
  , condition_node(condition_node)
  , body_node(body_node) {
  }

  // WhileLoop(const WhileLoop& other) 
  // : StatementAstNode(other)
  // , condition_node(other.condition_node)
  // , body_node(other.body_node) {
  // }

  static Ref<WhileLoop> make(Token start_token, Ref<ExpressionAstNode> condition_node, Ref<StatementAstNode> body_node) {
    return std::make_shared<WhileLoop>(start_token, condition_node, body_node);
  }

  Ref<AstNode> copy() override { return WhileLoop::make(this->start_token, this->condition_node, this->body_node); }

  virtual usize source_length() override {
    // @Todo: this is not the whole statement.
    return condition_node->source_length();
  }
};

struct ForLoop : public StatementAstNode {
  enum class Kind {
    INVALID,
    RANGED,
    C_STYLE
  };

  ForLoop::Kind         for_statement_kind = ForLoop::Kind::INVALID;
  Ref<StatementAstNode> body_node          = nullptr;

  ForLoop(Token start_token, ForLoop::Kind for_statement_kind, Ref<StatementAstNode> body_node = nullptr)
  : StatementAstNode(StatementAstNode::Kind::FOR, start_token)
  , for_statement_kind(for_statement_kind)
  , body_node(body_node) {
  }

  // ForLoop(const ForLoop& other) 
  // : StatementAstNode(other)
  // , for_statement_kind(other.for_statement_kind)
  // , body_node(other.body_node) {
  // }

  virtual ~ForLoop() = 0;
  virtual Ref<AstNode> copy() = 0;

  virtual usize source_length() = 0;
};

struct RangedForLoop : public ForLoop {
  Ref<ExpressionStatement> range_expression_statement = nullptr;

  RangedForLoop(Token start_token, Ref<ExpressionStatement> range_expression_statement, Ref<StatementAstNode> body_node)
  : ForLoop(start_token, ForLoop::Kind::RANGED, body_node)
  , range_expression_statement(range_expression_statement) {
  }

  // RangedForLoop(const RangedForLoop& other)
  // : ForLoop(other)
  // , range_expression_statement(other.range_expression_statement) {
  // }

  static Ref<RangedForLoop> make(Token start_token, Ref<ExpressionStatement> range_expression_statement, Ref<StatementAstNode> body_node) {
    return std::make_shared<RangedForLoop> (start_token, range_expression_statement, body_node);
  }

  Ref<AstNode> copy() override { return RangedForLoop::make(this->start_token, this->range_expression_statement, this->body_node); }

  virtual usize source_length() override {
    // @Todo: this is not the whole statement.
    return range_expression_statement->source_length();
  }
};

struct CStyleForLoop : public ForLoop {
  Ref<VariableDeclaration> initialisation_node = nullptr; // Ast_Stmt_Decl
  Ref<ExpressionStatement>  condition_node = nullptr;      // Ast_Stmt_Expr
  Ref<ExpressionAstNode>  increment_node = nullptr;      // Ast_Stmt_Expr

  CStyleForLoop(Token start_token, Ref<VariableDeclaration> initialisation_node, Ref<ExpressionStatement> condition_node, Ref<ExpressionAstNode> increment_node, Ref<StatementAstNode> body_node)
  : ForLoop(start_token, ForLoop::Kind::C_STYLE, body_node) 
  , initialisation_node(initialisation_node)
  , condition_node(condition_node)
  , increment_node(increment_node) {
  }

  // CStyleForLoop(const CStyleForLoop& other)
  // : ForLoop(other)
  // , initialisation_node(other.initialisation_node)
  // , condition_node(other.condition_node)
  // , increment_node(other.increment_node) {
  // }

  static Ref<CStyleForLoop> make(Token start_token, Ref<VariableDeclaration> initialisation_node, Ref<ExpressionStatement> condition_node, Ref<ExpressionAstNode> increment_node, Ref<StatementAstNode> body_node) {
    return std::make_shared<CStyleForLoop> (start_token, initialisation_node, condition_node, increment_node, body_node);
  }

  Ref<AstNode> copy() override { return CStyleForLoop::make(this->start_token, this->initialisation_node, this->condition_node, this->increment_node, this->body_node); }

  virtual usize source_length() override {
    // @Todo: this is not the whole statement.
    return initialisation_node->source_length();
  }
};

struct ReturnStatement : public StatementAstNode {
  Ref<ExpressionAstNode> returned_expression = nullptr;

  ReturnStatement(Token start_token, Ref<ExpressionAstNode> returned_expression) 
  : StatementAstNode(StatementAstNode::Kind::RETURN, start_token)
  , returned_expression(returned_expression) {
  }

  // ReturnStatement(const ReturnStatement& other) 
  // : StatementAstNode(other)
  // , returned_expression(other.returned_expression) {
  // }

  static Ref<ReturnStatement> make(Token start_token, Ref<ExpressionAstNode> returned_expression) {
    return std::make_shared<ReturnStatement>(start_token, returned_expression);
  }

  Ref<AstNode> copy() override { return ReturnStatement::make(this->start_token, this->returned_expression); }

  virtual usize source_length() override {
    auto start = this->start_token.file_loc.byte_offset;
    auto end = this->returned_expression->start_token.file_loc.byte_offset - this->returned_expression->source_length();
    return start-end;
  }
};

struct IncludeStatement : public StatementAstNode {
  Ref<AstNode> included_node = nullptr;

  IncludeStatement(Token start_token, Ref<AstNode> included_node) 
  : StatementAstNode(StatementAstNode::Kind::INCLUDE, start_token)
  , included_node(included_node) {
  }

  // IncludeStatement(const IncludeStatement& other)
  // : StatementAstNode(other)
  // , included_node(other.included_node) {
  // }

  static Ref<IncludeStatement> make(Token start_token, Ref<AstNode> included_node) {
    return std::make_shared<IncludeStatement>(start_token, included_node);
  }

  Ref<AstNode> copy() override { return IncludeStatement::make(this->start_token, this->included_node); }

  virtual usize source_length() override {
    auto start = this->start_token.file_loc.byte_offset;
    auto end = this->included_node->start_token.file_loc.byte_offset - this->included_node->source_length();
    return start-end;
  }
};

struct ProgramAstNode : public AstNode {
  Vec<Ref<StatementAstNode>> top_level_statements = {};

  ProgramAstNode(Token start_token, Vec<Ref<StatementAstNode>> top_level_statements = {})
  : AstNode(AstNode::Kind::PROGRAM, start_token)
  , top_level_statements(top_level_statements) {
  }

  // ProgramAstNode(const ProgramAstNode& other)
  // : AstNode(other)
  // , top_level_statements(other.top_level_statements) {
  // }

  static Ref<AstNode> make(Token program_start_token, Vec<Ref<StatementAstNode>> top_level_declarations = {}) {
    return std::make_shared<ProgramAstNode> (program_start_token, top_level_declarations);
  }

  ~ProgramAstNode() { }

  Ref<AstNode> copy() override { return ProgramAstNode::make(this->start_token, this->top_level_statements); }

  virtual usize source_length() override {
    auto start = this->start_token.file_loc.byte_offset;
    auto end = this->top_level_statements[top_level_statements.size()-1]->start_token.file_loc.byte_offset - this->top_level_statements[top_level_statements.size()-1]->source_length();
    return start-end;
  }
};

bool is_statement(Ref<AstNode> node);
bool is_expression(Ref<AstNode> node);



struct Ast {
  Vec<Ref<AstNode>> nodes = {};

  Ref<ProgramAstNode> program_node = nullptr;

  Vec<Ref<TypeAliasStatement>>   type_aliases        = {};
  Vec<Ref<StatementAstNode>>     global_declarations = {};

  Vec<Ref<StructDeclaration>>    struct_defs        = {};  // Populated by typechecker
  Vec<Ref<ProcedureDeclaration>> proc_decls         = {};  // Populated by typechecker
  Vec<Ref<EnumDeclaration>>      enum_declarations  = {};  // Populated by typechecker
  Vec<Ref<VariableDeclaration>>  global_variables   = {};  // Populated by typechecker

  struct IncludeFileInfo {
    bool loaded = false;
    File file = {};
  };

  Table<String, IncludeFileInfo> included_file_infos = {};
};




























///
/// Formatting
///

template <>
struct fmt::formatter<ForLoop::Kind> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(const ForLoop::Kind &for_kind, FormatContext &ctx) {
    auto out = ctx.out();
    switch(for_kind) {
      case ForLoop::Kind::C_STYLE:  fmt::format_to(out, "C-style"); break;
      case ForLoop::Kind::RANGED:   fmt::format_to(out, "ranged"); break;
      case ForLoop::Kind::INVALID:
      default: internal_error("Can't fmt invalid for-loop statement: {}", (int)for_kind);
    }
    return out;
  }
};

template <>
struct fmt::formatter<AstNode::Kind> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(const AstNode::Kind &node_kind, FormatContext &ctx) {
    auto out = ctx.out();
    switch(node_kind) {
      case AstNode::Kind::PROGRAM:      fmt::format_to(out, "program"); break;
      case AstNode::Kind::EXPRESSION:   fmt::format_to(out, "expression"); break;
      case AstNode::Kind::STATEMENT:    fmt::format_to(out, "statement"); break;
      case AstNode::Kind::INVALID:
      default: internal_error("Can't fmt invalid Ast_Node: {}", (int)node_kind);
    }
    return out;
  }
};

template <>
struct fmt::formatter<ExpressionAstNode::Kind> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(const ExpressionAstNode::Kind &expr_kind, FormatContext &ctx) {
    auto out = ctx.out();
    switch(expr_kind) {
      case ExpressionAstNode::Kind::LITERAL:            fmt::format_to(out, "literal"); break;
      case ExpressionAstNode::Kind::GROUPING:           fmt::format_to(out, "grouping"); break;
      case ExpressionAstNode::Kind::CAST:               fmt::format_to(out, "cast"); break;
      case ExpressionAstNode::Kind::BINARY:             fmt::format_to(out, "binary"); break;
      case ExpressionAstNode::Kind::UNARY:              fmt::format_to(out, "unary"); break;
      case ExpressionAstNode::Kind::VARIABLE_REFERENCE: fmt::format_to(out, "variable reference"); break;
      case ExpressionAstNode::Kind::ARRAY_REFERENCE:    fmt::format_to(out, "array reference"); break;
      case ExpressionAstNode::Kind::ARRAY_LITERAL:      fmt::format_to(out, "array literal"); break;
      case ExpressionAstNode::Kind::PROCEDURE_CALL:     fmt::format_to(out, "procedure call"); break;
      case ExpressionAstNode::Kind::ASSIGNMENT:         fmt::format_to(out, "assignment"); break;
      case ExpressionAstNode::Kind::INVALID: //            fmt::format_to(out, "invalid"); break;
      default: internal_error("Can't fmt invalid expression node: {}", (int)expr_kind);
    }
    return out;
  }
};

template <>
struct fmt::formatter<ExpressionAstNode> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(const ExpressionAstNode &expression, FormatContext &ctx) {
    auto out = ctx.out();

    switch (expression.expression_kind) {
    case ExpressionAstNode::Kind::VARIABLE_REFERENCE: {
      auto var_ref = dynamic_cast<const VariableReferenceExpression&>(expression);
      fmt::format_to(out, "{}", var_ref.name);
    } break;

    case ExpressionAstNode::Kind::ARRAY_REFERENCE: {
      auto array_ref = dynamic_cast<const ArrayReferenceExpression&>(expression);
      // fmt::format_to(out, "@Todo: print array references");
      fmt::format_to(out, "{} [{}]", *array_ref.array_expression, *array_ref.index_expression);
    } break;

    case ExpressionAstNode::Kind::ASSIGNMENT: {
      auto assignment = dynamic_cast<const AssignmentExpression&>(expression);
      fmt::format_to(out, "{} {} {}", *assignment.lhs_node, assignment.operatorr.info.name, *assignment.rhs_node);
    } break;

    case ExpressionAstNode::Kind::BINARY: {
      auto binary = dynamic_cast<const BinaryExpression&>(expression);
      fmt::format_to(out, "{} {} {}", *binary.lhs_expression, binary.operatorr.info.name, *binary.rhs_expression);
    } break;

    case ExpressionAstNode::Kind::UNARY: {
      auto unary = dynamic_cast<const UnaryExpression&>(expression);
      fmt::format_to(out, "{} {}", unary.operatorr.info.name, *unary.operand_node);
    } break;

    case ExpressionAstNode::Kind::LITERAL: {
      auto literal = dynamic_cast<const LiteralExpression&>(expression);
      switch (literal.literal_kind) {
      case LiteralExpression::Kind::BOOLEAN: fmt::format_to(out, "{}",   literal.literal_token.contents); break;
      case LiteralExpression::Kind::U64:     fmt::format_to(out, "{}",   literal.literal_token.contents); break;
      case LiteralExpression::Kind::F64:     fmt::format_to(out, "{}",   literal.literal_token.contents); break;
      case LiteralExpression::Kind::STRING:  fmt::format_to(out, "{}",   literal.literal_token.contents); break;
      case LiteralExpression::Kind::INVALID:
      default: internal_error("Can't fmt invalid literal expression node: {}", (int)literal.literal_kind); break;
      }
    } break;

    case ExpressionAstNode::Kind::ARRAY_LITERAL: {
      auto array_literal = dynamic_cast<const ArrayLiteralExpression&>(expression);
      fmt::format_to(out, "[ ");
      for (size_t i = 0; i < array_literal.element_nodes.size(); ++i) {
        fmt::format_to(out, "{}", *array_literal.element_nodes[i]);
        if (i != array_literal.element_nodes.size() - 1) fmt::format_to(out, ", ");
      }
      fmt::format_to(out, " ]");
    } break;

    case ExpressionAstNode::Kind::GROUPING: {
      auto grouping = dynamic_cast<const GroupingExpression&>(expression);
      fmt::format_to(out, "( {} )", *grouping.grouped_expression);
    } break;

    case ExpressionAstNode::Kind::PROCEDURE_CALL: {
      auto proc_call = dynamic_cast<const ProcedureCallExpression&>(expression);
      fmt::format_to(out, "{}(", *proc_call.called_node);
      for (size_t i = 0; i < proc_call.argument_nodes.size(); ++i) {
        fmt::format_to(out, "{}", *proc_call.argument_nodes[i]);
        if (i != proc_call.argument_nodes.size() - 1) fmt::format_to(out, ", ");
      }
      fmt::format_to(out, ")");
    } break;

    case ExpressionAstNode::Kind::CAST: {
      auto cast = dynamic_cast<const CastExpression&>(expression);
      fmt::format_to(out, "{} as {}", *cast.casted_node, cast.type);
    } break;

    case ExpressionAstNode::Kind::INVALID: 
    default: internal_error("Can't fmt invalid expression node: {}", (int)expression.expression_kind); break;
    }
    return out;
  }
};

template <>
struct fmt::formatter<StatementAstNode::Kind> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(const StatementAstNode::Kind &stmt_kind, FormatContext &ctx) {
    auto out = ctx.out();
    switch(stmt_kind) {
    case StatementAstNode::Kind::INVALID:     fmt::format_to(out, "invalid"); break;
    case StatementAstNode::Kind::CODE_BLOCK:  fmt::format_to(out, "code block"); break;
    case StatementAstNode::Kind::EXPRESSION:  fmt::format_to(out, "expression"); break;
    case StatementAstNode::Kind::IF:          fmt::format_to(out, "if"); break;
    case StatementAstNode::Kind::FOR:         fmt::format_to(out, "for"); break;
    case StatementAstNode::Kind::BREAK:         fmt::format_to(out, "break"); break;
    case StatementAstNode::Kind::WHILE:       fmt::format_to(out, "while"); break;
    case StatementAstNode::Kind::RETURN:      fmt::format_to(out, "return"); break;
    case StatementAstNode::Kind::PROCEDURE_DECLARATION: fmt::format_to(out, "procedure declaration"); break;
    case StatementAstNode::Kind::VARIABLE_DECLARATION: fmt::format_to(out, "variable declaration"); break;
    case StatementAstNode::Kind::STRUCT_DECLARATION: fmt::format_to(out, "struct declaration"); break;
    case StatementAstNode::Kind::ENUM_DECLARATION: fmt::format_to(out, "enum declaration"); break;
    case StatementAstNode::Kind::TYPE_ALIAS:  fmt::format_to(out, "type alias"); break;
    case StatementAstNode::Kind::INCLUDE:     fmt::format_to(out, "include"); break;
    case StatementAstNode::Kind::DEFER:       fmt::format_to(out, "defer"); break;
    case StatementAstNode::Kind::MATCH:       fmt::format_to(out, "match"); break;
    default: internal_error("Can't fmt invalid statement node: {}", (int)stmt_kind); break;
    }
    // fmt.format_to(out, " statement");
    return out;
  }
};

template <> struct fmt::formatter<Ref<AstNode>> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(const Ref<AstNode> &node, FormatContext &ctx) {
    auto out = ctx.out();

    fmt::format_to(out, "{} ", node->start_token.file_loc);

    switch (node->node_kind) {
    case AstNode::Kind::EXPRESSION: {
      auto expr = std::dynamic_pointer_cast<ExpressionAstNode> (node);
      fmt::format_to(out, "{}", expr);
     } break;

    case AstNode::Kind::STATEMENT: {
      auto statement = std::dynamic_pointer_cast<StatementAstNode> (node);

      switch (statement->statement_kind) {
      case StatementAstNode::Kind::BREAK: {
        auto break_stmt = std::dynamic_pointer_cast<BreakStatement> (statement);
        fmt::format_to(out, "break;");
      } break;
      
      case StatementAstNode::Kind::CODE_BLOCK: {
        auto scope = std::dynamic_pointer_cast<CodeBlockStatement>(statement);
        fmt::format_to(out, "{{\n");
        for (size_t i = 0; i < scope->statement_nodes.size(); ++i) {
          fmt::format_to(out, "{}\n", scope->statement_nodes[i]);
        }
        fmt::format_to(out, "}}");
      } break;

      case StatementAstNode::Kind::EXPRESSION: {
        auto expr_stmt = std::dynamic_pointer_cast<ExpressionStatement>(statement);
        fmt::format_to(out, "{}", expr_stmt->expression_node);
      } break;
      
      case StatementAstNode::Kind::IF: {
        auto iff = std::dynamic_pointer_cast<IfStatement>(statement);
        fmt::format_to(out, "if ( {} ) {}", iff->condition_node, iff->body_node);
        if (iff->else_node)
          fmt::format_to(out, "else {}", iff->condition_node, iff->body_node);
      } break;

      case StatementAstNode::Kind::MATCH: {
        auto match_statement = std::dynamic_pointer_cast<MatchStatement>(statement);
        fmt::format_to(out, "match {} {{", match_statement->match_on);
        for (auto& match_case : match_statement->cases) {
          fmt::format_to(out, "  is {} {}", match_case.value, match_case.then);
        }

        fmt::format_to(out, "else {}", match_statement->else_case);

        fmt::format_to(out, "}}");
      } break;

      case StatementAstNode::Kind::WHILE: {
        auto whilee = std::dynamic_pointer_cast<WhileLoop>(statement);
        fmt::format_to(out, "while ( {} ) {}", whilee->condition_node, whilee->body_node);
      } break;

      case StatementAstNode::Kind::FOR: {
        auto forr = std::dynamic_pointer_cast<ForLoop>(statement);
        fmt::format_to(out, "For ( @Todo: show the type of the for statement )");
      } break;

      case StatementAstNode::Kind::RETURN: {
        auto returnn = std::dynamic_pointer_cast<ReturnStatement>(statement);
        fmt::format_to(out, "return {}", returnn->returned_expression);
      } break;

      case StatementAstNode::Kind::VARIABLE_DECLARATION: {
        auto declaration = std::dynamic_pointer_cast<VariableDeclaration>(statement);
        fmt::format_to(out, "Variable Declaration ( `{}` )", declaration->name_token.contents);
      } break;

      case StatementAstNode::Kind::PROCEDURE_DECLARATION: {
        auto declaration = std::dynamic_pointer_cast<ProcedureDeclaration>(statement);
        fmt::format_to(out, "Procedure Declaration ( `{}` )", declaration->name_token.contents);
      } break;

      case StatementAstNode::Kind::ENUM_DECLARATION: {
        auto declaration = std::dynamic_pointer_cast<EnumDeclaration>(statement);
        fmt::format_to(out, "Enum Declaration ( `{}` )", declaration->name_token.contents);
      } break;

      case StatementAstNode::Kind::STRUCT_DECLARATION: {
        auto declaration = std::dynamic_pointer_cast<StructDeclaration>(statement);
        fmt::format_to(out, "Struct Declaration ( `{}` )", declaration->name_token.contents);
      } break;

      case StatementAstNode::Kind::TYPE_ALIAS: {
        auto type_alias = std::dynamic_pointer_cast<TypeAliasStatement>(statement);
        fmt::format_to(out, "Type_Alias ( `{}` )", type_alias->name.contents);
      } break;

      case StatementAstNode::Kind::DEFER: {
        auto defer = std::dynamic_pointer_cast<DeferStatement>(statement);
        fmt::format_to(out, "defer {{ {} }}", defer->deferred_node);
      } break;

      case StatementAstNode::Kind::INCLUDE: {
        auto include_statement = std::dynamic_pointer_cast<IncludeStatement>(statement);
        fmt::format_to(out, "include {}", include_statement->included_node);
      } break;

      case StatementAstNode::Kind::INVALID:
      default:  internal_error("Can't fmt invalid statement {}", (int)statement->statement_kind); break;
      }
    } break;

    case AstNode::Kind::PROGRAM: {
      fmt::format_to(out, "{}", "Program");
    } break;

    case AstNode::Kind::INVALID:
    default:  internal_error("Can't fmt invalid node kind {}", (int)node->node_kind); break;
    }

    return out;
  }
};






template <typename S, typename... Args>
void dragon_annotated_source(Token token, const File& source_file, const S& message, Args &&...args) {
  auto start_line = [](u64 line_number, bool show_line_number) {
    const std::string line_number_separator = "|";
    const usize       indent_size           = 2;
    const auto        indent_string         = std::string(indent_size, ' ');

    if (show_line_number)
      fmt::print("{:>4} {}{}", line_number, line_number_separator, indent_string);
    else
      fmt::print("     {}{}", std::string(line_number_separator.length(), ' '), indent_string);
  };

  const auto lines_above_error_to_show = 2;

  auto& annotation_file_loc = token.file_loc;
  auto& source = source_file.data;

  // Find the character index at the start of the error message code output
  usize character_index_at_start_of_source_output = annotation_file_loc.byte_offset;
  usize line_number_at_start_of_source_output = (annotation_file_loc.row - lines_above_error_to_show) > 0 ? (annotation_file_loc.row - lines_above_error_to_show) : 0;

  for (auto newlines_found_so_far = 0;
       newlines_found_so_far <= lines_above_error_to_show && character_index_at_start_of_source_output > 0; ) {
    character_index_at_start_of_source_output -= 1;
    if (source[character_index_at_start_of_source_output] == '\n')  newlines_found_so_far += 1;
  }

  if (character_index_at_start_of_source_output < 0) character_index_at_start_of_source_output = 0;

  for (auto line_number = line_number_at_start_of_source_output, current_character_index = character_index_at_start_of_source_output;
       current_character_index < annotation_file_loc.byte_offset;
       ++current_character_index) {
    fmt::print("{}", source[current_character_index]);

    if (source[current_character_index] == '\n') {
      start_line(line_number, true);
      line_number++;
    }
  }

  auto highlighted_source_length = token.length();
  auto current_character_index = annotation_file_loc.byte_offset;
  
  for (auto line_number = annotation_file_loc.row;
       current_character_index < annotation_file_loc.byte_offset + highlighted_source_length;
       ++current_character_index) {
    fmt::print(fmt::emphasis::bold | fmt::fg(log_error_color),    "{}", source[current_character_index]);

    if (source[current_character_index] == '\n') {
      start_line(line_number, true);
      line_number++;
    }
  }

  for ( ; source[current_character_index] != '\n' && current_character_index < source_file.size; ++current_character_index) {
    fmt::print("{}", source[current_character_index]);
  }
  fmt::print("\n");  ++current_character_index;

  start_line(0, false);
  fmt::print("{}", std::string(annotation_file_loc.col, ' ')); // Move "cursor" to the column of the anotation.
  fmt::print(fmt::emphasis::bold | fmt::fg(log_error_color),  "^ ");
  fmt::print(fmt::emphasis::bold | fmt::fg(log_error_color),  fmt::vformat(message, fmt::make_format_args(args...)));
  fmt::print("\n");

  for (usize newlines_so_far = 0, line_number = annotation_file_loc.row+1;
       newlines_so_far < lines_above_error_to_show && current_character_index < source_file.size;
       ++current_character_index) {
    fmt::print("{}", source[current_character_index]);

    if (source[current_character_index] == '\n') {
      start_line(line_number, true);
      line_number++;  newlines_so_far++;
    }
  }

  fmt::print("\n");
}

template <typename S, typename... Args>
void dragon_annotated_source(Ref<AstNode> node, const File& source_file, const S& message, Args &&...args) {
  auto start_line = [](u64 line_number, bool show_line_number) {
    const std::string line_number_separator = "|";
    const usize       indent_size           = 2;
    const auto        indent_string         = std::string(indent_size, ' ');

    if (show_line_number)
      fmt::print("{:>4} {}{}", line_number, line_number_separator, indent_string);
    else
      fmt::print("     {}{}", std::string(line_number_separator.length(), ' '), indent_string);
  };

  const auto lines_above_error_to_show = 2;

  auto& annotation_file_loc = node->start_token.file_loc;
  auto& source = source_file.data;

  // Find the character index at the start of the error message code output
  usize character_index_at_start_of_source_output = annotation_file_loc.byte_offset;
  usize line_number_at_start_of_source_output = (annotation_file_loc.row - lines_above_error_to_show) > 0 ? (annotation_file_loc.row - lines_above_error_to_show) : 0;

  for (auto newlines_found_so_far = 0;
       newlines_found_so_far <= lines_above_error_to_show && character_index_at_start_of_source_output > 0; ) {
    character_index_at_start_of_source_output -= 1;
    if (source[character_index_at_start_of_source_output] == '\n')  newlines_found_so_far += 1;
  }

  if (character_index_at_start_of_source_output < 0) character_index_at_start_of_source_output = 0;

  for (auto line_number = line_number_at_start_of_source_output, current_character_index = character_index_at_start_of_source_output;
       current_character_index < annotation_file_loc.byte_offset;
       ++current_character_index) {
    fmt::print("{}", source[current_character_index]);

    if (source[current_character_index] == '\n') {
      start_line(line_number, true);
      line_number++;
    }
  }

  auto highlighted_source_length = node->source_length();
  auto current_character_index = annotation_file_loc.byte_offset;
  
  for (auto line_number = annotation_file_loc.row;
       current_character_index < annotation_file_loc.byte_offset + highlighted_source_length;
       ++current_character_index) {
    fmt::print(fmt::emphasis::bold | fmt::fg(log_error_color),    "{}", source[current_character_index]);

    if (source[current_character_index] == '\n') {
      start_line(line_number, true);
      line_number++;
    }
  }

  for ( ; source[current_character_index] != '\n' && current_character_index < source_file.size; ++current_character_index) {
    fmt::print("{}", source[current_character_index]);
  }
  fmt::print("\n");  ++current_character_index;

  start_line(0, false);
  fmt::print("{}", std::string(annotation_file_loc.col, ' ')); // Move "cursor" to the column of the anotation.
  fmt::print(fmt::emphasis::bold | fmt::fg(log_error_color),  "^ ");
  fmt::print(fmt::emphasis::bold | fmt::fg(log_error_color),  fmt::vformat(message, fmt::make_format_args(args...)));
  fmt::print("\n");

  auto line_number = annotation_file_loc.row+1;
  start_line(line_number, true);
  for (usize newlines_so_far = 0;
       newlines_so_far < lines_above_error_to_show && current_character_index < source_file.size;
       ++current_character_index) {
    fmt::print("{}", source[current_character_index]);

    if (source[current_character_index] == '\n') {
      start_line(line_number, true);
      line_number++;  newlines_so_far++;
    }
  }

  fmt::print("\n");
}







#endif // DRAGON_AST_H_
