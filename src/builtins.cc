
#include "builtins.h"
#include "logging.h"

extern Token make_builtin_token(StringView contents);

bool is_builtin_type_name(StringView needle) {
  return (global_builtins.type_names.find(needle) != global_builtins.type_names.end());
}

StringView builtin_type_name(BuiltinType::Kind needle) {
  for (auto &[name, type] : global_builtins.type_names) {
    if (type->kind != Type::Kind::BUILTIN)  continue;

    auto builtin_type = std::dynamic_pointer_cast<BuiltinType> (type);
    if (builtin_type->builtin_kind == needle)  return name;
  }

  internal_error("Builtin_Type::Kind {} has not been defined in the builtin_type_names lookup table.", (s32)needle);
  return "invalid builtin kind";
}

void init(Builtins& builtins) {
#if DRAGON_WINDOWS
  builtin_include_paths.insert(std::end(builtin_include_paths), std::begin(windows_include_paths), std::end(windows_include_paths));

  builtin_library_paths.insert(std::end(builtin_library_paths), std::begin(windows_library_paths), std::end(windows_library_paths));
#endif

  builtins.include_paths.push_back(builtins.sdl_include_path);
  builtins.include_paths.push_back(builtins.fmt_include_path);

  builtins.library_paths.push_back(builtins.sdl_library_path);

  builtins.type_names["void"]        = BuiltinType::make(BuiltinType::Kind::V0ID);
  builtins.type_names["u64"]         = BuiltinType::make(BuiltinType::Kind::U64);
  builtins.type_names["u32"]         = BuiltinType::make(BuiltinType::Kind::U32);
  builtins.type_names["u16"]         = BuiltinType::make(BuiltinType::Kind::U16);
  builtins.type_names["u8"]          = BuiltinType::make(BuiltinType::Kind::U8);
  builtins.type_names["s64"]         = BuiltinType::make(BuiltinType::Kind::S64);
  builtins.type_names["s32"]         = BuiltinType::make(BuiltinType::Kind::S32);
  builtins.type_names["s16"]         = BuiltinType::make(BuiltinType::Kind::S16);
  builtins.type_names["s8"]          = BuiltinType::make(BuiltinType::Kind::S8);
  builtins.type_names["f64"]         = BuiltinType::make(BuiltinType::Kind::F64);
  builtins.type_names["f32"]         = BuiltinType::make(BuiltinType::Kind::F32);
  builtins.type_names["bool"]        = BuiltinType::make(BuiltinType::Kind::BOOLEAN);
  builtins.type_names["pointer"]     = PointerType::make(BuiltinType::make(BuiltinType::Kind::V0ID));
  builtins.type_names["string"]      = BuiltinType::make(BuiltinType::Kind::STRING);
  builtins.type_names["type_info"]   = TypeInfoType::make(make_builtin_token("type_info"));

  builtins.keywords["alias"]    = TokenKind::ALIAS;
  builtins.keywords["as"]       = TokenKind::AS;
  builtins.keywords["break"]    = TokenKind::BREAK;
  builtins.keywords["defer"]    = TokenKind::DEFER;
  builtins.keywords["else"]     = TokenKind::ELSE;
  builtins.keywords["enum"]     = TokenKind::ENUM;
  builtins.keywords["external"] = TokenKind::EXTERNAL;
  builtins.keywords["false"]    = TokenKind::FALSE;
  builtins.keywords["for"]      = TokenKind::FOR;
  builtins.keywords["if"]       = TokenKind::IF;
  builtins.keywords["is"]       = TokenKind::IS;
  builtins.keywords["include"]  = TokenKind::INCLUDE;
  builtins.keywords["match"]    = TokenKind::MATCH;
  builtins.keywords["return"]   = TokenKind::RETURN;
  builtins.keywords["struct"]   = TokenKind::STRUCT;
  builtins.keywords["true"]     = TokenKind::TRUE;
  builtins.keywords["while"]    = TokenKind::WHILE;

  builtins.token_infos.insert({TokenKind::INVALID,          TokenInfo(TokenKind::INVALID,         0,                                                                          "invalid")});
  builtins.token_infos.insert({TokenKind::ALIAS,            TokenInfo(TokenKind::ALIAS,           TOKEN_FLAG_KEYWORD,                                                         "alias")});
  builtins.token_infos.insert({TokenKind::IF,               TokenInfo(TokenKind::IF,              TOKEN_FLAG_KEYWORD,                                                         "if")});
  builtins.token_infos.insert({TokenKind::IS,               TokenInfo(TokenKind::IS,              TOKEN_FLAG_KEYWORD,                                                         "is")});
  builtins.token_infos.insert({TokenKind::AS,               TokenInfo(TokenKind::AS,              TOKEN_FLAG_KEYWORD | TOKEN_FLAG_BINARY_OPERATOR,                            "as")});
  builtins.token_infos.insert({TokenKind::BREAK,            TokenInfo(TokenKind::BREAK,           TOKEN_FLAG_KEYWORD,                                                         "break")});
  builtins.token_infos.insert({TokenKind::ELSE,             TokenInfo(TokenKind::ELSE,            TOKEN_FLAG_KEYWORD,                                                         "else")});
  builtins.token_infos.insert({TokenKind::ENUM,             TokenInfo(TokenKind::ENUM,            TOKEN_FLAG_KEYWORD,                                                         "enum")});
  builtins.token_infos.insert({TokenKind::WHILE,            TokenInfo(TokenKind::WHILE,           TOKEN_FLAG_KEYWORD,                                                         "while")});
  builtins.token_infos.insert({TokenKind::FOR,              TokenInfo(TokenKind::FOR,             TOKEN_FLAG_KEYWORD,                                                         "for")});
  builtins.token_infos.insert({TokenKind::RETURN,           TokenInfo(TokenKind::RETURN,          TOKEN_FLAG_KEYWORD,                                                         "return")});
  builtins.token_infos.insert({TokenKind::INCLUDE,          TokenInfo(TokenKind::INCLUDE,         TOKEN_FLAG_KEYWORD,                                                         "include")});
  builtins.token_infos.insert({TokenKind::MATCH,            TokenInfo(TokenKind::MATCH,           TOKEN_FLAG_KEYWORD,                                                        "match")});
  builtins.token_infos.insert({TokenKind::STRUCT,           TokenInfo(TokenKind::STRUCT,          TOKEN_FLAG_KEYWORD,                                                         "struct")});
  builtins.token_infos.insert({TokenKind::EXTERNAL,         TokenInfo(TokenKind::EXTERNAL,        TOKEN_FLAG_KEYWORD,                                                         "external")});
  builtins.token_infos.insert({TokenKind::DEFER,            TokenInfo(TokenKind::DEFER,           TOKEN_FLAG_KEYWORD,                                                         "defer")});
  builtins.token_infos.insert({TokenKind::TRUE,             TokenInfo(TokenKind::TRUE,            TOKEN_FLAG_KEYWORD,                                                         "true")});
  builtins.token_infos.insert({TokenKind::FALSE,            TokenInfo(TokenKind::FALSE,           TOKEN_FLAG_KEYWORD,                                                         "false")});
  builtins.token_infos.insert({TokenKind::EQ,               TokenInfo(TokenKind::EQ,              TOKEN_FLAG_SYMBOL | TOKEN_FLAG_ASSIGNMENT,                                  "=")});
  builtins.token_infos.insert({TokenKind::OPEN_PAREN,       TokenInfo(TokenKind::OPEN_PAREN,      TOKEN_FLAG_SYMBOL,                                                          "(")});
  builtins.token_infos.insert({TokenKind::CLOSE_PAREN,      TokenInfo(TokenKind::CLOSE_PAREN,     TOKEN_FLAG_SYMBOL,                                                          ")")});
  builtins.token_infos.insert({TokenKind::OPEN_CURLY,       TokenInfo(TokenKind::OPEN_CURLY,      TOKEN_FLAG_SYMBOL,                                                          "{")});
  builtins.token_infos.insert({TokenKind::CLOSE_CURLY,      TokenInfo(TokenKind::CLOSE_CURLY,     TOKEN_FLAG_SYMBOL,                                                          "}")});
  builtins.token_infos.insert({TokenKind::OPEN_SQUARE,      TokenInfo(TokenKind::OPEN_SQUARE,     TOKEN_FLAG_SYMBOL,                                                          "[")});
  builtins.token_infos.insert({TokenKind::CLOSE_SQUARE,     TokenInfo(TokenKind::CLOSE_SQUARE,    TOKEN_FLAG_SYMBOL,                                                          "]")});
  builtins.token_infos.insert({TokenKind::COMMA,            TokenInfo(TokenKind::COMMA,           TOKEN_FLAG_SYMBOL,                                                          ",")});
  builtins.token_infos.insert({TokenKind::SEMI,             TokenInfo(TokenKind::SEMI,            TOKEN_FLAG_SYMBOL,                                                          ";")});
  builtins.token_infos.insert({TokenKind::COLON,            TokenInfo(TokenKind::COLON,           TOKEN_FLAG_SYMBOL,                                                          ":")});
  builtins.token_infos.insert({TokenKind::EQEQ,             TokenInfo(TokenKind::EQEQ,            TOKEN_FLAG_COMPARATOR | TOKEN_FLAG_BINARY_OPERATOR | TOKEN_FLAG_SYMBOL,     "==")});
  builtins.token_infos.insert({TokenKind::NOTEQ,            TokenInfo(TokenKind::NOTEQ,           TOKEN_FLAG_COMPARATOR | TOKEN_FLAG_BINARY_OPERATOR | TOKEN_FLAG_SYMBOL,     "!=")});
  builtins.token_infos.insert({TokenKind::LT,               TokenInfo(TokenKind::LT,              TOKEN_FLAG_COMPARATOR | TOKEN_FLAG_BINARY_OPERATOR | TOKEN_FLAG_SYMBOL,     "<")});
  builtins.token_infos.insert({TokenKind::GT,               TokenInfo(TokenKind::GT,              TOKEN_FLAG_COMPARATOR | TOKEN_FLAG_BINARY_OPERATOR | TOKEN_FLAG_SYMBOL,     ">")});
  builtins.token_infos.insert({TokenKind::LE,               TokenInfo(TokenKind::LE,              TOKEN_FLAG_COMPARATOR | TOKEN_FLAG_BINARY_OPERATOR | TOKEN_FLAG_SYMBOL,     "<=")});
  builtins.token_infos.insert({TokenKind::GE,               TokenInfo(TokenKind::GE,              TOKEN_FLAG_COMPARATOR | TOKEN_FLAG_BINARY_OPERATOR | TOKEN_FLAG_SYMBOL,     ">=")});
  builtins.token_infos.insert({TokenKind::ADD,              TokenInfo(TokenKind::ADD,             TOKEN_FLAG_BINARY_OPERATOR | TOKEN_FLAG_SYMBOL,                             "+")});
  builtins.token_infos.insert({TokenKind::SUB,              TokenInfo(TokenKind::SUB,             TOKEN_FLAG_BINARY_OPERATOR | TOKEN_FLAG_UNARY_OPERATOR | TOKEN_FLAG_SYMBOL, "-")});
  builtins.token_infos.insert({TokenKind::MUL,              TokenInfo(TokenKind::MUL,             TOKEN_FLAG_BINARY_OPERATOR | TOKEN_FLAG_SYMBOL,                             "*")});
  builtins.token_infos.insert({TokenKind::DIV,              TokenInfo(TokenKind::DIV,             TOKEN_FLAG_BINARY_OPERATOR | TOKEN_FLAG_SYMBOL,                             "/")});
  builtins.token_infos.insert({TokenKind::MOD,              TokenInfo(TokenKind::MOD,             TOKEN_FLAG_BINARY_OPERATOR | TOKEN_FLAG_SYMBOL,                             "%")});
  builtins.token_infos.insert({TokenKind::DOT,              TokenInfo(TokenKind::DOT,             TOKEN_FLAG_BINARY_OPERATOR | TOKEN_FLAG_SYMBOL,                             ".")});
  builtins.token_infos.insert({TokenKind::DOTDOT,           TokenInfo(TokenKind::DOTDOT,          TOKEN_FLAG_BINARY_OPERATOR | TOKEN_FLAG_SYMBOL,                             "..")});
  builtins.token_infos.insert({TokenKind::OR,               TokenInfo(TokenKind::OR,              TOKEN_FLAG_BINARY_OPERATOR | TOKEN_FLAG_SYMBOL,                             "|")});
  builtins.token_infos.insert({TokenKind::AND,              TokenInfo(TokenKind::AND,             TOKEN_FLAG_BINARY_OPERATOR | TOKEN_FLAG_SYMBOL,                             "&")});
  builtins.token_infos.insert({TokenKind::OROR,             TokenInfo(TokenKind::OROR,            TOKEN_FLAG_BINARY_OPERATOR | TOKEN_FLAG_SYMBOL,                             "||")});
  builtins.token_infos.insert({TokenKind::ANDAND,           TokenInfo(TokenKind::ANDAND,          TOKEN_FLAG_BINARY_OPERATOR | TOKEN_FLAG_SYMBOL,                             "&&")});
  builtins.token_infos.insert({TokenKind::ADD_EQ,           TokenInfo(TokenKind::ADD_EQ,          TOKEN_FLAG_ASSIGNMENT | TOKEN_FLAG_SYMBOL,                                  "+=")});
  builtins.token_infos.insert({TokenKind::SUB_EQ,           TokenInfo(TokenKind::SUB_EQ,          TOKEN_FLAG_ASSIGNMENT | TOKEN_FLAG_SYMBOL,                                  "-=")});
  builtins.token_infos.insert({TokenKind::MUL_EQ,           TokenInfo(TokenKind::MUL_EQ,          TOKEN_FLAG_ASSIGNMENT | TOKEN_FLAG_SYMBOL,                                  "*=")});
  builtins.token_infos.insert({TokenKind::DIV_EQ,           TokenInfo(TokenKind::DIV_EQ,          TOKEN_FLAG_ASSIGNMENT | TOKEN_FLAG_SYMBOL,                                  "/=")});
  builtins.token_infos.insert({TokenKind::MOD_EQ,           TokenInfo(TokenKind::MOD_EQ,          TOKEN_FLAG_ASSIGNMENT | TOKEN_FLAG_SYMBOL,                                  "%=")});
  builtins.token_infos.insert({TokenKind::AND_EQ,           TokenInfo(TokenKind::AND_EQ,          TOKEN_FLAG_ASSIGNMENT | TOKEN_FLAG_SYMBOL,                                  "&=")});
  builtins.token_infos.insert({TokenKind::OR_EQ,            TokenInfo(TokenKind::OR_EQ,           TOKEN_FLAG_ASSIGNMENT | TOKEN_FLAG_SYMBOL,                                  "|=")});
  builtins.token_infos.insert({TokenKind::BANG,             TokenInfo(TokenKind::BANG,            TOKEN_FLAG_UNARY_OPERATOR | TOKEN_FLAG_SYMBOL,                              "!")});
  builtins.token_infos.insert({TokenKind::POINTER,          TokenInfo(TokenKind::POINTER,         TOKEN_FLAG_UNARY_OPERATOR | TOKEN_FLAG_SYMBOL,                              "^")});
  builtins.token_infos.insert({TokenKind::DEREFERENCE,      TokenInfo(TokenKind::DEREFERENCE,     TOKEN_FLAG_UNARY_OPERATOR | TOKEN_FLAG_SYMBOL,                              "@")});
  builtins.token_infos.insert({TokenKind::IDENTIFIER,       TokenInfo(TokenKind::IDENTIFIER,      0,                                                                          "identifier")});
  builtins.token_infos.insert({TokenKind::STRING_LITERAL,   TokenInfo(TokenKind::STRING_LITERAL,  0,                                                                          "string literal")});
  builtins.token_infos.insert({TokenKind::INTEGER_LITERAL,  TokenInfo(TokenKind::INTEGER_LITERAL, 0,                                                                          "integer literal")});
  builtins.token_infos.insert({TokenKind::FLOAT_LITERAL,    TokenInfo(TokenKind::FLOAT_LITERAL,   0,                                                                          "float literal")});
  builtins.token_infos.insert({TokenKind::END_OF_FILE,      TokenInfo(TokenKind::END_OF_FILE,     TOKEN_FLAG_SYMBOL,                                                          "eof")});

  if (builtins.token_infos.size() != token_kind_count)
    internal_error("{} Token_Kinds are missing an entry in the Token_Info lookup table.", token_kind_count - builtins.token_infos.size());

  builtins.binary_operator_tokens.push_back(TokenKind::EQEQ);
  builtins.binary_operator_tokens.push_back(TokenKind::NOTEQ);
  builtins.binary_operator_tokens.push_back(TokenKind::LT);
  builtins.binary_operator_tokens.push_back(TokenKind::GT);
  builtins.binary_operator_tokens.push_back(TokenKind::LE);
  builtins.binary_operator_tokens.push_back(TokenKind::GE);
  builtins.binary_operator_tokens.push_back(TokenKind::DOTDOT);
  builtins.binary_operator_tokens.push_back(TokenKind::ADD);
  builtins.binary_operator_tokens.push_back(TokenKind::SUB);
  builtins.binary_operator_tokens.push_back(TokenKind::MUL);
  builtins.binary_operator_tokens.push_back(TokenKind::DIV);
  builtins.binary_operator_tokens.push_back(TokenKind::MOD);
  builtins.binary_operator_tokens.push_back(TokenKind::OR);
  builtins.binary_operator_tokens.push_back(TokenKind::OROR);
  builtins.binary_operator_tokens.push_back(TokenKind::AND);
  builtins.binary_operator_tokens.push_back(TokenKind::ANDAND);

  builtins.integral_types.push_back(BuiltinType::make(BuiltinType::Kind::U64));
  builtins.integral_types.push_back(BuiltinType::make(BuiltinType::Kind::U32));
  builtins.integral_types.push_back(BuiltinType::make(BuiltinType::Kind::U16));
  builtins.integral_types.push_back(BuiltinType::make(BuiltinType::Kind::U8));
  builtins.integral_types.push_back(BuiltinType::make(BuiltinType::Kind::S64));
  builtins.integral_types.push_back(BuiltinType::make(BuiltinType::Kind::S32));
  builtins.integral_types.push_back(BuiltinType::make(BuiltinType::Kind::S16));
  builtins.integral_types.push_back(BuiltinType::make(BuiltinType::Kind::S8));

  for (size_t i = 0; i < builtins.binary_operator_tokens.size(); ++i) {
    auto op = builtins.binary_operator_tokens[i];

    for (size_t j = 0; j < builtins.integral_types.size(); ++j) {
      for (size_t k = 0; k < builtins.integral_types.size(); ++k) {
        auto t1 = builtins.integral_types[j];
        auto t2 = builtins.integral_types[k];

        auto op_info = builtins.token_infos[op];

        if (TOKEN_FLAG_COMPARATOR & op_info.flags) {
          builtins.binary_operations.push_back(Binary_Operation(op, t1, t2, BuiltinType::make(BuiltinType::Kind::BOOLEAN)));
          builtins.binary_operations.push_back(Binary_Operation(op, t2, t2, BuiltinType::make(BuiltinType::Kind::BOOLEAN)));
        }
        else {
          builtins.binary_operations.push_back(Binary_Operation(op, t1, t2, t1));
          builtins.binary_operations.push_back(Binary_Operation(op, t2, t1, t2));
        }
      }
    }

    if (op == TokenKind::EQ || op == TokenKind::NOTEQ) {
      builtins.binary_operations.push_back(Binary_Operation(op, BuiltinType::make(BuiltinType::Kind::BOOLEAN), BuiltinType::make(BuiltinType::Kind::BOOLEAN), BuiltinType::make(BuiltinType::Kind::BOOLEAN)));
      builtins.binary_operations.push_back(Binary_Operation(op, BuiltinType::make(BuiltinType::Kind::STRING),  BuiltinType::make(BuiltinType::Kind::STRING),  BuiltinType::make(BuiltinType::Kind::BOOLEAN)));
    }

    if (op == TokenKind::ANDAND || op == TokenKind::OROR) {
      builtins.binary_operations.push_back(Binary_Operation(op, BuiltinType::make(BuiltinType::Kind::BOOLEAN), BuiltinType::make(BuiltinType::Kind::BOOLEAN), BuiltinType::make(BuiltinType::Kind::BOOLEAN)));
    }
  }
}
