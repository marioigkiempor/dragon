
#pragma once

#ifndef DRAGON_BUILTINS_H_
#define DRAGON_BUILTINS_H_

#include <map>

#include "defines.h"

#include "lexer.h"
#include "types.h"

struct Binary_Operation {
  TokenKind op;
  Ref<Type> a;
  Ref<Type> b;
  Ref<Type> result;

  Binary_Operation() = default;

  Binary_Operation(TokenKind op, Ref<Type> a, Ref<Type> b, Ref<Type> result)
  : op(op)
  , a(a->copy())
  , b(b->copy())
  , result(result->copy()) {
  }
};


struct Builtins {
  Table<StringView, Ref<Type>> type_names;
  Table<StringView, TokenKind> keywords    = {};
  Table<TokenKind, TokenInfo>  token_infos = {};
  Vec<Binary_Operation>        binary_operations;
  Vec<TokenKind>               binary_operator_tokens;
  Vec<Ref<Type>>               integral_types;

  // @Todo: These should probably live inside the C++ code generator.
  String cpp_compiler = "clang++-15";

  String windows_sdk_version = "10.0.19041.0";
  Path   windows_sdk_path    = Path("C:\\") / "Program Files (x86)" / "Windows Kits" / "10";

  Vec<Path> windows_include_paths{
      windows_sdk_path / "Include" / windows_sdk_version / "shared",
      windows_sdk_path / "Include" / windows_sdk_version / "um",
      windows_sdk_path / "Include" / windows_sdk_version / "ucrt",
  };

  Vec<Path> windows_library_paths{
      windows_sdk_path / "Lib" / windows_sdk_version / "um" / "x64",
      windows_sdk_path / "Lib" / windows_sdk_version / "ucrt" / "x64",
  };

  Path bundled_thirdparty_root = Path("thirdparty");

  Path sdl_include_path = bundled_thirdparty_root / "SDL2" / "include";
  Path sdl_library_path = bundled_thirdparty_root / "SDL2" / "build";

  // @Note: we are compiling fmt into the main executable so we don't need its library path.
  Path fmt_include_path = bundled_thirdparty_root / "fmt" / "include";

  Vec<Path> include_paths = {};
  Vec<Path> library_paths = {};
};

inline Builtins global_builtins = { };

void init(Builtins& builtins);

bool is_builtin_type_name(StringView needle);
StringView builtin_type_name(BuiltinType::Kind type);

#endif // DRAGON_BUILTINS_H_