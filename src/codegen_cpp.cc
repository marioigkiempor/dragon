

#include "codegen_cpp.h"

#include <errno.h>
#include <filesystem>
#include <memory>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>

#include "fmt/os.h"
#include "fmt/ostream.h"

#include "defines.h"
#include "system_helpers.h"
#include "logging.h"
#include "ast.h"
#include "types.h"
#include "builtins.h"

struct Cpp_Code_Generator {
  Ast *ast = nullptr;

  // Compilation.
  bool is_on_lhs_of_assignment = false;
};

static std::string cpp_type_name(Ref<Type> type) {
  if (!type)  internal_error("Can't get the C++ type name for a null type.");

  switch (type->kind) {
  case Type::Kind::BUILTIN: {
    auto builtin_type = std::dynamic_pointer_cast<BuiltinType>(type);

    switch (builtin_type->builtin_kind) {
    case BuiltinType::Kind::U64:       return "uint64_t";
    case BuiltinType::Kind::U32:       return "uint32_t";
    case BuiltinType::Kind::U16:       return "uint16_t";
    case BuiltinType::Kind::U8:        return "uint8_t";
    case BuiltinType::Kind::S64:       return "int64_t";
    case BuiltinType::Kind::S32:       return "int32_t";
    case BuiltinType::Kind::S16:       return "int16_t";
    case BuiltinType::Kind::S8:        return "char";
    case BuiltinType::Kind::F64:       return "double";
    case BuiltinType::Kind::F32:       return "float";
    case BuiltinType::Kind::BOOLEAN:   return "bool";
    case BuiltinType::Kind::STRING:    return "std::string";
    case BuiltinType::Kind::V0ID:      return "void";

    case BuiltinType::COUNT_BUILTIN_TYPES: 
    default:
      internal_error("Can't  get C++ type name for invalid Builtin_Type::Kind: {}", (int)builtin_type->builtin_kind);
    }
  } break;

  case Type::Kind::POINTER: {
    auto pointer_type = std::dynamic_pointer_cast<PointerType> (type);
    return cpp_type_name(pointer_type->pointed) + "*";
  } break;

  case Type::Kind::ARRAY: {
    auto array_type = std::dynamic_pointer_cast<ArrayType>(type);
    if (array_type->is_vector)
      return "std::vector<" + cpp_type_name(array_type->element_type) + ">";

    return "std::array<" + cpp_type_name(array_type->element_type) + "," +
           std::to_string(array_type->count) + ">";
  } break;

  case Type::Kind::PROCEDURE: {
    auto procedure_type = std::dynamic_pointer_cast<ProcedureType>(type);
    return cpp_type_name(procedure_type->return_type);
  } break;

  case Type::Kind::STRUCT: {
    auto struct_type = std::dynamic_pointer_cast<StructType>(type);
    auto result = std::string(struct_type->name_token.contents);

    if (struct_type->struct_kind == StructType::Kind::INSTANCE) {
      auto struct_instance = std::dynamic_pointer_cast<StructInstanceType> (struct_type);
      if (!struct_instance->instance_arguments.empty()) {
        result += "<";
        for (size_t parameter_index = 0; parameter_index < struct_instance->instance_arguments.size(); ++parameter_index) {
          auto& given_parameter_type  = struct_instance->instance_arguments[parameter_index];
          result += cpp_type_name(given_parameter_type.type);
        }
        result += ">";
      }
    }

    return result;
  } break;

  case Type::Kind::ENUM: {
    auto enum_type = std::dynamic_pointer_cast<EnumType> (type);
    return std::string(enum_type->name_token.contents);
  } break;

  case Type::Kind::TYPE_INFO: {
    internal_error("Generating C++ type name for Type_Info type not implemented.");
  } break;

  case Type::Kind::UNRESOLVED: {
    internal_error("Cannot emit cpp for unresolved type");
  } break;

  default:  internal_error("Can't get C++ type name for invalid Type::Kind: {}", (int)type->kind);
  }

  return "invalid C++ type name";
}


// 
// Expressions
// 

static std::string builtin_cpp_print_proc_name = "dragon_builtin_print_proc";
static void codegen_cpp_from_expression(Ref<ExpressionAstNode> expr, fmt::ostream &cpp_file);

static void codegen_cpp_from_procedure_call_expression(Ref<ProcedureCallExpression> proc_call, fmt::ostream& cpp_file) {
  assert(is_expression(proc_call->called_node));

  auto calling_expr = std::dynamic_pointer_cast<ExpressionAstNode>(proc_call->called_node);

  assert(calling_expr->expression_kind == ExpressionAstNode::Kind::VARIABLE_REFERENCE);
  auto calling_var_ref = std::dynamic_pointer_cast<VariableReferenceExpression>(calling_expr);

  bool we_are_printing = false;
  if (calling_var_ref->name.contents == "print") {
    we_are_printing = true;
    cpp_file.print("{}(", builtin_cpp_print_proc_name);
  }
  else {
    cpp_file.print("{}(", calling_var_ref->name.contents);
  }

  for (size_t arg_index = 0; arg_index < proc_call->argument_nodes.size(); ++arg_index) {
    auto& arg = proc_call->argument_nodes[arg_index];
    if (we_are_printing && arg->type->is_array()) cpp_file.print("fmt::join(");
    codegen_cpp_from_expression(arg, cpp_file);
    if (we_are_printing && arg->type->is_array()) cpp_file.print(", \", \")");
    if (arg_index < proc_call->argument_nodes.size() - 1)  cpp_file.print(", ");
  }
  cpp_file.print(")");
}

static void codegen_cpp_from_assignment_expression(Ref<AssignmentExpression> assignment, fmt::ostream& cpp_file) {
  auto rhs_node = assignment->rhs_node;
  auto lhs_node = assignment->lhs_node;

  assert(is_expression(lhs_node));
  assert(is_expression(rhs_node));

  auto lhs_expr = std::dynamic_pointer_cast<ExpressionAstNode>(lhs_node);
  auto rhs_expr = std::dynamic_pointer_cast<ExpressionAstNode>(rhs_node);

  codegen_cpp_from_expression(lhs_node, cpp_file);

  cpp_file.print(" {} ", assignment->operatorr.info.name);

  // @Correctness: should I automatically cast the rhs to the lhs type?
  // Or should I rely on C++ to do it for me?
  if (*rhs_expr->type != *lhs_expr->type)  {
    if (lhs_expr->type->is_string() && rhs_expr->type->is_builtin()) {
      auto builtin = std::dynamic_pointer_cast<BuiltinType>(rhs_expr->type);
      if (builtin->builtin_kind == BuiltinType::Kind::S8) {
        // @Todo: this is a bit dirty.
        // C++ can't cast from char -> string, so don't print the type if that's the case.
      }
      else cpp_file.print("({}) ", cpp_type_name(lhs_expr->type));
    }
    else cpp_file.print("({}) ", cpp_type_name(lhs_expr->type));
  }

  codegen_cpp_from_expression(rhs_node, cpp_file);
}

static void codegen_cpp_from_literal_expression(Ref<LiteralExpression> literal, fmt::ostream& cpp_file) {
  switch (literal->literal_kind) {
  case LiteralExpression::Kind::U64: {
    cpp_file.print("{}", literal->literal_token);
  } break;

  case LiteralExpression::Kind::F64: {
    std::string str = std::string(literal->literal_token.contents);
    cpp_file.print("{}", str); // Todo: actually convert the value? std::stof(str);
  } break;

  case LiteralExpression::Kind::BOOLEAN: {
    cpp_file.print("{}", literal->literal_token);
  } break;

  case LiteralExpression::Kind::STRING: {
    cpp_file.print("\"{}\"", literal->literal_token.contents);
  } break;

  case LiteralExpression::Kind::INVALID:
  default:
    internal_error("Can't codegen C++ for invalid Literal_Expression::Kind: {}", (int)literal->literal_kind);
  }
}

static void codegen_cpp_from_binary_expression(Ref<BinaryExpression> binary, fmt::ostream& cpp_file) {
  auto lhs_expr = std::dynamic_pointer_cast<ExpressionAstNode>(binary->lhs_expression);
  auto rhs_expr = std::dynamic_pointer_cast<ExpressionAstNode>(binary->rhs_expression);
  auto &op       = binary->operatorr;

  if (lhs_expr->type->is_enum()) {
    auto enum_type = std::dynamic_pointer_cast<EnumType>(lhs_expr->type);

    if (enum_type->enum_kind == EnumType::Kind::BOXED) {
      auto boxed_enum = std::dynamic_pointer_cast<BoxedEnumType>(lhs_expr->type);

      if (rhs_expr->expression_kind == ExpressionAstNode::Kind::VARIABLE_REFERENCE) { // Must be a simple member inside a boxed enum.
        auto rhs_var_ref = std::dynamic_pointer_cast<VariableReferenceExpression>(rhs_expr);
        cpp_file.print("{{ {}_{}{{}} }}", boxed_enum->name(), rhs_var_ref->name.contents);
      }
      else if (rhs_expr->expression_kind == ExpressionAstNode::Kind::PROCEDURE_CALL) { // Must be a boxed member inside a boxed enum.
        auto rhs_proc_call = std::dynamic_pointer_cast<ProcedureCallExpression>(rhs_expr);
        log_assert(rhs_proc_call->called_node->expression_kind == ExpressionAstNode::Kind::VARIABLE_REFERENCE, "Boxed member");
        auto member_name_var_ref = std::dynamic_pointer_cast<VariableReferenceExpression>(rhs_proc_call->called_node);
        cpp_file.print("{{ {}_{}{{", boxed_enum->name(), member_name_var_ref->name.contents);
        for (size_t i = 0; i < rhs_proc_call->argument_nodes.size(); ++i) {
          auto& arg = rhs_proc_call->argument_nodes[i];
          codegen_cpp_from_expression(arg, cpp_file);
          if (i != rhs_proc_call->argument_nodes.size() - 1) cpp_file.print(", ");
        }
        cpp_file.print("}} }}");
      }
      else {
        internal_error("Trying to codegen C++ for a binary expression that refers to a boxed enum: the rhs expression has an invalid type.");
      }
      
      return;
    }
  }

  // cpp_file.print("(");

  if (op.info.kind == TokenKind::DOT) {
    if (lhs_expr->expression_kind == ExpressionAstNode::Kind::VARIABLE_REFERENCE) {
      auto lhs_as_var_ref = std::dynamic_pointer_cast<VariableReferenceExpression>(lhs_expr);

      if (is_builtin_type_name(lhs_as_var_ref->name.contents))  cpp_file.print("dragon_");
    }
  }

  codegen_cpp_from_expression(binary->lhs_expression, cpp_file);

  // log_assert(lhs_expr->type != nullptr, "The type of the binary lhs has not been resolved yet.");
  if (lhs_expr->type == nullptr) {
    log_info("Lhs type has not been resolved.");
  }

  if (binary->is_enum_member_reference)                                    cpp_file.print("::");
  else if (op.info.kind == TokenKind::DOT && lhs_expr->type->is_pointer()) cpp_file.print("->");
  else                                                                     cpp_file.print("{}", global_builtins.token_infos[op.info.kind].name);

  if (op.info.kind != TokenKind::DOT) {
    codegen_cpp_from_expression(binary->rhs_expression, cpp_file);
    return;
  }

  // if (lhs_expr->expression_kind != ExpressionAstNode::Kind::VARIABLE_REFERENCE) {
  //   codegen_cpp_from_expression(scope, binary->rhs_expression, cpp_file);
  //   return;
  // }

  if (lhs_expr->expression_kind == ExpressionAstNode::Kind::VARIABLE_REFERENCE) {
    auto lhs_as_var_ref = std::dynamic_pointer_cast<VariableReferenceExpression>(lhs_expr);
    if (lhs_as_var_ref->is_type_info_reference) {
      if (rhs_expr->expression_kind == ExpressionAstNode::Kind::VARIABLE_REFERENCE) {
        auto rhs_as_var_ref = std::dynamic_pointer_cast<VariableReferenceExpression>(rhs_expr);
        if ("name" == rhs_as_var_ref->name.contents) {
          cpp_file.print("name");
        }
        else {
          log_assert(0, "type_info can only have .name member references.");
        }
      }
      return;
    }
  }

  if (lhs_expr->type->is_array()) {
    if (rhs_expr->expression_kind == ExpressionAstNode::Kind::VARIABLE_REFERENCE) {
      auto rhs_as_var_ref = std::dynamic_pointer_cast<VariableReferenceExpression>(rhs_expr);
      if ("count" == rhs_as_var_ref->name.contents) {
        cpp_file.print("size()");
      }
      else if ("data" == rhs_as_var_ref->name.contents) {
        cpp_file.print("data()");
      }
      else {
        internal_error("Cannot codegen C++ for Array.`{}`.", rhs_as_var_ref->name.contents);
      }
    }
    else {
      internal_error("RHS of `Array.` must be a VariableReferenceExpression");
    }
  }
  else if (lhs_expr->type->is_string()) {
    if (rhs_expr->expression_kind == ExpressionAstNode::Kind::VARIABLE_REFERENCE) {
      auto rhs_as_var_ref = std::dynamic_pointer_cast<VariableReferenceExpression>(rhs_expr);
      if ("length" == rhs_as_var_ref->name.contents) {
        cpp_file.print("length()");
      }
      else {
        internal_error("Cannot codegen C++ for string.`{}`.", rhs_as_var_ref->name.contents);
      }
    }
    else {
      internal_error("RHS of `string.` must be a VariableReferenceExpression");
    }
  }
  else {
    codegen_cpp_from_expression(binary->rhs_expression, cpp_file);
  }

  // cpp_file.print(")");
}

static void codegen_cpp_from_unary_expression(Ref<UnaryExpression> unary, fmt::ostream& cpp_file) {
  if (unary->operatorr.info.kind == TokenKind::POINTER)          cpp_file.print("&");
  else if (unary->operatorr.info.kind == TokenKind::DEREFERENCE) cpp_file.print("*");
  else                                                           cpp_file.print("{}", global_builtins.token_infos[unary->operatorr.info.kind].name);

  codegen_cpp_from_expression(unary->operand_node, cpp_file);
}

static void codegen_cpp_from_array_reference_expression(Ref<ArrayReferenceExpression> array_ref, fmt::ostream& cpp_file) {
  codegen_cpp_from_expression(array_ref->array_expression, cpp_file);

  cpp_file.print(" [ ");
  codegen_cpp_from_expression(array_ref->index_expression, cpp_file);
  cpp_file.print(" ]");
}

static void codegen_cpp_from_array_literal_expression(Ref<ArrayLiteralExpression> array_literal, fmt::ostream& cpp_file) {
  cpp_file.print("{{ {{ ");
  for (size_t element_index = 0; element_index < array_literal->element_nodes.size(); ++element_index) {
    codegen_cpp_from_expression(array_literal->element_nodes[element_index], cpp_file);

    if (element_index != array_literal->element_nodes.size() - 1)  cpp_file.print(", ");
  }

  cpp_file.print(" }} }}");
}

static void codegen_cpp_from_variable_reference_expression(Ref<VariableReferenceExpression> var_ref, fmt::ostream& cpp_file) {
  if (is_builtin_type_name(var_ref->name.contents)) {
    cpp_file.print("dragon_{}.type_info", var_ref->name.contents);
  } else {
    cpp_file.print("{}", var_ref->name.contents);
  }
}

static void codegen_cpp_from_grouping_expression(Ref<GroupingExpression> grouping, fmt::ostream& cpp_file) {
  cpp_file.print("(");
  codegen_cpp_from_expression(grouping->grouped_expression, cpp_file);
  cpp_file.print(")");
}

static void codegen_cpp_from_cast_expression(Ref<CastExpression> cast, fmt::ostream& cpp_file) {
  cpp_file.print("static_cast< {} > (", cpp_type_name(cast->type));
  codegen_cpp_from_expression(cast->casted_node, cpp_file);
  cpp_file.print(" )");
}

static void codegen_cpp_from_expression(Ref<ExpressionAstNode> expr, fmt::ostream &cpp_file) {
  switch (expr->expression_kind) {
  
  case ExpressionAstNode::Kind::PROCEDURE_CALL: {
    auto proc_call = std::dynamic_pointer_cast<ProcedureCallExpression>(expr);
    codegen_cpp_from_procedure_call_expression(proc_call, cpp_file);
  } break;

  case ExpressionAstNode::Kind::GROUPING: {
    auto grouping = std::dynamic_pointer_cast<GroupingExpression>(expr);
    codegen_cpp_from_grouping_expression(grouping, cpp_file);
  } break;

  case ExpressionAstNode::Kind::ASSIGNMENT: {
    auto assignment = std::dynamic_pointer_cast<AssignmentExpression>(expr);
    codegen_cpp_from_assignment_expression(assignment, cpp_file);
  } break;

  case ExpressionAstNode::Kind::LITERAL: {
    auto literal = std::dynamic_pointer_cast<LiteralExpression>(expr);
    codegen_cpp_from_literal_expression(literal, cpp_file);
  } break;

  case ExpressionAstNode::Kind::BINARY: {
    auto binary = std::dynamic_pointer_cast<BinaryExpression>(expr);
    codegen_cpp_from_binary_expression(binary, cpp_file);
  } break;

  case ExpressionAstNode::Kind::UNARY: {
    auto unary = std::dynamic_pointer_cast<UnaryExpression>(expr);
    codegen_cpp_from_unary_expression(unary, cpp_file);
  } break;

  case ExpressionAstNode::Kind::ARRAY_REFERENCE: {
    auto array_ref = std::dynamic_pointer_cast<ArrayReferenceExpression>(expr);
    codegen_cpp_from_array_reference_expression(array_ref, cpp_file);
  } break;

  case ExpressionAstNode::Kind::ARRAY_LITERAL: {
    auto array_literal = std::dynamic_pointer_cast<ArrayLiteralExpression>(expr);
    codegen_cpp_from_array_literal_expression(array_literal, cpp_file);
  } break;

  case ExpressionAstNode::Kind::VARIABLE_REFERENCE: {
    auto var_ref = std::dynamic_pointer_cast<VariableReferenceExpression>(expr);
    codegen_cpp_from_variable_reference_expression(var_ref, cpp_file);
  } break;

  case ExpressionAstNode::Kind::CAST: {
    auto cast = std::dynamic_pointer_cast<CastExpression>(expr);
    codegen_cpp_from_cast_expression(cast, cpp_file);
  } break;

  case ExpressionAstNode::Kind::INVALID:
  default: internal_error("Cannot codegen C++ for invalid Expression::Kind: {}", (int)expr->expression_kind);
  }
}





// 
// Statements
// 

static bool codegen_cpp_from_statement(Ref<StatementAstNode> statement, fmt::ostream &cpp_file, bool output_semi = true);

static void codegen_cpp_from_match_statement(Ref<MatchStatement> match_statement, fmt::ostream& cpp_file) {
  if (match_statement->match_on->type->is_boxed_enum()) {
    // internal_error("@Todo: C++ codegen for boxed enum matches is unfinishes.");
    auto boxed_enum = std::dynamic_pointer_cast<BoxedEnumType>(match_statement->match_on->type);
    
    for (size_t match_case_index = 0; match_case_index < match_statement->cases.size(); ++match_case_index) {
      auto& match_case = match_statement->cases[match_case_index];
      log_assert(match_case.value->expression_kind == ExpressionAstNode::Kind::BINARY, "Generating cpp for a boxed enum, a case statement doesn't look like a enum member reference (binary expression)");
      auto enum_reference = std::dynamic_pointer_cast<BinaryExpression> (match_case.value);

      if (match_case_index != 0) cpp_file.print("else ");

      if (enum_reference->rhs_expression->expression_kind == ExpressionAstNode::Kind::PROCEDURE_CALL) {
        auto proc_call = std::dynamic_pointer_cast<ProcedureCallExpression>(enum_reference->rhs_expression);
        log_assert(proc_call->called_node->expression_kind == ExpressionAstNode::Kind::VARIABLE_REFERENCE, "Generating cpp for a boxed enum in a match statement, the called node in the procedure is not a variable reference.");
        auto member_name = std::dynamic_pointer_cast<VariableReferenceExpression>(proc_call->called_node);

        auto member = boxed_enum->find_member(member_name->name.contents);
        log_assert(member != nullptr, "Generating cpp for a boxed enum in a match statement, a boxed member could not be found inside the enum.");
        log_assert(member->kind == EnumType::Member::Kind::BOXED, "Generating cpp for a boxed enum in a match statement, boxed member is not actually a boxed member.");
        auto boxed_member = std::dynamic_pointer_cast<BoxedEnumType::Member> (member);

        cpp_file.print("if (auto* x = std::get_if<{}_{}>(&", boxed_enum->name(), member->name_token.contents);
        codegen_cpp_from_expression(match_statement->match_on, cpp_file);
        cpp_file.print(")) {{ \n");

        Vec<StringView> member_names = {};
        for (auto argument : proc_call->argument_nodes) {
          log_assert(argument->expression_kind == ExpressionAstNode::Kind::VARIABLE_REFERENCE,  "Generating cpp for a boxed enum in a match statement, one of the procedure arguments is not a variable reference expression.");
          auto value_name = std::dynamic_pointer_cast<VariableReferenceExpression>(argument);
          member_names.push_back(value_name->name.contents);
        }

        for (size_t i = 0; i < boxed_member->values.size(); ++i) {
          auto& value = boxed_member->values[i];
          cpp_file.print("auto& {} = x->{};\n", member_names[i], value.name.contents);
        }

        codegen_cpp_from_statement(match_case.then, cpp_file);

        cpp_file.print("}} \n", boxed_enum->name(), member->name_token.contents);
      }
      else if (enum_reference->rhs_expression->expression_kind == ExpressionAstNode::Kind::VARIABLE_REFERENCE) {
        auto variable_reference = std::dynamic_pointer_cast<VariableReferenceExpression>(enum_reference->rhs_expression);

        cpp_file.print("if (auto* x = std::get_if<{}_{}>(&", boxed_enum->name(), variable_reference->name.contents);
        codegen_cpp_from_expression(match_statement->match_on, cpp_file);
        cpp_file.print(")) {{ \n");

        codegen_cpp_from_statement(match_case.then, cpp_file);
        
        cpp_file.print("}}\n");
        // for (auto& simple_member : boxed_enum->simple_members) {
        // }
      }
    }
    cpp_file.print("else {{\n");
    codegen_cpp_from_statement(match_statement->else_case, cpp_file);
    cpp_file.print("}}\n");
  }
  else {
    cpp_file.print("switch (");
    codegen_cpp_from_expression(match_statement->match_on, cpp_file);
    cpp_file.print(") {{\n");
    for (auto& match_case : match_statement->cases) {
      cpp_file.print("case ");
      codegen_cpp_from_expression(match_case.value, cpp_file);
      cpp_file.print(": {{\n");
      codegen_cpp_from_statement(match_case.then, cpp_file);
      cpp_file.print("}} break;\n");
    }

    cpp_file.print("default: {{");
    codegen_cpp_from_statement(match_statement->else_case, cpp_file);
    cpp_file.print("}} break;\n");
    cpp_file.print("}}");
  }
}

static void codegen_cpp_from_for_loop_statement(Ref<ForLoop> for_loop, fmt::ostream& cpp_file) {
  switch (for_loop->for_statement_kind) {
    case ForLoop::Kind::RANGED: {
      auto ranged_for_loop = std::dynamic_pointer_cast<RangedForLoop> (for_loop);

      log_assert(ranged_for_loop->range_expression_statement->node_kind == AstNode::Kind::STATEMENT,
                  "Ranged_For_Statement.range_expression_statement is not an Ast_Node::Kind::STATEMENT (it is a {}).", ranged_for_loop->range_expression_statement->node_kind);
      log_assert((std::dynamic_pointer_cast<StatementAstNode> (ranged_for_loop->range_expression_statement))->statement_kind == StatementAstNode::Kind::EXPRESSION,
                  "Ranged_For_Statement.range_expression_statement is not an Statement_Node::Kind::EXPRESSION.", (std::dynamic_pointer_cast<StatementAstNode> (ranged_for_loop->range_expression_statement))->statement_kind);

      auto range_stmt_node_expr = std::dynamic_pointer_cast<ExpressionStatement>(ranged_for_loop->range_expression_statement);

      log_assert(range_stmt_node_expr->expression_node->node_kind == AstNode::Kind::EXPRESSION,
                  "Ranged_For_Statement.range_expression_statement.expression_node is not an Ast_Node::Kind::EXPRESSION (it is a {}).", range_stmt_node_expr->expression_node->node_kind);

      auto range_expr = std::dynamic_pointer_cast<ExpressionAstNode> (range_stmt_node_expr->expression_node);
      log_assert(range_expr->expression_kind == ExpressionAstNode::Kind::BINARY,
                  "Ranged_For_Statement.range_expression_statement.expression_node is not an Ast_Expression::Kind::BINARY.", range_expr->expression_kind);

      auto range_binary_expr      = std::dynamic_pointer_cast<BinaryExpression>(range_stmt_node_expr->expression_node);

      cpp_file.print("for (auto it = ");
      codegen_cpp_from_expression(range_binary_expr->lhs_expression, cpp_file);
      cpp_file.print(" ; it < ");
      codegen_cpp_from_expression(range_binary_expr->rhs_expression, cpp_file);
      cpp_file.print(" ; ++it) ");

      codegen_cpp_from_statement(for_loop->body_node, cpp_file);
    } break;

    case ForLoop::Kind::C_STYLE: {
      auto c_style_for_loop = std::dynamic_pointer_cast<CStyleForLoop> (for_loop);
      cpp_file.print("for (\n");
      codegen_cpp_from_statement(c_style_for_loop->initialisation_node, cpp_file);
      codegen_cpp_from_statement(c_style_for_loop->condition_node, cpp_file);
      codegen_cpp_from_expression(c_style_for_loop->increment_node, cpp_file);
      cpp_file.print(")");
      codegen_cpp_from_statement(for_loop->body_node, cpp_file);
    } break;

    case ForLoop::Kind::INVALID: 
    default:
      internal_error("Tried to codegen C++ for invalid for-loop kind: {}", (int)for_loop->for_statement_kind);
      break;
  }
}

static void codegen_cpp_from_while_loop_statement(Ref<WhileLoop> while_loop, fmt::ostream& cpp_file) {
  cpp_file.print("while ( ");
  codegen_cpp_from_expression(while_loop->condition_node, cpp_file);
  cpp_file.print(" ) ");

  codegen_cpp_from_statement(while_loop->body_node, cpp_file);
}

static void codegen_cpp_from_variable_declaration_statement(Ref<VariableDeclaration> variable_declaration, fmt::ostream& cpp_file) {
  if (variable_declaration->is_external)  return;

  if (variable_declaration->value) {
    // variable->value = *declaration.value;
    cpp_file.print("auto {} = ", variable_declaration->name_token.contents);

    auto decl_value = std::dynamic_pointer_cast<ExpressionAstNode>(variable_declaration->value);
    if (*variable_declaration->type != *decl_value->type) {
      cpp_file.print("({})", cpp_type_name(variable_declaration->type));
    }

    codegen_cpp_from_expression(variable_declaration->value, cpp_file);

    cpp_file.print(";");
  }
  else {
    cpp_file.print("{} {} {{}};", cpp_type_name(variable_declaration->type), variable_declaration->name_token.contents);
  }
}

static void codegen_cpp_from_enum_declaration_statement(Ref<EnumDeclaration> enum_declaration, fmt::ostream& cpp_file) {
  auto enum_type = std::dynamic_pointer_cast<EnumType> (enum_declaration->type);
  auto enum_name = enum_type->name();
  switch (enum_type->enum_kind) {
  case EnumType::Kind::SIMPLE: {
    auto simple_enum = std::dynamic_pointer_cast<SimpleEnumType> (enum_type);
    cpp_file.print("enum class {} {{", enum_name);
    for (auto& enum_member : simple_enum->members) {
      cpp_file.print("{}, ", enum_member->name_token.contents);
    }
    cpp_file.print("}};\n");

    cpp_file.print("template <> struct fmt::formatter<{}> {{\n", enum_name);
    cpp_file.print("  constexpr auto parse(fmt::format_parse_context &ctx) {{ return ctx.end(); }}\n");
    cpp_file.print("\n");
    cpp_file.print("  template <typename FormatContext>\n");
    cpp_file.print("  auto format(const {} &a, FormatContext &ctx) {{\n", enum_name);
    cpp_file.print("    switch(a) {{\n");
    for (auto& member : simple_enum->members)
      cpp_file.print("        case {}::{}: fmt::format_to(ctx.out(), \"{}.{}\"); break;\n", enum_name, member->name_token.contents, enum_name, member->name_token.contents);
    cpp_file.print("        default: break;\n");
    cpp_file.print("    }}\n");
    cpp_file.print("    return ctx.out();\n");
    cpp_file.print("  }}\n");
    cpp_file.print("}};", enum_name);
  } break;

  case EnumType::Kind::BOXED: {
    auto boxed_enum = std::dynamic_pointer_cast<BoxedEnumType>(enum_type);

    // Declare structs for each of the enum's members.
    for (auto& member : boxed_enum->members) {
      switch(member->kind) {
      case EnumType::Member::Kind::BOXED: {
        auto boxed_member = std::dynamic_pointer_cast<BoxedEnumType::Member> (member);
        cpp_file.print("struct {}_{} {{ \n", enum_name, boxed_member->name_token.contents);
        for (auto& boxed_value : boxed_member->values) {
          cpp_file.print("    {} {};\n", cpp_type_name(boxed_value.type), boxed_value.name.contents);
        }
        cpp_file.print("}};\n");
      } break;
      case EnumType::Member::Kind::SIMPLE: {
        cpp_file.print("struct {}_{} {{}};\n", enum_name, member->name_token.contents);
      } break;
      default: internal_error("Invalid enum member kind.");
      }
    }

    cpp_file.print("using {} = std::variant<", enum_name);
    for (size_t i = 0; i < boxed_enum->members.size(); ++i) {
      cpp_file.print("{}_{}", enum_name, boxed_enum->members[i]->name_token.contents);
      if (i != boxed_enum->members.size() - 1) cpp_file.print(",");
    }
    cpp_file.print(">;\n\n");

    cpp_file.print("template<> struct fmt::formatter<{}> {{\n", enum_name);
    cpp_file.print("    constexpr auto parse(fmt::format_parse_context& ctx) {{ return ctx.end(); }}\n");
    cpp_file.print("    template<typename FormatContext> auto format(const {}& v, FormatContext& ctx) {{\n", enum_name);
    for (auto& member : boxed_enum->members) {
      switch (member->kind) {
      case EnumType::Member::Kind::SIMPLE: {
        cpp_file.print("        if (auto* x = std::get_if<{}_{}>(&v)) {{ return fmt::format_to(ctx.out(), \"{}.{}\"); }}\n", enum_name, member->name_token.contents, enum_name, member->name_token.contents);
      } break;

      case EnumType::Member::Kind::BOXED: {
        auto boxed_member = std::dynamic_pointer_cast<BoxedEnumType::Member>(member);
        cpp_file.print("        if (auto* x = std::get_if<{}_{}>(&v)) {{ return fmt::format_to(ctx.out(), \"{}.{}(",
                        enum_name, boxed_member->name_token.contents, enum_name, boxed_member->name_token.contents);

        for (auto& boxed_value : boxed_member->values) cpp_file.print("{} = {{}}", boxed_value.name.contents);

        cpp_file.print(")\"");

        if (!boxed_member->values.empty()) {
          cpp_file.print(",");
          for (size_t i = 0; i < boxed_member->values.size(); ++i) {
            cpp_file.print("x->{}", boxed_member->values[i].name.contents);
            if (i != boxed_member->values.size() - 1) cpp_file.print(", ");
          }
        }
        cpp_file.print("); }}\n");
      } break;
      
      default:
        break;
      }
    }
    cpp_file.print("        return fmt::format_to(ctx.out(), \"{}.INVALID\");", enum_name);
    cpp_file.print("    }};\n");
    cpp_file.print("}};\n");
  } break;

  default: internal_error("Cannot codegen C++ for invalid EnumType::Kind {}", (int)enum_type->enum_kind);
  }
}

static void codegen_cpp_from_struct_declaration_statement(Ref<StructDeclaration> struct_declaration, fmt::ostream& cpp_file) {
  auto declare_struct_params = [](fmt::ostream &cpp_file, std::vector<ParameterisedStructType::Parameter> &struct_params) {
    if (!struct_params.empty()) {
      cpp_file.print("template <");
      for (size_t i = 0; i < struct_params.size(); ++i) {
        cpp_file.print("typename {}", struct_params[i].name);
        if (i < struct_params.size()-1)  cpp_file.print(", ");
      }
      cpp_file.print(">");
    }
  };

  auto print_full_struct_name = [](fmt::ostream &cpp_file, Ref<StructType> struct_type, Token &struct_name) {
    auto parameterised_struct_type = std::dynamic_pointer_cast<ParameterisedStructType> (struct_type);
    auto is_parameterised = (struct_type->struct_kind == StructType::Kind::PARAMETERISED);
    if (is_parameterised) {
      assert(parameterised_struct_type != nullptr);
    }

    cpp_file.print("{}", struct_name.contents);
    if (is_parameterised) {
      cpp_file.print("<");

      for (size_t i = 0; i < parameterised_struct_type->expected_parameters.size(); ++i) {
        cpp_file.print("{}", parameterised_struct_type->expected_parameters[i].name);
        if (i < parameterised_struct_type->expected_parameters.size()-1)  cpp_file.print(", ");
      }

      cpp_file.print(">");
    }
  };

  assert(struct_declaration->type->is_struct());
  auto struct_type = std::dynamic_pointer_cast<StructType>(struct_declaration->type);

  auto parameterised_struct_type = std::dynamic_pointer_cast<ParameterisedStructType> (struct_type);
  auto is_parameterised = (struct_type->struct_kind == StructType::Kind::PARAMETERISED);
  if (is_parameterised) {
    assert(parameterised_struct_type != nullptr);
  }

  auto &struct_name = struct_declaration->name_token;
  auto &struct_members = struct_type->members;

  if (is_parameterised) {
    declare_struct_params(cpp_file, parameterised_struct_type->expected_parameters);
    cpp_file.print("\n");
  }

  cpp_file.print("struct {} {{\n", struct_name.contents);
  cpp_file.print("    Type_Info type_info;\n");

  for (size_t member_index = 0; member_index < struct_members.size(); ++member_index) {
    auto& member = struct_members[member_index];
    if (member.refers_to_parameter) cpp_file.print("    {}", member.referred_parameter_name.contents);
    else                            cpp_file.print("    {}", cpp_type_name(member.type));

    cpp_file.print(" {};\n", member.name.contents);
  }

  cpp_file.print("}};\n");
  cpp_file.print("\n");

  if (is_parameterised)   declare_struct_params(cpp_file, parameterised_struct_type->expected_parameters);
  else                    cpp_file.print("template <>");

  cpp_file.print("\nstruct fmt::formatter<");
  // @Copypasta
  print_full_struct_name(cpp_file, struct_type, struct_name);
  cpp_file.print("> {{\n");
  cpp_file.print("  constexpr auto parse(fmt::format_parse_context &ctx) {{ return ctx.end(); }}\n");
  cpp_file.print("\n");
  cpp_file.print("  template <typename FormatContext>\n");
  cpp_file.print("  auto format(const ");
  // @Copypasta
  print_full_struct_name(cpp_file, struct_type, struct_name);
  cpp_file.print(" &a, FormatContext &ctx) const {{\n");
  cpp_file.print("    fmt::format_to(ctx.out(), \"");
  // @Copypasta
  print_full_struct_name(cpp_file, struct_type, struct_name);
  cpp_file.print(" {{{{ \");\n");
  for (size_t member_index = 0; member_index < struct_members.size(); ++member_index) {
    auto &member = struct_members[member_index];
    cpp_file.print("    fmt::format_to(ctx.out(), \"{} = {{}}\", a.{});\n", member.name.contents, member.name.contents);

    if (member_index < struct_members.size() - 1)  cpp_file.print("    fmt::format_to(ctx.out(), \", \");\n");
  }
  cpp_file.print("    fmt::format_to(ctx.out(), \" }}}}\");\n");
  cpp_file.print("    return ctx.out();\n");
  cpp_file.print("  }}\n");
  cpp_file.print("}};\n");

}

static void codegen_cpp_from_code_block_statement(Ref<CodeBlockStatement> code_block, fmt::ostream& cpp_file) {
  cpp_file.print("{{\n");

  for (size_t statement_index = 0; statement_index < code_block->statement_nodes.size(); ++statement_index) {
    auto& statement_node  = code_block->statement_nodes[statement_index];
    codegen_cpp_from_statement(statement_node, cpp_file);
  }

  for (size_t deferred_index = 0; deferred_index < code_block->deferred_statement_nodes.size(); ++deferred_index) {
    auto& deferred_node = code_block->deferred_statement_nodes[deferred_index];
    codegen_cpp_from_statement(deferred_node, cpp_file);
  }

  cpp_file.print("}}");
}

static void codegen_cpp_from_if_statement(Ref<IfStatement> if_statement, fmt::ostream& cpp_file) {
  cpp_file.print("if ( ");
  codegen_cpp_from_expression(if_statement->condition_node, cpp_file);
  cpp_file.print(" )\n");

  assert(is_statement(if_statement->body_node));
  // assert(if_stmt_node.then->as.stmt_node->kind == AST_stmt_node_SCOPE);
  codegen_cpp_from_statement(if_statement->body_node, cpp_file);

  if (if_statement->else_node) {
    cpp_file.print("else\n");
    codegen_cpp_from_statement(if_statement->else_node, cpp_file);
  }
}

static void codegen_cpp_from_return_statement(Ref<ReturnStatement> return_statement, fmt::ostream& cpp_file) {
  cpp_file.print("return ");
  codegen_cpp_from_expression(return_statement->returned_expression, cpp_file);
  cpp_file.print(";");
}

static void codegen_cpp_from_expression_statement(Ref<ExpressionStatement> expression_statement, fmt::ostream& cpp_file) {
  codegen_cpp_from_expression(expression_statement->expression_node, cpp_file);
}

static void codegen_cpp_from_defer_statement(Ref<DeferStatement> defer_statement, fmt::ostream& cpp_file) {
  codegen_cpp_from_statement(defer_statement->deferred_node, cpp_file);
}

static bool codegen_cpp_from_statement(Ref<StatementAstNode> statement, fmt::ostream &cpp_file, bool output_semi) {
  switch (statement->statement_kind) {
  case StatementAstNode::Kind::MATCH: {
    auto match_statement = std::dynamic_pointer_cast<MatchStatement>(statement);
    codegen_cpp_from_match_statement(match_statement, cpp_file);
  } break;

  case StatementAstNode::Kind::BREAK: {
    cpp_file.print("break;");
  } break;

  case StatementAstNode::Kind::FOR: {
    auto for_loop = std::dynamic_pointer_cast<ForLoop>(statement);
    codegen_cpp_from_for_loop_statement(for_loop, cpp_file);
  } break;

  case StatementAstNode::Kind::WHILE: {
    auto while_loop = std::dynamic_pointer_cast<WhileLoop>(statement);
    codegen_cpp_from_while_loop_statement(while_loop, cpp_file);
  } break;

  case StatementAstNode::Kind::IF: {
    auto if_statement = std::dynamic_pointer_cast<IfStatement>(statement);
    codegen_cpp_from_if_statement(if_statement, cpp_file);
  } break;

  case StatementAstNode::Kind::RETURN: {
    auto return_statement = std::dynamic_pointer_cast<ReturnStatement>(statement);
    codegen_cpp_from_return_statement(return_statement, cpp_file);
  } break;

  case StatementAstNode::Kind::CODE_BLOCK: {
    auto code_block = std::dynamic_pointer_cast<CodeBlockStatement>(statement);
    codegen_cpp_from_code_block_statement(code_block, cpp_file);
  } break;

  case StatementAstNode::Kind::EXPRESSION: {
    auto expression_statement = std::dynamic_pointer_cast<ExpressionStatement>(statement);
    codegen_cpp_from_expression_statement(expression_statement, cpp_file);
    if (output_semi)  cpp_file.print(";");
  } break;

  case StatementAstNode::Kind::DEFER: {
    auto defer_statement = std::dynamic_pointer_cast<DeferStatement>(statement);
    codegen_cpp_from_defer_statement(defer_statement, cpp_file);
  } break;

  case StatementAstNode::Kind::VARIABLE_DECLARATION: {
    auto variable_declaration = std::dynamic_pointer_cast<VariableDeclaration>(statement);
    codegen_cpp_from_variable_declaration_statement(variable_declaration, cpp_file);
  } break;

  case StatementAstNode::Kind::STRUCT_DECLARATION: {
    auto struct_declaration = std::dynamic_pointer_cast<StructDeclaration>(statement);
    codegen_cpp_from_struct_declaration_statement(struct_declaration, cpp_file);
  } break;

  case StatementAstNode::Kind::ENUM_DECLARATION: {
    auto enum_declaration = std::dynamic_pointer_cast<EnumDeclaration>(statement);
    codegen_cpp_from_enum_declaration_statement(enum_declaration, cpp_file);
  } break;

  case StatementAstNode::Kind::PROCEDURE_DECLARATION: {
    auto procedure_declaration = std::dynamic_pointer_cast<ProcedureDeclaration>(statement);
    internal_error("@Todo: C++ codegen for lambdas.");
    // codegen_cpp_from_procedure_declaration_statement(procedure_declaration, cpp_file);
  } break;

  case StatementAstNode::Kind::INCLUDE: {
    internal_error("Generating C++ for include statements not implemented.");
  } break;

  case StatementAstNode::Kind::TYPE_ALIAS: {
    internal_error("Generating C++ for type alias statements not implemented.");
  } break;

  case StatementAstNode::Kind::INVALID:
  default: internal_error("Cannot codegen C++ for invalid Statement::Kind: {}", (int)statement->statement_kind); break;
  }

  cpp_file.print("\n");
  return true;
}

bool codegen_cpp_for_ast(Ast* ast, const std::string &filepath) {
  assert(ast->program_node);
  assert(ast->program_node->node_kind == AstNode::Kind::PROGRAM);

  auto ast_program = ast->program_node;

  auto cpp_file = fmt::output_file(filepath);
  // @Todo: check if the file actually opened.
  // if (cpp_file.fail())   log_user_error("Couldn't open intermediary C++ file (`{}`)", filepath);

  // cpp_file << "#include <stdio.h>\n";
  cpp_file.print("#include <stdlib.h>\n");
  // cpp_file.print("#include <cstdint>\n";
  cpp_file.print("\n");
  cpp_file.print("#include <iostream>\n");
  cpp_file.print("#include <array>\n");
  cpp_file.print("#include <vector>\n");
  cpp_file.print("#include <variant> // for boxed enums.\n");


#if DRAGON_WINDOWS
  cpp_file.print("#include <windows.h>\n");
#endif
  cpp_file.print("\n");

  cpp_file.print("#define SDL_MAIN_HANDLED\n");
  cpp_file.print("#include \"SDL.h\"\n");
  cpp_file.print("\n");

  cpp_file.print("#include \"../thirdparty/fmt/src/format.cc\"\n");
  cpp_file.print("#include \"../thirdparty/fmt/src/os.cc\"\n");
  cpp_file.print("\n");

  cpp_file.print("#include \"fmt/format.h\"\n");
  cpp_file.print("#include \"fmt/color.h\"\n");
  cpp_file.print("#include \"fmt/ranges.h\"\n");
  cpp_file.print("#include \"fmt/os.h\"\n");
  cpp_file.print("\n");

  cpp_file.print("struct Type_Info {{\n");
  cpp_file.print("    std::string name;\n");
  cpp_file.print("}};\n");
  cpp_file.print("\n");

  // Builtin type constructors
  cpp_file.print("std::string make_string(char *buffer, size_t size) {{ return std::string(buffer, size); }}\n");
  cpp_file.print("const char* cstr(const std::string &str) {{ return str.c_str(); }}\n");
  cpp_file.print("template <typename T> auto array_push(std::vector<T>& array, T item) {{ array.push_back(item); }}\n");
  cpp_file.print("\n");
  // cpp_file.print("template <typename T>\n";
  // cpp_file.print("std::array make_array<T>(void *buffer, uint64_t size) {\n";
  // cpp_file.print("    return std::array<T, size>(buffer);\n";
  // cpp_file.print("}\n";

  // cpp_file.print("template<typename T> struct fmt::formatter<std::vector<T>> {{\n");
  // cpp_file.print("  constexpr auto parse(fmt::format_parse_context &ctx) {{ return ctx.end(); }}\n");
  // cpp_file.print("  template <typename FormatContext>\n");
  // cpp_file.print("  auto format(std::vector<T> v, FormatContext& ctx) {{ for (auto x : v) {{ fmt::format_to(ctx.out(), \"{{}}\", x); }} }}\n");
  // cpp_file.print("}};\n");

  for (auto &[name, type] : global_builtins.type_names) {
    cpp_file.print("struct {}_Type {{\n", name);
    cpp_file.print("    Type_Info type_info;\n");
    cpp_file.print("}};\n");
    cpp_file.print("{}_Type dragon_{} {{ .type_info = {{ .name = \"{}\" }} }};\n", name, name, name);
    cpp_file.print("\n");
  }

  // Declare all the enumerations
  for (auto& enum_decl : ast->enum_declarations) {
    codegen_cpp_from_statement(enum_decl, cpp_file);
  }


  // Declare all the structs
  for (size_t struct_def_index = 0; struct_def_index < ast->struct_defs.size(); ++struct_def_index) {
    auto& struct_declaration = ast->struct_defs[struct_def_index];

    if (struct_declaration->is_external)  continue;
    log_assert(struct_declaration->type->is_struct(), "Struct declaration for {} does not have a struct type.", struct_declaration->name_token);

    codegen_cpp_from_struct_declaration_statement(struct_declaration, cpp_file);
  }

  {
    // Typedefs
    for (size_t alias_index = 0; alias_index < ast->type_aliases.size(); alias_index++) {
      auto& alias_node = ast->type_aliases[alias_index];
      auto alias = std::dynamic_pointer_cast<TypeAliasStatement>(alias_node);

      cpp_file.print("typedef {} {};", cpp_type_name(alias->type), alias->name.contents);
    }
  }

  {
    // Global variables.
    for (size_t top_level_index = 0; top_level_index < ast_program->top_level_statements.size(); ++top_level_index) {
      auto& top_level_statement = ast_program->top_level_statements[top_level_index];

      if (top_level_statement->statement_kind != StatementAstNode::Kind::VARIABLE_DECLARATION) continue;

      auto variable_declaration = std::dynamic_pointer_cast<VariableDeclaration>(top_level_statement);

      log_assert(!variable_declaration->type->is_struct() && !variable_declaration->type->is_procedure() && !variable_declaration->type->is_enum(), "Cannot codegen C++ for variable with invalid type (type is one of procedure, struct, enum declaration)");

      codegen_cpp_from_variable_declaration_statement(variable_declaration, cpp_file);
    }
  }


  {
    // Procedure declarations.
    for (size_t proc_decl_index = 0; proc_decl_index < ast->proc_decls.size(); ++proc_decl_index) {
      auto procedure_declaration = ast->proc_decls[proc_decl_index];

      if (procedure_declaration->is_external) continue;

      if (procedure_declaration->name_token.contents == "main") continue;

      if (!procedure_declaration->type->is_procedure()) continue;

      auto proc_type = std::dynamic_pointer_cast<ProcedureType>(procedure_declaration->type);
      cpp_file.print("{} {}(", cpp_type_name(proc_type->return_type), procedure_declaration->name_token.contents);

      for (size_t arg_index = 0; arg_index < proc_type->arguments.size(); ++arg_index) {
        auto arg = proc_type->arguments[arg_index];
        cpp_file.print("{} {}", cpp_type_name(arg.type), arg.name.contents);

        if (arg_index < proc_type->arguments.size() - 1) cpp_file.print(", ");
      }

      cpp_file.print(");\n");
    }
    cpp_file.print("\n");
  }

  cpp_file.print("template <typename S, typename... Args>\n");
  cpp_file.print("void {}(const S& format_string, Args &&...args) {{\n", builtin_cpp_print_proc_name);
  cpp_file.print("  fmt::vprint(format_string, fmt::make_format_args(args...));\n");
  cpp_file.print("  fmt::print(\"\\n\");\n");
  cpp_file.print("}}\n");
  cpp_file.print("\n");

  {
    // Proceudre definitions.
    for (size_t proc_decl_index = 0; proc_decl_index < ast->proc_decls.size(); ++proc_decl_index) {
      auto& ast_proc_decl = ast->proc_decls[proc_decl_index];

      log_assert(ast_proc_decl->type->is_procedure(), "Generating C++ for a procedure definition, the procedure's type is not a ProcedureType.");
      auto proc_type = std::dynamic_pointer_cast<ProcedureType>(ast_proc_decl->type);

      if (ast_proc_decl->is_external) continue;

      if (ast_proc_decl->name_token.contents == "main") {
        cpp_file.print("int main(int _argc, char *_argv[]) {{\n");
        // @Todo: change name of args based on the argument name
        cpp_file.print("    std::vector<std::string> args;\n");
        cpp_file.print("    for (auto i = 0; i < _argc; ++i) args.push_back(_argv[i]);\n");
        cpp_file.print("\n");
      }
      else {
        cpp_file.print("{} {} ", cpp_type_name(ast_proc_decl->type), ast_proc_decl->name_token.contents);

        cpp_file.print("(");
        auto argc = proc_type->arguments.size();
        for (size_t arg_index = 0; arg_index < argc; ++arg_index) {
          auto arg = proc_type->arguments[arg_index];

          cpp_file.print("{} {}", cpp_type_name(arg.type), arg.name.contents);

          if (arg_index != argc - 1)  cpp_file.print(", ");
        }
        cpp_file.print(") {{\n");
      }

      assert(ast_proc_decl->body != nullptr);

      codegen_cpp_from_statement(ast_proc_decl->body, cpp_file);

      cpp_file.print("}}\n");
      cpp_file.print("\n");

      // runtime->frames.pop();
    }
  }

  // std::cout << cpp_file.rdbuf();
  // cpp_file.close();

  return true;
}
