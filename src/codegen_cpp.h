
#pragma once

#ifndef DRAGON_CPP_CODEGEN_H_
#define DRAGON_CPP_CODEGEN_H_

#include "ast.h"
#include "parser.h"

#include <string>
#include <filesystem>

bool codegen_cpp_for_ast(Ast* ast, const std::string &filepath);

#endif // DRAGON_CPP_CODEGEN_H_
