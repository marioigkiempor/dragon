
#if 0

#include "codegen_llvm.h"
#include <memory>

#include "ast.h"
#include "builtins.h"
#include "logging.h"

static llvm::Value* codegen_llvm_for_expression(std::shared_ptr<ExpressionAstNode> expression) {
  auto& llvm_builder = global_builtins.llvm_builder;

  switch(expression->expression_kind) {
  case ExpressionAstNode::Kind::LITERAL: {
    auto literal = std::dynamic_pointer_cast<LiteralExpression>(expression);
    switch (literal->literal_kind) {
    case LiteralExpression::Kind::INVALID:
    case LiteralExpression::Kind::U64:
    case LiteralExpression::Kind::F64:
    case LiteralExpression::Kind::BOOLEAN:
    case LiteralExpression::Kind::STRING:
      break;
    }
  } break;
  case ExpressionAstNode::Kind::GROUPING: {
    auto grouping = std::dynamic_pointer_cast<GroupingExpression>(expression);
    return codegen_llvm_for_expression(grouping->grouped_expression);
  } break;

  case ExpressionAstNode::Kind::CAST: {
    internal_error("@Todo: codegen LLVM for {} expression", expression->expression_kind);
  } break;

  case ExpressionAstNode::Kind::BINARY: {
    auto binary_expression = std::dynamic_pointer_cast<BinaryExpression>(expression);

    auto lhs_value = codegen_llvm_for_expression(binary_expression->lhs_expression);
    if (!lhs_value) {
      internal_error("Couldn't generate LLVM for lhs of binary expression {}", binary_expression);
    }

    auto rhs_value = codegen_llvm_for_expression(binary_expression->rhs_expression);
    if (!rhs_value) {
      internal_error("Couldn't generate LLVM for rhs of binary expression {}", binary_expression);
    }

    if (binary_expression->operatorr.info.kind == TokenKind::ADD) {
      return llvm_builder->CreateFAdd(lhs_value, rhs_value, "add_expression");
    }
    if (binary_expression->operatorr.info.kind == TokenKind::SUB) {
      return llvm_builder->CreateFSub(lhs_value, rhs_value, "sub_expression");
    }
    if (binary_expression->operatorr.info.kind == TokenKind::MUL) {
      return llvm_builder->CreateFMul(lhs_value, rhs_value, "mul_expression");
    }
    if (binary_expression->operatorr.info.kind == TokenKind::DIV) {
      return llvm_builder->CreateFDiv(lhs_value, rhs_value, "div_expression");
    }
    else {
      internal_error("@Todo: codegen LLVM for binary operator {}", binary_expression->operatorr);
    }
  } break;

  case ExpressionAstNode::Kind::UNARY: {

  } break;

  case ExpressionAstNode::Kind::VARIABLE_REFERENCE: {

  } break;

  case ExpressionAstNode::Kind::ARRAY_REFERENCE: {

  } break;

  case ExpressionAstNode::Kind::ARRAY_LITERAL: {

  } break;

  case ExpressionAstNode::Kind::PROCEDURE_CALL: {

  } break;

  case ExpressionAstNode::Kind::ASSIGNMENT: {

  } break;

  case ExpressionAstNode::Kind::INVALID: 
  default: internal_error("Cannot codegen LLVM for invalid expression kind: {}", (int)expression->expression_kind);
    break;
  }
}

bool codegen_llvm_for_ast(Ast* ast, const std::string &filepath) {
  
  log_assert(ast != nullptr, "Cannot codegen LLVM for null ast.");
  
  // build a 'main' function
  auto i32 = global_builtins.llvm_builder->getInt32Ty();
  auto prototype = llvm::FunctionType::get(i32, false);
  llvm::Function *main_fn = llvm::Function::Create(prototype, llvm::Function::ExternalLinkage, "main", global_builtins.llvm_module.get());
  llvm::BasicBlock *body = llvm::BasicBlock::Create(*global_builtins.llvm_context, "body", main_fn);
  global_builtins.llvm_builder->SetInsertPoint(body);

  for (auto& proc_decl : ast->proc_decls) {
    log_assert (!proc_decl->type->is_procedure(), "Cannot codegen LLVM for procedure whose type isn't a ProcedureType");

    auto proc_type = proc_decl->type;

  }

}


#endif