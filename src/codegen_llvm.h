
#if 0

#pragma once

#ifndef DRAGON_LLVM_CODEGEN_H_
#define DRAGON_LLVM_CODEGEN_H_

#include "ast.h"
#include "parser.h"

#include <string>
#include <filesystem>

bool codegen_llvm_for_ast(Ast* ast, const std::string &filepath);

#endif // DRAGON_LLVM_CODEGEN_H_


#endif
