#ifndef FLAG_H_
#define FLAG_H_

#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "defines.h"

#include "logging.h"

union CommandLineArg_Value {
  char*    as_str;
  uint64_t as_u64;
  bool     as_bool;
};

struct CommandLineArg {
  enum class Type {
    BOOL,
    U64,
    STR,
  };

  Type        type;
  const char* name;
  const char* desc;

  CommandLineArg_Value value;

  u64 default_val;
  u64 min;
  u64 max;

  bool mandatory;

  bool specified;
};

#ifndef FLAGS_CAP
#define FLAGS_CAP 1024
#endif  // FLAGS_CAP

bool*     flag_bool(const char* name, const char* desc, bool default_val);
uint64_t* flag_u64(const char* name, const char* desc, uint64_t default_val);
char**    flag_str(const char* name, const char* desc, const char* default_val);
void      flag_min(void* data_ptr, uint64_t min);
void      flag_max(void* data_ptr, uint64_t max);
void      flag_mandatory(void* data_ptr);
void      flags_parse(int argc, char* argv[]);
void      flags_print();

#endif  // FLAG_H_

#ifdef COMMAND_LINE_ARGS_IMPLEMENTATION

CommandLineArg flags[FLAGS_CAP];
size_t         flags_count;

CommandLineArg* flag_new(CommandLineArg::Type type, const char* name, const char* desc) {
  log_assert(flags_count < FLAGS_CAP, "Too many command line options defined.");

  CommandLineArg* flag = &flags[flags_count++];
  flag->name           = name;
  flag->desc           = desc;
  flag->type           = type;

  return flag;
}

bool* flag_bool(const char* name, const char* desc, bool default_val) {
  CommandLineArg* flag = flag_new(CommandLineArg::Type::BOOL, name, desc);

  uintptr_t default_value_punned;
  memcpy(&default_value_punned, &default_val, sizeof(default_value_punned));
  flag->default_val = default_value_punned;

  return &flag->value.as_bool;
}

uint64_t* flag_u64(const char* name, const char* desc, uint64_t default_val) {
  CommandLineArg* flag = flag_new(CommandLineArg::Type::U64, name, desc);

  uintptr_t default_value_punned;
  memcpy(&default_value_punned, &default_val, sizeof(default_value_punned));
  flag->default_val = default_value_punned;
  flag->min         = 0;
  flag->max         = UINT64_MAX;

  return &flag->value.as_u64;
}

char** flag_str(const char* name, const char* desc, const char* default_val) {
  CommandLineArg* flag = flag_new(CommandLineArg::Type::STR, name, desc);

  uintptr_t default_value_punned;
  memcpy(&default_value_punned, &default_val, sizeof(default_value_punned));
  flag->default_val = default_value_punned;

  return &flag->value.as_str;
}

CommandLineArg* flag_find_name(char* str) {
  for (size_t i = 0; i < flags_count; ++i) {
    if (strcmp(flags[i].name, str) == 0) {
      return &flags[i];
    }
  }

  return NULL;
}

CommandLineArg* flag_find_data(void* ptr) {
  for (size_t i = 0; i < flags_count; ++i) {
    if (ptr == &flags[i].value) {
      return &flags[i];
    }
  }

  return NULL;
}

void flag_min(void* data_ptr, uint64_t min) {
  CommandLineArg* flag = flag_find_data(data_ptr);
  if (!flag) internal_error("flag_min() couldn't find a flag pointed to by the pointer it was supplied{}", "");

  if (flag->type != CommandLineArg::Type::U64) internal_error("flag_min() can only set minimums for u64 flags{}", "");

  *((uint64_t*)&flag->min) = min;
}

void flag_max(void* data_ptr, uint64_t max) {
  CommandLineArg* flag = flag_find_data(data_ptr);
  if (!flag) internal_error("flag_max() couldn't find a flag pointed to by the pointer it was supplied{}", "");

  if (flag->type != CommandLineArg::Type::U64) internal_error("flag_max() can only set maximums for u64 flags{}", "");

  *((uint64_t*)&flag->max) = max;
}

void flag_mandatory(void* data_ptr) {
  CommandLineArg* flag = flag_find_data(data_ptr);

  if (!flag) internal_error("ERROR: flag_mandatory() couldn't find a flag pointed to by the pointer it was supplied{}", "");

  flag->mandatory = true;
}

void usage() {
  fmt::print("Usage: \n");
  flags_print();
}

void flags_parse(int argc, char* argv[]) {
  for (int i = 1; i < argc; ++i) {
    if (argv[i][0] == '-') {
      CommandLineArg* flag = flag_find_name(argv[i] + 1);
      if (!flag) {
        internal_error("ERROR: Invalid flag {}\n", argv[i] + 1);
        usage();
        exit(1);
      }

      flag->specified = true;

      switch (flag->type) {
        case CommandLineArg::Type::BOOL: {
          flag->value.as_bool = true;
        } break;

        case CommandLineArg::Type::U64: {
          if (argv[i + 1]) {
            flag->value.as_u64 = strtoull(argv[i + 1], NULL, 0);
          } else {
            log_error("Command line argument `{}` expects a u64 argument\n", argv[i]);
            usage();
            exit(1);
          }
        } break;

        case CommandLineArg::Type::STR: {
          if (argv[i + 1]) {
            flag->value.as_str = argv[i + 1];
          } else {
            log_error("Command line argument `{}` expects a string argument\n", argv[i]);
            usage();
            exit(1);
          }
        } break;

        default:
          internal_error("Tried to parse invalid CommandLineArg::Type: {}", (int)flag->type);
      }
    }
  }

  for (size_t i = 0; i < flags_count; ++i) {
    if (flags[i].specified)
      continue;

    if (flags[i].mandatory) {
      log_error("Mandatory flag '{}' has not been supplied", flags[i].name);
      usage();
      exit(1);
    }

    if (flags[i].type == CommandLineArg::Type::BOOL || flags[i].type == CommandLineArg::Type::U64)
      *(u64*)&flags[i].value = flags[i].default_val;
  }
}

void flags_print() {
  for (size_t i = 0; i < flags_count; ++i) {
    log("-{}", flags[i].name);
    if (flags[i].mandatory) {
      log(" (Mandatory)");
    }
    log("\n");
    log("     {}\n", flags[i].desc);

    switch (flags[i].type) {
      case CommandLineArg::Type::BOOL: {
        bool flag_default = flags[i].default_val;
        log("     Default: {}\n", flag_default ? "true" : "false");
      } break;

      case CommandLineArg::Type::U64: {
        log("     Default: {}, ", *((uint64_t*)&flags[i].default_val));
        log("Minimum: {}, ", *((uint64_t*)&flags[i].min));
        log("Maximum: {}\n", *((uint64_t*)&flags[i].max));
      } break;

      case CommandLineArg::Type::STR: {
        char* flag_default = (char*)flags[i].default_val;
        log("     Default: {}\n", flag_default);
      } break;

      default:
        internal_error("Tried to print invalid CommandLineArg::Type: {}", (int)flags[i].type);
    }
  }
}

#endif  // FLAG_IMPLEMENTATION
