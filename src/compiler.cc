

#include "compiler.h"

#include <string>

#include "system_helpers.h"

#include "builtins.h"
#include "logging.h"
#include "parser.h"
#include "typechecker.h"
#include "lexer.h"
#include "codegen_cpp.h"

Compilation_Result compile_file(Compilation_Options options) {
  [[maybe_unused]] auto string_replace = [](std::string &str, const std::string &from, const std::string &to) {
    size_t start_pos = str.find(from);

    if (start_pos == std::string::npos)  return false;

    str.replace(start_pos, from.length(), to);

    return true;
  };

  File source_file;
  if (!open(source_file, options.input_filepath)) {
    return Compilation_Result::COULDNT_OPEN_FILE;
  }

  auto lex_result = lex_file(source_file);
  if (lex_result.code != LexCode::OK) {
    return Compilation_Result::LEXING_FAILED;
  }

  auto parse_result = parse_file(lex_result.tokens, source_file);
  if (parse_result.code != Parse_Code::OK) {
    return Compilation_Result::PARSING_FAILED;
  }

  auto typecheck_result = typecheck_ast(parse_result.ast);
  if (typecheck_result.code != Typecheck_Code::OK) {
    return Compilation_Result::TYPECHECKING_FAILED;
  }


  ///
  /// Codegen C++
  ///

  auto cpp_output_filepath = options.cpp_output_filepath;
  if (cpp_output_filepath.empty()) {
    cpp_output_filepath = (options.input_filepath.parent_path() / options.input_filepath.stem()).string() + ".cpp";
  }

  bool did_cpp_codegen = codegen_cpp_for_ast(typecheck_result.ast, options.cpp_output_filepath.string());
  if (!did_cpp_codegen) {
    return Compilation_Result::CPP_CODEGEN_FAILED;
  }

  // std::ifstream the_cpp_file(options.cpp_output_filepath);
  // std::cout << the_cpp_file.rdbuf();

  auto cpp_compiler = global_builtins.cpp_compiler;
  auto cpp_compiler_flags = "--std=c++2a -Wall -Wextra -Wno-parentheses-equality -ggdb";
  std::string cmd = cpp_compiler + " " + cpp_compiler_flags + " ";

#if DRAGON_WINDOWS
  cmd.append("-I\"src/\" ");
  for (const auto& include_path : builtin_include_paths) cmd.append("-I\"" + include_path.string() + "\" ");

  cmd.append("-Fo\"build/\" ");
  cmd.append(options.cpp_output_filepath.string() + " ");
  cmd.append("-link /out:" + options.exe_output_filepath.string() + " ");
  
  for (const auto& library_path : builtin_library_paths) cmd.append("/LIBPATH:\"" + library_path.string() + "\" ");

  cmd.append("User32.lib SDL2.lib SDL2main.lib ");
#else
  cmd.append(options.cpp_output_filepath.string() + " ");

  cmd.append("-o " + options.exe_output_filepath.string() + " ");

  cmd.append("-I\"src/\" ");
  for (const auto& include_path : global_builtins.include_paths) cmd.append("-I\"" + include_path.string() + "\" ");

  // cmd.append("-object-file-name=\"build/\" ");

  for (const auto& library_path : global_builtins.library_paths) cmd.append("-L\"" + library_path.string() + "\" ");

  cmd.append("-D_REENTRANT -pthread ");       // Compilation flags
  cmd.append("-lm -lSDL2 -lSDL2main -ldl ");  // Link flags
#endif

  auto cpp_compilation_return_code = run_process_and_wait(cmd);
  if (cpp_compilation_return_code != 0) {
    log_error("Couldn't compile intermediate C++ file `{}` (C++ compiler `{}` returned with exit code {})", options.cpp_output_filepath.string(), cpp_compilation_return_code);
  }

  // Cleanup junk files produced by the C++ compiler.
  if (!options.keep_junk) {
    Vec<String> junk_extensions = { "pdb", "ilk" };

    if (!options.keep_cpp_file)  junk_extensions.push_back("cpp");

    for (auto& extension : junk_extensions) {
      auto junk_stem = options.input_filepath.parent_path() / options.input_filepath.stem();
      auto junk_path = junk_stem.string() + std::string(".") + extension;

      auto did_delete = delete_file(junk_path);
      if (did_delete) {
        log_info("Deleted intermediate file `{}`", junk_path);
      }
    }
  }

  return Compilation_Result::OK;
}
