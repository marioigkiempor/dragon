
#pragma once

#ifndef DRAGON_COMPILER_H
#define DRAGON_COMPILER_H

#include "defines.h"
#include "logging.h"

struct Compilation_Options {
  bool only_lex;

  Path input_filepath;
  Path exe_output_filepath = "out.exe";
  Path cpp_output_filepath;

  bool keep_cpp_file;
  bool keep_junk;
};

enum class Compilation_Result {
  OK,
  COULDNT_OPEN_FILE,
  LEXING_FAILED,
  PARSING_FAILED,
  TYPECHECKING_FAILED,
  CPP_CODEGEN_FAILED
};

Compilation_Result compile_file(Compilation_Options options);

template <>
struct fmt::formatter<Compilation_Result> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(Compilation_Result compilation_result, FormatContext &ctx) {
    switch(compilation_result) {
      case Compilation_Result::OK:                  return fmt::format_to(ctx.out(), "all good");
      case Compilation_Result::COULDNT_OPEN_FILE:   return fmt::format_to(ctx.out(), "couldn't open file");
      case Compilation_Result::LEXING_FAILED:       return fmt::format_to(ctx.out(), "lexing failed");
      case Compilation_Result::PARSING_FAILED:      return fmt::format_to(ctx.out(), "parsing failed");
      case Compilation_Result::TYPECHECKING_FAILED: return fmt::format_to(ctx.out(), "typechecking failed");
      case Compilation_Result::CPP_CODEGEN_FAILED:  return fmt::format_to(ctx.out(), "C++ codegen failed");
      default: internal_error("Tried to fmt invalid Compilation_Result.");
    }
    return ctx.out();
  }
};

#endif // DRAGON_COMPILER_H
