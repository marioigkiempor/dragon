
#include "defines.h"

#include <charconv>

bool string_view_to_bool(StringView string_view) {
  if (string_view == "true") {
    return true;
  }
  else if (string_view == "false") {
    return false;
  }

  fmt::print("Tried Boolean_Literal::string_view_to_bool got a StringView that wasn't `true` or `false` (got `{}`)", string_view);
  exit(1);
}

u64 string_view_to_u64(StringView string_view) {
  u64 the_value;
  auto from_chars_result = std::from_chars(string_view.data(), string_view.data() + string_view.size(), the_value);

  if (from_chars_result.ec != std::errc()) {
    fmt::print("string_view_to_u64() got value that isn't a u64 (got `{}`", string_view);
    exit(1);
  }

  return the_value;
}

f64 string_view_to_f64(StringView string_view) {
  f64 the_value;
  auto from_chars_result = std::from_chars(string_view.data(), string_view.data() + string_view.size(), the_value);

  if (from_chars_result.ec != std::errc()) {
    fmt::print("string_view_to_f64() got value that isn't a float (got `{}`", string_view);
    exit(1);
  }

  return the_value;
}