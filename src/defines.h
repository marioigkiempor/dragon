
#pragma once

#ifndef DRAGON_DEFINES_H_
#define DRAGON_DEFINES_H_




#include "fmt/format.h"

//
// Platform detection.
//
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
  #define DRAGON_WINDOWS 1
  #define DRAGON_UNIX    0
#else
  #define DRAGON_WINDOWS 0
  #define DRAGON_UNIX    1
#endif

// Tag for output function parameters.
#define OUT

//
// Integer types.
//

#include <cstdint>
#include <stddef.h>
using u64 = uint64_t;
using u32 = uint32_t;
using u16 = uint16_t;
using u8  = uint8_t;

using s64 = int64_t;
using s32 = int32_t;
using s16 = int16_t;
using s8  = int8_t;

using f32 = float;
using f64 = double;

using usize = size_t;


//
// String types.
//

#include <string>
using String     = std::string;

#include <string_view>
using StringView = std::string_view;

#include <charconv>
bool string_view_to_bool(StringView string_view);
u64  string_view_to_u64(StringView string_view);
f64 string_view_to_f64(StringView string_view);




//
// Files and file source locations.
//

#include <source_location>
using SourceLoc  = std::source_location;

#define HERE() std::source_location::current()

#include <filesystem>
using Path       = std::filesystem::path;




//
// Containers.
//

#include <vector>
template<typename T> using Vec = std::vector<T>;

#include <map>
template<typename V, typename K> using Table = std::map<V, K>;




//
// Pointers.
//

#include <memory>
template<typename T> using Box = std::unique_ptr<T>;
template<typename T> using Ref = std::shared_ptr<T>;



#endif // DRAGON_DEFINES_H_