
#include "lexer.h"

#include <string>

#include "system_helpers.h"
#include "logging.h"

#include "builtins.h"

// Token functions

static Token make_token(TokenKind kind, StringView contents, FileLoc file_loc) {
  auto info = global_builtins.token_infos.find(kind)->second;

  return Token(file_loc, info, contents);
}

Token make_builtin_token(StringView contents) {
  static FileLoc builtin_file_loc = FileLoc(0, "builtin", 0, 0);
  static TokenInfo builtin_info = TokenInfo(TokenKind::IDENTIFIER, 0, "");

  return Token(builtin_file_loc, builtin_info, contents);
}

static Token make_symbol_token(TokenKind kind, FileLoc file_loc) {
  auto info = global_builtins.token_infos.find(kind)->second;

  if (!(info.flags & TOKEN_FLAG_SYMBOL)) {
    internal_error("make_token_symbol() was given a TokenKind that isn't a symbol token (got `{}`)", kind);
  }

  return make_token(kind, "", file_loc);
}

static Token make_variable_token(TokenKind kind, StringView contents, FileLoc file_loc) {
  auto& info = global_builtins.token_infos.at(kind);
  log_assert(!(info.flags & TOKEN_FLAG_SYMBOL), "make_varible_token() got TokenKind which is a symbol (got `{}`)", kind);
  log_assert(!(info.flags & TOKEN_FLAG_KEYWORD), "make_varible_token() got TokenKind which is a keyword (got `{}`)", kind);

  return make_token(kind, contents, file_loc);
}




static Token next_token(Lexer& lexer);
static bool is_valid_identifier_character(char c);
static void next_character_on_this_line(Lexer& lexer);

LexResult lex_file(File& source_file) {
  if (source_file.size == 0) return { LexCode::EMPTY_FILE, {} };

  Lexer lexer = {};
  lexer.content = std::string_view(source_file.data, source_file.size);
  lexer.character = lexer.content[0];
  lexer.file_loc.path = source_file.path;

  Vec<Token> tokens = {};

  auto token = next_token(lexer);
  tokens.push_back(token);

  while(token.info.kind != TokenKind::END_OF_FILE) {
    token = next_token(lexer);
    tokens.push_back(token); 

    if (token.info.kind == TokenKind::INVALID) {
      return { LexCode::INVALID_TOKEN, tokens }; // The invalid token will be at the end of the Vec, to be consumed by the caller.
    }
  }

  return { LexCode::OK, tokens };
}

static char peek(Lexer& lexer, usize offset);
static void advance(Lexer& lexer);
static void next_character_on_this_line(Lexer& lexer);
static Token eat_string_literal(Lexer& lexer);
static Token eat_identifier(Lexer& lexer);
static Token eat_number(Lexer& lexer);
static void skip_whitespace(Lexer& lexer);

static char peek(Lexer& lexer, usize offset) {
  log_assert(lexer.file_loc.byte_offset + offset >= 0,                   "Lexer tried to peek to a character before the start of the input (tried to look at character index {})", lexer.file_loc.byte_offset + offset);
  log_assert(lexer.file_loc.byte_offset + offset < lexer.content.size(), "Lexer tried to peek to a character past the end of the source file (tried to look at character {})", lexer.file_loc.byte_offset + offset);

  return lexer.content[lexer.file_loc.byte_offset + offset];
}

static void skip_whitespace(Lexer& lexer) {  
  while (is_space(lexer.character))  advance(lexer);  
}



static void next_character_on_this_line(Lexer& lexer) {
  if (!is_newline(lexer.character) && lexer.character != '\0' && lexer.file_loc.byte_offset < lexer.content.size()) {
    lexer.file_loc.byte_offset += 1;
    lexer.character = lexer.content[lexer.file_loc.byte_offset];
    lexer.file_loc.col += 1;
  }
}

static void advance(Lexer& lexer) {
  next_character_on_this_line(lexer);

  while(is_newline(lexer.character)) {
    lexer.file_loc.byte_offset += 1;
    lexer.character = lexer.content[lexer.file_loc.byte_offset];
    lexer.file_loc.col = 0;
    lexer.file_loc.row += 1;
  }
}

static Token eat_string_literal(Lexer& lexer) {
  auto file_loc = lexer.file_loc;
  advance(lexer); // Skip starting `"`

  usize string_literal_start_index = lexer.file_loc.byte_offset;

  u32 string_literal_size = 0;
  while (lexer.character != '"') {
    string_literal_size += 1;
    advance(lexer);
  }

  advance(lexer); // Skip ending `"`

  auto string_literal = std::string_view(&lexer.content[string_literal_start_index], string_literal_size);
  return make_variable_token(TokenKind::STRING_LITERAL, string_literal, file_loc);
}

static Token eat_identifier(Lexer& lexer) {
  auto file_loc = lexer.file_loc;

  usize identifier_start_index = lexer.file_loc.byte_offset;

  u32 identifier_size = 0;
  while(is_valid_identifier_character(lexer.character)) {
    identifier_size += 1;
    advance(lexer);
  }

  auto identifier = std::string_view(&lexer.content[identifier_start_index], identifier_size);
  return make_variable_token(TokenKind::IDENTIFIER, identifier, file_loc);
}

static Token eat_number(Lexer& lexer) {
  auto file_loc = lexer.file_loc;

  auto s = std::string(&lexer.content[lexer.file_loc.byte_offset], lexer.content.size() - lexer.file_loc.byte_offset);

  [[maybe_unused]] double double_number = 0;
  usize characters_parsed = 0;

  try {
    double_number = std::stod(s, &characters_parsed);
  }
  catch ([[maybe_unused]] const std::invalid_argument& e) {
    internal_error("eat_number() encountered a source string that didn't start with a number (got `{}`).", lexer.character);
  }
  // @Todo: catch std::out_of_range exceptions.

  bool is_floating = false;
  if (characters_parsed >= 1) { // We have a number.
    // Check if the number was an integer
    for (usize i = 0; i < characters_parsed; ++i) {
      if (lexer.content[lexer.file_loc.byte_offset + i] == '.') is_floating = true;
    }
  }

  auto number_as_sv = std::string_view(&lexer.content[lexer.file_loc.byte_offset], characters_parsed);

  for (usize i = 0; i < characters_parsed; ++i)  advance(lexer);

  if (is_floating) return make_variable_token(TokenKind::FLOAT_LITERAL, number_as_sv, file_loc);
  else             return make_variable_token(TokenKind::INTEGER_LITERAL, number_as_sv, file_loc);
}



static Token next_token(Lexer& lexer) {
  auto advance_with_symbol = [](Lexer& lexer, u32 n, TokenKind token_kind, FileLoc file_loc) {
    for (u32 i = 0; i < n; ++i) advance(lexer);
    return make_symbol_token(token_kind, file_loc);
  };

  auto is_at_start_of_comment = [](Lexer& lexer) { return lexer.character == '/' && peek(lexer, 1) == '/'; };

  skip_whitespace(lexer);

  if (lexer.file_loc.byte_offset >= lexer.content.size()) return make_symbol_token(TokenKind::END_OF_FILE, lexer.file_loc);

  while(is_at_start_of_comment(lexer)) {
    while(!is_newline(lexer.character)) next_character_on_this_line(lexer);

    skip_whitespace(lexer);
  }

  if (isdigit(lexer.character)) return eat_number(lexer);

  if (is_valid_identifier_character(lexer.character)) {
    auto identifier = eat_identifier(lexer);

    if (global_builtins.keywords.contains(identifier.contents)) {
      auto keyword_token_kind = global_builtins.keywords.at(identifier.contents);
      auto keyword_token_info = global_builtins.token_infos[keyword_token_kind];
      auto keyword = Token(identifier.file_loc, keyword_token_info, identifier.contents);

      return keyword;
    }

    return identifier;
  } 

  auto file_loc = lexer.file_loc;

  switch(lexer.character) {
    case '\"': return eat_string_literal(lexer);
    case '\0': return make_symbol_token(TokenKind::END_OF_FILE, file_loc);

    case ';':  return advance_with_symbol(lexer, 1, TokenKind::SEMI,         file_loc);
    case ':':  return advance_with_symbol(lexer, 1, TokenKind::COLON,        file_loc);
    case ',':  return advance_with_symbol(lexer, 1, TokenKind::COMMA,        file_loc);
    case '{':  return advance_with_symbol(lexer, 1, TokenKind::OPEN_CURLY,   file_loc);
    case '}':  return advance_with_symbol(lexer, 1, TokenKind::CLOSE_CURLY,  file_loc);
    case '(':  return advance_with_symbol(lexer, 1, TokenKind::OPEN_PAREN,   file_loc);
    case ')':  return advance_with_symbol(lexer, 1, TokenKind::CLOSE_PAREN,  file_loc);
    case '[':  return advance_with_symbol(lexer, 1, TokenKind::OPEN_SQUARE,  file_loc);
    case ']':  return advance_with_symbol(lexer, 1, TokenKind::CLOSE_SQUARE, file_loc);
    case '^':  return advance_with_symbol(lexer, 1, TokenKind::POINTER,      file_loc);
    case '@':  return advance_with_symbol(lexer, 1, TokenKind::DEREFERENCE,  file_loc);

    case '!':  {
      if (peek(lexer, 1) == '=') return advance_with_symbol(lexer, 2, TokenKind::NOTEQ, file_loc);
      return advance_with_symbol(lexer, 1, TokenKind::BANG,         file_loc);
    } break;

    case '=': {
      if (peek(lexer, 1) == '=') return advance_with_symbol(lexer, 2, TokenKind::EQEQ, file_loc);
      return advance_with_symbol(lexer, 1, TokenKind::EQ, file_loc);
    }
    case '>': {
      if (peek(lexer, 1) == '=') return advance_with_symbol(lexer, 2, TokenKind::GE, file_loc);
      return advance_with_symbol(lexer, 1, TokenKind::GT, file_loc);
    }
    case '<': {
      if (peek(lexer, 1) == '=') return advance_with_symbol(lexer, 2, TokenKind::LE, file_loc);
      return advance_with_symbol(lexer, 1,  TokenKind::LT, file_loc);
    }
    case '+': {
      if (peek(lexer, 1) == '=') return advance_with_symbol(lexer, 2, TokenKind::ADD_EQ, file_loc);
      return advance_with_symbol(lexer, 1, TokenKind::ADD, file_loc);
    }
    case '-': {
      if (peek(lexer, 1) == '=') return advance_with_symbol(lexer, 2, TokenKind::SUB_EQ, file_loc);
      return advance_with_symbol(lexer, 1, TokenKind::SUB, file_loc);
    }
    case '*': {
      if (peek(lexer, 1) == '=') return advance_with_symbol(lexer, 2, TokenKind::MUL_EQ, file_loc);
      return advance_with_symbol(lexer, 1, TokenKind::MUL, file_loc);
    }
    case '/': {
      if (peek(lexer, 1) == '=') return advance_with_symbol(lexer, 2, TokenKind::DIV_EQ, file_loc);
      return advance_with_symbol(lexer, 1, TokenKind::DIV, file_loc);
    }
    case '%': {
      if (peek(lexer, 1) == '=') return advance_with_symbol(lexer, 2, TokenKind::MOD_EQ, file_loc);
      return advance_with_symbol(lexer, 1, TokenKind::MOD, file_loc);
    }
    case '.': {
      if (peek(lexer, 1) == '.') return advance_with_symbol(lexer, 2, TokenKind::DOTDOT, file_loc);
      return advance_with_symbol(lexer, 1, TokenKind::DOT, file_loc);
    }
    case '|': {
      if (peek(lexer, 1) == '|') return advance_with_symbol(lexer, 2, TokenKind::OROR, file_loc);
      if (peek(lexer, 1) == '=') return advance_with_symbol(lexer, 2, TokenKind::OR_EQ, file_loc);
      return advance_with_symbol(lexer, 1, TokenKind::OR, file_loc);
    }
    case '&': {
      if (peek(lexer, 1) == '&') return advance_with_symbol(lexer, 2, TokenKind::ANDAND, file_loc);
      if (peek(lexer, 1) == '=') return advance_with_symbol(lexer, 2, TokenKind::AND_EQ, file_loc);
      return advance_with_symbol(lexer, 1, TokenKind::AND, file_loc);
    }

    default: internal_error("{}: Lexer ran into unlexable character: {}", lexer.file_loc, lexer.character);
  }

  internal_error("next_token() did not return a token.");
  return make_builtin_token("<unlexable>");
}

static bool is_valid_identifier_character(char c) {
  return isalnum(c) || c == '_';
}