
#pragma once

#ifndef DRAGON_LEXER_H_
#define DRAGON_LEXER_H_


#include "defines.h"
#include "logging.h"
#include "system_helpers.h"

constexpr u32 TOKEN_FLAG_SYMBOL =          0x1;
constexpr u32 TOKEN_FLAG_KEYWORD =         0x2;
constexpr u32 TOKEN_FLAG_UNARY_OPERATOR =  0x4;
constexpr u32 TOKEN_FLAG_BINARY_OPERATOR = 0x8;
constexpr u32 TOKEN_FLAG_COMPARATOR =      0x10;
constexpr u32 TOKEN_FLAG_ASSIGNMENT =      0x20;

// Update this when adding a new token.
constexpr u8 token_kind_count = 60; // Bit messy but avoids extra enum member (eg COUNT)
enum class TokenKind {
    INVALID, END_OF_FILE,

    ALIAS, AS, BREAK, DEFER, ELSE, ENUM, EXTERNAL, FOR, FALSE, IF, IS, INCLUDE, MATCH, RETURN, STRUCT, TRUE, WHILE, 

    STRING_LITERAL, INTEGER_LITERAL, FLOAT_LITERAL,

    IDENTIFIER,

    OPEN_PAREN, CLOSE_PAREN, OPEN_CURLY, CLOSE_CURLY, OPEN_SQUARE, CLOSE_SQUARE, 

    EQ, COMMA, SEMI, COLON,
    EQEQ, NOTEQ,

    LT, GT, LE, GE,

    ADD, SUB, MUL, DIV, MOD,

    DOT, DOTDOT,

    AND, ANDAND, OR, OROR,

    ADD_EQ, SUB_EQ, MUL_EQ, DIV_EQ, MOD_EQ,
    AND_EQ, OR_EQ,

    BANG, POINTER, DEREFERENCE
};

struct TokenInfo {
  TokenKind kind = TokenKind::INVALID;
  u32 flags = 0;
  StringView name = "";

  TokenInfo() = default;
  TokenInfo(TokenKind kind, u32 flags, StringView name) 
  : kind(kind)
  , flags(flags)
  , name(name) {
  }
};

struct Token {
  FileLoc   file_loc = {};
  TokenInfo info     = {};

  StringView contents = {};

  Token() = default;
  Token(FileLoc file_loc, TokenInfo info, StringView contents)
  : file_loc(file_loc)
  , info(info)
  , contents(contents) {}

  bool operator==(const Token& other) const { return this->file_loc == other.file_loc; }
  bool operator!=(const Token& other) const { return !(*this == other); }

  usize length() const {
    switch(this->info.kind) {
    case TokenKind::ALIAS:
    case TokenKind::AS:
    case TokenKind::BREAK:
    case TokenKind::DEFER:
    case TokenKind::ELSE:
    case TokenKind::ENUM:
    case TokenKind::EXTERNAL:
    case TokenKind::FOR:
    case TokenKind::FALSE:
    case TokenKind::IF:
    case TokenKind::IS:
    case TokenKind::INCLUDE:
    case TokenKind::MATCH:
    case TokenKind::RETURN:
    case TokenKind::STRUCT:
    case TokenKind::TRUE:
    case TokenKind::WHILE:
    case TokenKind::OPEN_PAREN:
    case TokenKind::CLOSE_PAREN:
    case TokenKind::OPEN_CURLY:
    case TokenKind::CLOSE_CURLY:
    case TokenKind::OPEN_SQUARE:
    case TokenKind::CLOSE_SQUARE:
    case TokenKind::EQ:
    case TokenKind::COMMA:
    case TokenKind::SEMI:
    case TokenKind::COLON:
    case TokenKind::EQEQ:
    case TokenKind::NOTEQ:
    case TokenKind::LT:
    case TokenKind::GT:
    case TokenKind::LE:
    case TokenKind::GE:
    case TokenKind::ADD:
    case TokenKind::SUB:
    case TokenKind::MUL:
    case TokenKind::DIV:
    case TokenKind::MOD:
    case TokenKind::DOT:
    case TokenKind::DOTDOT:
    case TokenKind::AND:
    case TokenKind::ANDAND:
    case TokenKind::OR:
    case TokenKind::OROR:
    case TokenKind::ADD_EQ:
    case TokenKind::SUB_EQ:
    case TokenKind::MUL_EQ:
    case TokenKind::DIV_EQ:
    case TokenKind::MOD_EQ:
    case TokenKind::AND_EQ:
    case TokenKind::OR_EQ:
    case TokenKind::BANG:
    case TokenKind::POINTER:
    case TokenKind::DEREFERENCE:
      return this->info.name.length();

    case TokenKind::STRING_LITERAL:
    case TokenKind::INTEGER_LITERAL:
    case TokenKind::FLOAT_LITERAL:
    case TokenKind::IDENTIFIER:
      return this->contents.length();

    case TokenKind::END_OF_FILE:
      return 0;

    case TokenKind::INVALID:
    default:
      internal_error("Tried to get the length of invalid token kind {}.", (int)this->info.kind);
      return 0;
    }
  }
};

Token make_builtin_token(StringView contents);

struct Lexer {
    FileLoc file_loc = { 0, "", 0, 0 };
    StringView content = "";
    char character = 0;

    Lexer() = default;
};

enum class LexCode {
    OK,
    EMPTY_FILE,
    INVALID_TOKEN,
};

struct LexResult {
    LexCode code = LexCode::OK;
    Vec<Token> tokens = {};
};

LexResult lex_file(File& source_file);






#include "fmt/core.h"
#include "fmt/format.h"
#include "logging.h"

template <>
struct fmt::formatter<TokenKind> {
	constexpr auto parse(fmt::format_parse_context& ctx) { return ctx.end(); }

	template <typename FormatContext>
	auto format(const TokenKind& token_kind, FormatContext& ctx) {
		switch(token_kind) {
        case TokenKind::INVALID:     return fmt::format_to(ctx.out(), "invalid");
        case TokenKind::END_OF_FILE: return fmt::format_to(ctx.out(), "eof");

        case TokenKind::ALIAS:    return fmt::format_to(ctx.out(), "alias");
        case TokenKind::AS:       return fmt::format_to(ctx.out(), "as");
        case TokenKind::BREAK:    return fmt::format_to(ctx.out(), "break");
        case TokenKind::DEFER:    return fmt::format_to(ctx.out(), "defer");
        case TokenKind::ELSE:     return fmt::format_to(ctx.out(), "else");
        case TokenKind::ENUM:     return fmt::format_to(ctx.out(), "enum");
        case TokenKind::EXTERNAL: return fmt::format_to(ctx.out(), "external");
        case TokenKind::FALSE:    return fmt::format_to(ctx.out(), "false");
        case TokenKind::FOR:      return fmt::format_to(ctx.out(), "for");
        case TokenKind::IF:       return fmt::format_to(ctx.out(), "if");
        case TokenKind::IS:       return fmt::format_to(ctx.out(), "is");
        case TokenKind::INCLUDE:  return fmt::format_to(ctx.out(), "include");
        case TokenKind::MATCH:    return fmt::format_to(ctx.out(), "match");
        case TokenKind::RETURN:   return fmt::format_to(ctx.out(), "return");
        case TokenKind::STRUCT:   return fmt::format_to(ctx.out(), "struct");
        case TokenKind::TRUE:     return fmt::format_to(ctx.out(), "true");
        case TokenKind::WHILE:    return fmt::format_to(ctx.out(), "while");

        case TokenKind::STRING_LITERAL:  return fmt::format_to(ctx.out(), "string literla");
        case TokenKind::INTEGER_LITERAL: return fmt::format_to(ctx.out(), "integer literal");
        case TokenKind::FLOAT_LITERAL:   return fmt::format_to(ctx.out(), "float literal");
        case TokenKind::IDENTIFIER:      return fmt::format_to(ctx.out(), "identifier");

        case TokenKind::OPEN_PAREN:   return fmt::format_to(ctx.out(), "(");
        case TokenKind::CLOSE_PAREN:  return fmt::format_to(ctx.out(), ")");
        case TokenKind::OPEN_CURLY:   return fmt::format_to(ctx.out(), "{{");
        case TokenKind::CLOSE_CURLY:  return fmt::format_to(ctx.out(), "}}");
        case TokenKind::OPEN_SQUARE:  return fmt::format_to(ctx.out(), "[");
        case TokenKind::CLOSE_SQUARE: return fmt::format_to(ctx.out(), "]");

        case TokenKind::EQ:     return fmt::format_to(ctx.out(), "=");
        case TokenKind::COMMA:  return fmt::format_to(ctx.out(), ",");
        case TokenKind::SEMI:   return fmt::format_to(ctx.out(), ";");
        case TokenKind::COLON:  return fmt::format_to(ctx.out(), ":");
        case TokenKind::EQEQ:   return fmt::format_to(ctx.out(), "==");
        case TokenKind::NOTEQ:  return fmt::format_to(ctx.out(), "!=");

        case TokenKind::LT: return fmt::format_to(ctx.out(), "<");
        case TokenKind::GT: return fmt::format_to(ctx.out(), ">");
        case TokenKind::LE: return fmt::format_to(ctx.out(), "<=");
        case TokenKind::GE: return fmt::format_to(ctx.out(), ">=");

        case TokenKind::ADD: return fmt::format_to(ctx.out(), "+");
        case TokenKind::SUB: return fmt::format_to(ctx.out(), "-");
        case TokenKind::MUL: return fmt::format_to(ctx.out(), "*");
        case TokenKind::DIV: return fmt::format_to(ctx.out(), "/");
        case TokenKind::MOD: return fmt::format_to(ctx.out(), "%");

        case TokenKind::DOT:    return fmt::format_to(ctx.out(), ".");
        case TokenKind::DOTDOT: return fmt::format_to(ctx.out(), "..");

        case TokenKind::AND:    return fmt::format_to(ctx.out(), "&");
        case TokenKind::ANDAND: return fmt::format_to(ctx.out(), "&&");
        case TokenKind::OR:     return fmt::format_to(ctx.out(), "|");
        case TokenKind::OROR:   return fmt::format_to(ctx.out(), "||");

        case TokenKind::ADD_EQ: return fmt::format_to(ctx.out(), "+=");
        case TokenKind::SUB_EQ: return fmt::format_to(ctx.out(), "-=");
        case TokenKind::MUL_EQ: return fmt::format_to(ctx.out(), "*=");
        case TokenKind::DIV_EQ: return fmt::format_to(ctx.out(), "/=");
        case TokenKind::MOD_EQ: return fmt::format_to(ctx.out(), "%=");
        case TokenKind::AND_EQ: return fmt::format_to(ctx.out(), "&=");
        case TokenKind::OR_EQ: return fmt::format_to(ctx.out(), "|=");

        case TokenKind::BANG:        return fmt::format_to(ctx.out(), "!");
        case TokenKind::POINTER:     return fmt::format_to(ctx.out(), "^");
        case TokenKind::DEREFERENCE: return fmt::format_to(ctx.out(), "@");
        
        default: internal_error("Tried to print invalid TokenKind `{}`", (int)token_kind);
        }

        internal_error("Failed to fmt TokenKind `{}`", token_kind);
        return fmt::format_to(ctx.out(), "invalid");
	}
};

template <>
struct fmt::formatter<Token> {
	constexpr auto parse(fmt::format_parse_context& ctx) { return ctx.end(); }

	template <typename FormatContext>
	auto format(const Token& token, FormatContext& ctx) {
        if (token.info.flags & TOKEN_FLAG_SYMBOL) return fmt::format_to(ctx.out(), "{}", token.info.kind);

        if (token.info.kind == TokenKind::STRING_LITERAL) return fmt::format_to(ctx.out(), "\"{}\"", token.contents);

        if (token.info.kind == TokenKind::IDENTIFIER) return fmt::format_to(ctx.out(), "`{}`", token.contents);

        return fmt::format_to(ctx.out(), "{}", token.contents);
    }
};



#endif // DRAGON_LEXER_H_