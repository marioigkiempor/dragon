
#include "logging.h"

void log_print(const char* file, int line, fmt::string_view format, fmt::format_args args) {
  fmt::print("{}:{}: ", file, line);
  fmt::vprint(format, args);
}

void vlog_print(fmt::string_view format, fmt::format_args args) {
  fmt::vprint(format, args);
}

void internal_error_implementation(const DebugLogString& format, fmt::format_args args) {
  fmt::print(fmt::emphasis::bold | fmt::fg(log_error_color),    "[INTERNAL ERROR] ");
  fmt::print(fmt::emphasis::bold | fmt::fg(log_error_color),    "{}:{}: ", format.loc.file_name(), format.loc.line());
  fmt::vprint(format.str, args);
  fmt::print("\n");
}
