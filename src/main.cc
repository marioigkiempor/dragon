
#include <cstdlib>
#include <iostream>

#include "command_line_args.h"
#include "defines.h"

#include "builtins.h"
#include "lexer.h"
#include "parser.h"
#include "compiler.h"

struct CommandLineArguments {
  char** input_filepath = nullptr;
  char** output_filepath = nullptr;
  bool*  run_tests = nullptr;
  bool*  lex_only = nullptr;
};

// static std::map<std::string, llvm::Value *> NamedValues;

int main(int argc, char* argv[]) {

  // Parse command line arguments
  CommandLineArguments command_line_args = {};
  command_line_args.input_filepath  = flag_str("i", "Input .drag file to compile.", "main.drag");
  command_line_args.output_filepath = flag_str("o", "Output executable name", "out.exe");
  command_line_args.run_tests = flag_bool("tests", "Compile and run all .drag files in the `tests` directory", false);
  command_line_args.lex_only  = flag_bool("only_lex", "Only lex the input file. Print all tokens", false);
  // flag_mandatory(input_filepath);
  flags_parse(argc, argv);

  init(global_builtins);

  // Run the test suite.
  if (*command_line_args.run_tests) {
    std::string tests_directory = "tests";
    for (const auto& entry : std::filesystem::directory_iterator(tests_directory)) {
      auto& path = entry.path();
      if (path.extension() != ".drag")
        continue;


      auto full_stem = tests_directory / path.stem();

      Compilation_Options options;
      options.input_filepath = full_stem.string() + ".drag";
      options.cpp_output_filepath = full_stem.string() + ".cpp";
      options.exe_output_filepath = full_stem.string() + ".exe";
      options.only_lex = *command_line_args.lex_only;
      options.keep_cpp_file = false;
      options.keep_junk = false;

      [[maybe_unused]] auto compilation_result = compile_file(options);

      run_process_and_wait(options.exe_output_filepath.string());

      delete_file(options.exe_output_filepath.string());
    }

    return 0;
  }

  // Compile the file given by the command line arguments.
  auto input_filepath = std::filesystem::path(*command_line_args.input_filepath);
  auto file_name      = input_filepath.parent_path() / input_filepath.stem();

  Compilation_Options options;
  options.input_filepath = input_filepath;
  options.cpp_output_filepath = file_name.string() + ".cpp";
  options.exe_output_filepath = file_name.string() + ".exe";
  options.keep_junk = true;

  // @Todo: sanitize output filepath.
  if (*command_line_args.output_filepath)
  	options.exe_output_filepath = *command_line_args.output_filepath;

  options.only_lex = *command_line_args.lex_only;

  auto compilation_result = compile_file(options);
  if (compilation_result != Compilation_Result::OK) {
  	log_error("Couldn't compile `{}`: {}", options.input_filepath.string(), compilation_result);
  	return -1;
  }

  return 0;
}

#define COMMAND_LINE_ARGS_IMPLEMENTATION
#include "command_line_args.h"