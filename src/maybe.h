
#pragma once

#ifndef DRAGON_MAYBE_H_
#define DRAGON_MAYBE_H_

template <typename T>
struct Maybe {
  T value = {};
  bool has_value = false;
};

template <typename T>
Maybe<T> nothing(T value = {}) {
  return { value, false };
}

template <typename T>
Maybe<T> just(T value) {
  return { value, true };
}



#endif // DRAGON_MAYBE_H_
