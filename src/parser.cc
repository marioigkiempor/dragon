
#include "parser.h"

#include <iostream>
#include <memory>
#include <string_view>

#include "defines.h"

#include "logging.h"
#include "ast.h"
#include "lexer.h"
#include "maybe.h"
#include "typechecker.h"

///
/// Parser functions
///

// @Todo: parser_error_at_current_token(Parser *parser, const char *message)
template <typename S, typename... Args>
void parser_error_at_token(Parser& parser, Token token, const S& format_string, Args &&...args) {
  if (parser.panic_mode)  return;

  parser.panic_mode = true;
  parser.had_error = true;

  fmt::print("{}\n", token.file_loc);
  log_error(format_string, args...);

  const auto error_filepath = token.file_loc.path.string();
  const auto [source_has_been_loaded, source] = parser.ast.included_file_infos.at(error_filepath);
  log_assert(source_has_been_loaded, "Cannot report parse error in file which has not been loaded (filepath `{}`)", error_filepath);
  dragon_annotated_source(token, source, format_string, args...);
}

///
/// Parsing system
///
typedef enum {
  PREC_NONE = 0,
  PREC_ASSIGNMENT, // =
  PREC_OR,         // or
  PREC_AND,        // and
  PREC_EQUALITY,   // == !=
  PREC_COMPARISON, // < > <= >=
  PREC_TERM,       // + -
  PREC_FACTOR,     // * /
  PREC_UNARY,      // ! -
  PREC_CALL,       // . ()
  PREC_PRIMARY
} Precedence;

// NOTE Expression parsing functions
typedef void (*Expr_Parse_Fn)(Parser&, Ref<ExpressionAstNode> &, bool);
static Ref<ExpressionAstNode> parse_expression(Parser&);
static void parse_expression_using_precedence_rule(Parser&, Ref<ExpressionAstNode> &, Precedence);
static void parse_expression_integer(Parser&, Ref<ExpressionAstNode> &, bool);
static void parse_expression_float(Parser&, Ref<ExpressionAstNode> &, bool);
static void parse_expression_literal(Parser&, Ref<ExpressionAstNode> &, bool);
static void parse_expression_grouping(Parser&, Ref<ExpressionAstNode> &, bool);
static void parse_expression_unary(Parser&, Ref<ExpressionAstNode> &, bool);
static void parse_expression_binary(Parser&, Ref<ExpressionAstNode> &, bool);
static void parse_expression_string(Parser&, Ref<ExpressionAstNode> &, bool);
static void parse_expression_var_ref(Parser&, Ref<ExpressionAstNode> &, bool);
static void parse_expression_proc_call(Parser&, Ref<ExpressionAstNode> &, bool);

static Ref<CodeBlockStatement> parse_code_block_statement(Parser&);
static Ref<TypeAliasStatement> parse_type_alias_statement(Parser&);

static Ref<StatementAstNode> parse_statement(Parser&, bool ignore_semicolon_at_the_end_of_expr_statement = false);
static void parse_top_level_statement(Parser&);

struct Parse_Rule {
  Expr_Parse_Fn prefix;
  Expr_Parse_Fn infix;
  Precedence precedence;
};

static Parse_Rule get_parse_rule(TokenKind token_kind) {
  switch (token_kind) {
  case TokenKind::OPEN_SQUARE:     return { parse_expression_literal,  NULL,                       PREC_NONE       };
  case TokenKind::CLOSE_SQUARE:    return { NULL,                      NULL,                       PREC_NONE       };
  case TokenKind::ALIAS:           return { NULL,                      NULL,                       PREC_NONE       };
  case TokenKind::BREAK:           return { NULL,                      NULL,                       PREC_NONE       };
  case TokenKind::WHILE:           return { NULL,                      NULL,                       PREC_NONE       };
  case TokenKind::FOR:             return { NULL,                      NULL,                       PREC_NONE       };
  case TokenKind::STRUCT:          return { NULL,                      NULL,                       PREC_NONE       };
  case TokenKind::RETURN:          return { NULL,                      NULL,                       PREC_NONE       };
  case TokenKind::DEFER:           return { NULL,                      NULL,                       PREC_NONE       };
  case TokenKind::INCLUDE:         return { NULL,                      NULL,                       PREC_NONE       };
  case TokenKind::MATCH:           return { NULL,                      NULL,                       PREC_NONE       };
  case TokenKind::EXTERNAL:        return { NULL,                      NULL,                       PREC_NONE       };
  case TokenKind::AS:              return { NULL,                      parse_expression_binary,    PREC_CALL       }; // @Correctness: Casting should have high precedence but not sure what exactly.
  case TokenKind::IF:              return { NULL,                      NULL,                       PREC_NONE       };
  case TokenKind::IS:              return { NULL,                      NULL,                       PREC_NONE       };
  case TokenKind::ELSE:            return { NULL,                      NULL,                       PREC_NONE       };
  case TokenKind::ENUM:            return { NULL,                      NULL,                       PREC_NONE       };
  case TokenKind::EQ:              return { NULL,                      NULL,                       PREC_ASSIGNMENT };
  case TokenKind::CLOSE_PAREN:     return { NULL,                      NULL,                       PREC_NONE       };
  case TokenKind::OPEN_CURLY:      return { NULL,                      NULL,                       PREC_NONE       };
  case TokenKind::CLOSE_CURLY:     return { NULL,                      NULL,                       PREC_NONE       };
  case TokenKind::COMMA:           return { NULL,                      NULL,                       PREC_NONE       };
  case TokenKind::SEMI:            return { NULL,                      NULL,                       PREC_NONE       };
  case TokenKind::COLON:           return { NULL,                      NULL,                       PREC_NONE       };
  case TokenKind::OPEN_PAREN:      return { parse_expression_grouping, parse_expression_proc_call, PREC_CALL       };
  case TokenKind::TRUE:            return { parse_expression_literal,  NULL,                       PREC_NONE       };
  case TokenKind::FALSE:           return { parse_expression_literal,  NULL,                       PREC_NONE       };
  case TokenKind::EQEQ:            return { NULL,                      parse_expression_binary,    PREC_EQUALITY   };
  case TokenKind::NOTEQ:           return { NULL,                      parse_expression_binary,    PREC_EQUALITY   };
  case TokenKind::LT:              return { NULL,                      parse_expression_binary,    PREC_COMPARISON };
  case TokenKind::GT:              return { NULL,                      parse_expression_binary,    PREC_COMPARISON };
  case TokenKind::LE:              return { NULL,                      parse_expression_binary,    PREC_COMPARISON };
  case TokenKind::GE:              return { NULL,                      parse_expression_binary,    PREC_COMPARISON };
  case TokenKind::AND:             return { NULL,                      parse_expression_binary,    PREC_AND        };
  case TokenKind::OR:              return { NULL,                      parse_expression_binary,    PREC_OR         };
  case TokenKind::ANDAND:          return { NULL,                      parse_expression_binary,    PREC_AND        };
  case TokenKind::OROR:            return { NULL,                      parse_expression_binary,    PREC_OR         };
  case TokenKind::BANG:            return { parse_expression_unary,    NULL,                       PREC_UNARY      };
  case TokenKind::POINTER:         return { parse_expression_unary,    NULL,                       PREC_UNARY      };
  case TokenKind::DEREFERENCE:     return { parse_expression_unary,    NULL,                       PREC_UNARY      };
  case TokenKind::ADD:             return { NULL,                      parse_expression_binary,    PREC_TERM       };
  case TokenKind::MUL:             return { NULL,                      parse_expression_binary,    PREC_FACTOR     };
  case TokenKind::DIV:             return { NULL,                      parse_expression_binary,    PREC_FACTOR     };
  case TokenKind::MOD:             return { NULL,                      parse_expression_binary,    PREC_FACTOR     };
  case TokenKind::DOT:             return { NULL,                      parse_expression_binary,    PREC_CALL       };
  case TokenKind::DOTDOT:          return { NULL,                      parse_expression_binary,    PREC_CALL       };
  case TokenKind::SUB:             return { parse_expression_unary,    parse_expression_binary,    PREC_TERM       };
  case TokenKind::MUL_EQ:          return { NULL,                      NULL,                       PREC_ASSIGNMENT };
  case TokenKind::DIV_EQ:          return { NULL,                      NULL,                       PREC_ASSIGNMENT };
  case TokenKind::ADD_EQ:          return { NULL,                      NULL,                       PREC_ASSIGNMENT };
  case TokenKind::MOD_EQ:          return { NULL,                      NULL,                       PREC_ASSIGNMENT };
  case TokenKind::SUB_EQ:          return { NULL,                      NULL,                       PREC_ASSIGNMENT };
  case TokenKind::AND_EQ:          return { NULL,                      NULL,                       PREC_ASSIGNMENT };
  case TokenKind::OR_EQ:           return { NULL,                      NULL,                       PREC_ASSIGNMENT };
  case TokenKind::IDENTIFIER:      return { parse_expression_var_ref,  NULL,                       PREC_NONE       };
  case TokenKind::INTEGER_LITERAL: return { parse_expression_integer,  NULL,                       PREC_NONE       };
  case TokenKind::FLOAT_LITERAL:   return { parse_expression_float,    NULL,                       PREC_NONE       };
  case TokenKind::STRING_LITERAL:  return { parse_expression_string,   NULL,                       PREC_NONE       };
  case TokenKind::END_OF_FILE:     return { NULL,                      NULL,                       PREC_NONE       };

  case TokenKind::INVALID:
  default:
    internal_error("No parse rule defined for invalid TokenKind {}", (int)token_kind);
    exit(1);
    // UNREACHABLE();
  }

  internal_error("Couldn't find a parse rule for TokenKind {}", (int)token_kind);
}

static void parser_advance(Parser& parser) {
  parser.previous_token = parser.current_token;
  if (parser.token_index < parser.tokens.size()) {
    parser.current_token = parser.tokens[parser.token_index++];

    if (parser.token_index < parser.tokens.size()) parser.next_token = parser.tokens[parser.token_index];
    else                                           parser.next_token = parser.current_token;
  }
}

// NOTE When the parser encounters a TOKEN_INVALID, it'll enter this procedure.
// This procedure will skip tokens until it reaches the end of the statement.
static void synchronise(Parser& parser) {
  parser.panic_mode = false;

  while (parser.current_token.info.kind != TokenKind::END_OF_FILE) {
    if (parser.previous_token.info.kind == TokenKind::SEMI)        return;
    if (parser.previous_token.info.kind == TokenKind::CLOSE_CURLY) return;

    if (parser.current_token.info.kind == TokenKind::ALIAS)        return;
    if (parser.current_token.info.kind == TokenKind::DEFER)        return;
    if (parser.current_token.info.kind == TokenKind::FOR)          return;
    if (parser.current_token.info.kind == TokenKind::IF)           return;
    if (parser.current_token.info.kind == TokenKind::MATCH)        return;
    if (parser.current_token.info.kind == TokenKind::RETURN)       return;
    if (parser.current_token.info.kind == TokenKind::OPEN_CURLY)   return;
    if (parser.current_token.info.kind == TokenKind::INCLUDE)      return;
    if (parser.current_token.info.kind == TokenKind::WHILE)        return;
    if (parser.current_token.info.kind == TokenKind::IDENTIFIER)   return;

    // Keep going until you hit one of the tokens listed above.
    parser_advance(parser);
  }
}

static Token consume_token(Parser& parser, TokenKind expected_kind, const char *message) {
  if (parser.current_token.info.kind == expected_kind) {
    parser_advance(parser);
    return parser.previous_token;
  }

  parser_error_at_token(parser, parser.current_token, message);
  return parser.current_token;
}

static Vec<Token> consume_type(Parser& parser) {
  auto consume_argument_list = [](Parser& parser, Vec<Token>& unparsed_type) {
    unparsed_type.push_back(consume_token(parser, TokenKind::OPEN_PAREN, "Expected `(` to start argument list in procedure type declaration."));

    if (parser.current_token.info.kind != TokenKind::CLOSE_PAREN) {
      do {
        auto name = consume_token(parser, TokenKind::IDENTIFIER, "Expected parameter name.");
        unparsed_type.push_back(name);
        unparsed_type.push_back(consume_token(parser, TokenKind::COLON, "Expected `:` seperating argument name and type."));

        auto type_tokens = consume_type(parser);
        for (size_t i = 0; i < type_tokens.size(); ++i) unparsed_type.push_back(type_tokens[i]);

        if (parser.current_token.info.kind == TokenKind::COMMA) {
          unparsed_type.push_back(consume_token(parser, TokenKind::COMMA, "Expected comma between procedure arguments."));
        }
      } while (parser.current_token.info.kind != TokenKind::CLOSE_PAREN);
    }

    unparsed_type.push_back(consume_token(parser, TokenKind::CLOSE_PAREN, "Expected `)` to end argument list in procedure type declaration."));
  };

  Vec<Token> type_tokens = {};

  if (parser.current_token.info.kind == TokenKind::OPEN_PAREN) {
    consume_argument_list(parser, type_tokens);

    // result->procedure.return_type = parser_consume_type(parser);
    if (parser.current_token.info.kind != TokenKind::SEMI &&
        parser.current_token.info.kind != TokenKind::OPEN_CURLY) {
      // @Note: a procedure signature necessarily ends with a `;` or `{`.
      //        If we don't see one of those, there must be a return type blocking us.
      auto return_type_tokens = consume_type(parser);
      for (size_t i = 0; i < return_type_tokens.size(); ++i) type_tokens.push_back(return_type_tokens[i]);
    }

    return type_tokens;
  }

  if (parser.current_token.info.kind == TokenKind::ENUM) {
    type_tokens.push_back(consume_token(parser, TokenKind::ENUM, "Expected `enum` keyword."));
    type_tokens.push_back(consume_token(parser, TokenKind::OPEN_CURLY, "Expected `{` after `enum` keyword."));

    while (parser.current_token.info.kind != TokenKind::CLOSE_CURLY) {
      type_tokens.push_back(consume_token(parser, TokenKind::IDENTIFIER, "Expected identifier as enumeration members."));
      if (parser.current_token.info.kind == TokenKind::COMMA) {
        type_tokens.push_back(consume_token(parser, TokenKind::COMMA, "Expected `,` after enum member."));
      }
      else if (parser.current_token.info.kind == TokenKind::OPEN_PAREN) {
        consume_argument_list(parser, type_tokens);
      }

      if (parser.current_token.info.kind == TokenKind::COMMA) {
        type_tokens.push_back(consume_token(parser, TokenKind::COMMA, "Expected `,` after enum member."));
      }
    }

    type_tokens.push_back(consume_token(parser, TokenKind::CLOSE_CURLY, "Expected `}` to end enum declaration."));
    return type_tokens;
  }

  if (parser.current_token.info.kind == TokenKind::STRUCT) {
    type_tokens.push_back(consume_token(parser, TokenKind::STRUCT, "Expected `struct` keyword."));

    if (parser.current_token.info.kind == TokenKind::OPEN_PAREN) {
      consume_argument_list(parser, type_tokens);
    }

    type_tokens.push_back(consume_token(parser, TokenKind::OPEN_CURLY, "Expected `{` after `struct` keyword."));

    while (parser.current_token.info.kind != TokenKind::CLOSE_CURLY) {
      type_tokens.push_back(consume_token(parser, TokenKind::IDENTIFIER, "Expected struct member name."));

      type_tokens.push_back(consume_token(parser, TokenKind::COLON, "Expected `:` seperating struct member name and type."));

      auto struct_member_type_tokens = consume_type(parser);
      for (size_t i = 0; i < struct_member_type_tokens.size(); ++i) type_tokens.push_back(type_tokens[i]);

      type_tokens.push_back(consume_token(parser, TokenKind::SEMI, "Expected `;` to end struct member definition."));
    }

    type_tokens.push_back(consume_token(parser, TokenKind::CLOSE_CURLY, "Expected `}` at the end of struct definition."));
    return type_tokens;
  }

  if (parser.current_token.info.kind == TokenKind::OPEN_SQUARE) {
    type_tokens.push_back(consume_token(parser, TokenKind::OPEN_SQUARE, "Expected `[` to start array type declaration."));

    if (TokenKind::INTEGER_LITERAL == parser.current_token.info.kind) {
      Token array_size_token = consume_token(parser, TokenKind::INTEGER_LITERAL, "Vec size can only be initialised with an integer literal (like `3`). It cannot be a variable.");
      type_tokens.push_back(array_size_token);
    }

    type_tokens.push_back(consume_token(parser, TokenKind::CLOSE_SQUARE, "Expected `]` to end array type declaration."));
  }

  if (parser.current_token.info.kind == TokenKind::POINTER) {
    Token pointer = consume_token(parser, TokenKind::POINTER, "Expected `^` to denote pointer type"); // Past the `^`
    type_tokens.push_back(pointer);
  }

  Token type_name_token = consume_token(parser, TokenKind::IDENTIFIER, "Expected a type name.");
  type_tokens.push_back(type_name_token);

  // Parameterised types may have parameters.. obviously.
  if (parser.current_token.info.kind == TokenKind::OPEN_PAREN) {
    type_tokens.push_back(consume_token(parser, TokenKind::OPEN_PAREN, "Expected `(` to start type parameter list."));

    // @Todo: should we allow empty type params? eg `x: Vector2()`

    if (parser.current_token.info.kind != TokenKind::CLOSE_PAREN) {
      do {
        auto parameter_type_tokens = consume_type(parser);
        for (size_t i = 0; i < parameter_type_tokens.size(); ++i) parameter_type_tokens.push_back(type_tokens[i]);

        if (parser.current_token.info.kind == TokenKind::COMMA)
          type_tokens.push_back(consume_token(parser, TokenKind::COMMA, "Expected comma between type parameter arguments."));
      } while (parser.previous_token.info.kind == TokenKind::COMMA);
    }

    type_tokens.push_back(consume_token(parser, TokenKind::CLOSE_PAREN, "Expected `)` to end type parameter list."));
  }

  return type_tokens;
}

static void parse_top_level_statement(Parser& parser) {
  auto& ast = parser.ast;
  Ref<StatementAstNode> statement = parse_statement(parser);

  if (statement == nullptr)  return;  // Null statement means we have reached the end of the file.

  ast.program_node->top_level_statements.push_back(statement);

  switch (statement->statement_kind) {
  case StatementAstNode::Kind::VARIABLE_DECLARATION:
    ast.global_variables.push_back(std::dynamic_pointer_cast<VariableDeclaration>(statement));
    ast.global_declarations.push_back(statement);
    break;
  case StatementAstNode::Kind::PROCEDURE_DECLARATION:
    ast.proc_decls.push_back(std::dynamic_pointer_cast<ProcedureDeclaration>(statement));
    ast.global_declarations.push_back(statement);
    break;
  case StatementAstNode::Kind::STRUCT_DECLARATION:
    ast.struct_defs.push_back(std::dynamic_pointer_cast<StructDeclaration>(statement));
    ast.global_declarations.push_back(statement);
    break;
  case StatementAstNode::Kind::ENUM_DECLARATION:
    ast.enum_declarations.push_back(std::dynamic_pointer_cast<EnumDeclaration>(statement));
    ast.global_declarations.push_back(statement);
    break;

  case StatementAstNode::Kind::TYPE_ALIAS:
    ast.type_aliases.push_back(std::dynamic_pointer_cast<TypeAliasStatement>(statement));
    ast.global_declarations.push_back(statement);
    break;

  case StatementAstNode::Kind::INCLUDE:
    // @Todo: confirm that parse_statement adds the necessary data to the Ast's include data.
    break;

  case StatementAstNode::Kind::CODE_BLOCK:
  case StatementAstNode::Kind::EXPRESSION:
  case StatementAstNode::Kind::IF:
  case StatementAstNode::Kind::FOR:
  case StatementAstNode::Kind::BREAK:
  case StatementAstNode::Kind::WHILE:
  case StatementAstNode::Kind::RETURN:
  case StatementAstNode::Kind::DEFER:
  case StatementAstNode::Kind::MATCH:
    parser_error_at_token(parser, statement->start_token, "Cannot have {} at the top level of the program.", statement->statement_kind);
    break;

  case StatementAstNode::Kind::INVALID:
  default:
    internal_error("Invalid top level statement"); break;
  }
}

Parse_Result parse_file(Vec<Token>& tokens, File source_file) {
  Parser parser = Parser(tokens, source_file);

  if (parser.tokens.empty()) return { Parse_Code::EMPTY_TOKENS, parser.ast };

  parser_advance(parser);

  auto program = ProgramAstNode::make(parser.current_token);
  parser.ast.program_node = std::dynamic_pointer_cast<ProgramAstNode> (program);

  while (parser.current_token.info.kind != TokenKind::END_OF_FILE) {
    parse_top_level_statement(parser);
  }

  for (auto& [filepath, file_info] : parser.ast.included_file_infos) {
    if (file_info.loaded)  continue;

    File included_file;
    if (!open(included_file, filepath)) {
      log_error("Couldn't open included file `{}`.", filepath);
      return { Parse_Code::COULDNT_OPEN_INCLUDED_FILE, parser.ast };
    }
    else {
      log_info("Including file `{}`", filepath);
    }

    file_info.file = included_file;

    auto include_lex_result = lex_file(included_file);
    if (include_lex_result.code != LexCode::OK) {
      log_error("There was a problem lexing included file `{}`.", included_file.path);
    }

    auto include_parse_result = parse_file(include_lex_result.tokens, included_file);
    if (include_parse_result.code != Parse_Code::OK)  {
      log_error("There was a problem parsing included file `{}`.", included_file.path);
      continue;
    }

    if (!include_parse_result.ast.program_node) continue;

    auto include_program = include_parse_result.ast.program_node;

    for (auto& top : include_program->top_level_statements) {
      assert(is_statement(top));
      auto the_copy = top->copy();
      auto the_statement_copy = std::dynamic_pointer_cast<StatementAstNode> (the_copy);

      parser.ast.program_node->top_level_statements.push_back(the_statement_copy);

      switch (the_statement_copy->statement_kind) {
      case StatementAstNode::Kind::VARIABLE_DECLARATION:
        parser.ast.global_variables.push_back(std::dynamic_pointer_cast<VariableDeclaration>(the_statement_copy));
        parser.ast.global_declarations.push_back(the_statement_copy);
        break;
      case StatementAstNode::Kind::PROCEDURE_DECLARATION:
        parser.ast.proc_decls.push_back(std::dynamic_pointer_cast<ProcedureDeclaration>(the_statement_copy));
        parser.ast.global_declarations.push_back(the_statement_copy);
        break;
      case StatementAstNode::Kind::STRUCT_DECLARATION:
        parser.ast.struct_defs.push_back(std::dynamic_pointer_cast<StructDeclaration>(the_statement_copy));
        parser.ast.global_declarations.push_back(the_statement_copy);
        break;
      case StatementAstNode::Kind::ENUM_DECLARATION:
        parser.ast.enum_declarations.push_back(std::dynamic_pointer_cast<EnumDeclaration>(the_statement_copy));
        parser.ast.global_declarations.push_back(the_statement_copy);
        break;

      case StatementAstNode::Kind::TYPE_ALIAS:
        parser.ast.type_aliases.push_back(std::dynamic_pointer_cast<TypeAliasStatement>(the_statement_copy));
        parser.ast.global_declarations.push_back(the_statement_copy);
        break;

      case StatementAstNode::Kind::CODE_BLOCK:
      case StatementAstNode::Kind::EXPRESSION:
      case StatementAstNode::Kind::IF:
      case StatementAstNode::Kind::FOR:
      case StatementAstNode::Kind::BREAK:
      case StatementAstNode::Kind::WHILE:
      case StatementAstNode::Kind::RETURN:
      case StatementAstNode::Kind::INCLUDE:
      case StatementAstNode::Kind::DEFER:
      case StatementAstNode::Kind::MATCH:
        internal_error("Cannot have {} at the top level of the program.", the_statement_copy->statement_kind);
        break;

      case StatementAstNode::Kind::INVALID:
      default:
        internal_error("Invalid top level statement"); break;
      }
    }

    file_info.loaded = true;
  }

  return { parser.had_error ? Parse_Code::HAD_ERROR : Parse_Code::OK, parser.ast };
}

///
/// Parsing system
///

static void parse_array_index_if_possible(Parser& parser, Ref<ExpressionAstNode>& dest_node) {
  if (parser.current_token.info.kind != TokenKind::OPEN_SQUARE) return;

  Ref<ExpressionAstNode> array_expression = std::dynamic_pointer_cast<ExpressionAstNode> (dest_node->copy());

  Token open_square = consume_token(parser, TokenKind::OPEN_SQUARE, "Expect `[` to start array index expression.");

  auto index_expression = parse_expression(parser);

  Token close_square = consume_token(parser, TokenKind::CLOSE_SQUARE, "Expect `]` to end array index expression.");

  dest_node = ArrayReferenceExpression::make(array_expression->start_token, array_expression, open_square, index_expression, close_square);
}

static void parse_assignment_if_possible(Parser& parser, Ref<ExpressionAstNode>& dest_node, bool can_assign) {
  if (!(parser.current_token.info.flags & TOKEN_FLAG_ASSIGNMENT)) return;

  if (can_assign) {
    internal_error("{} is an invalid assignment target", dest_node->expression_kind);
  }

  if (parser.is_on_rhs_of_dot) return;

  Token op = consume_token(parser, parser.current_token.info.kind, "Expected assignment operator.");

  Ref<AstNode> lhs_node = dest_node->copy();

  log_assert(is_expression(lhs_node), "LHS of assignment is not an expression");
  auto lhs_expr = std::dynamic_pointer_cast<ExpressionAstNode> (lhs_node);

  log_warning("Lhs of assignment is: {}", lhs_expr->expression_kind);

  auto rhs_node = parse_expression(parser);

  dest_node = AssignmentExpression::make(lhs_node->start_token, lhs_expr, op, std::dynamic_pointer_cast<ExpressionAstNode> (rhs_node));
}

static Ref<ExpressionAstNode> parse_expression(Parser& parser) {
  Ref<ExpressionAstNode> expr = std::dynamic_pointer_cast<ExpressionAstNode> (InvalidExpression::make(parser.current_token));

  parse_expression_using_precedence_rule(parser, expr, PREC_ASSIGNMENT);

  if (expr->expression_kind == ExpressionAstNode::Kind::INVALID) {
    internal_error("parse_expression_using_precedence_rule() produced an invalid expression");
  }

  return expr;
}

static void parse_expression_integer(Parser& parser, Ref<ExpressionAstNode>& dest_node, bool can_assign) {
  assert(is_expression(dest_node));

  auto integer_token = parser.previous_token;

  dest_node = IntegerLiteral::make(integer_token);

  auto dest_literal = std::dynamic_pointer_cast<LiteralExpression>(dest_node);
  dest_literal->literal_token = integer_token;

  (void)can_assign; // Can't assign to an integer.
}

static void parse_expression_float(Parser& parser, Ref<ExpressionAstNode>& dest_node, bool can_assign) {
  assert(is_expression(dest_node));

  auto float_token = parser.previous_token;

  dest_node = FloatLiteral::make(float_token);

  auto dest_literal = std::dynamic_pointer_cast<LiteralExpression>(dest_node);
  dest_literal->literal_token = float_token;

  (void)can_assign; // Can't assign to a float.
}

static void parse_expression_literal(Parser& parser, Ref<ExpressionAstNode>& dest_node, bool can_assign) {
  assert(is_expression(dest_node));

  auto the_literal_token = parser.previous_token;

  if (TokenKind::FALSE == the_literal_token.info.kind) {
    dest_node = BooleanLiteral::make(the_literal_token);
  }
  else if (TokenKind::TRUE == the_literal_token.info.kind) {
    dest_node = BooleanLiteral::make(the_literal_token);
  }
  else if (the_literal_token.info.kind == TokenKind::OPEN_SQUARE) {
    auto open_square = the_literal_token;
    Vec<Ref<ExpressionAstNode>> array_elements = {};
    while (parser.current_token.info.kind != TokenKind::CLOSE_SQUARE) {
      auto element_expression = parse_expression(parser);
      array_elements.push_back(std::dynamic_pointer_cast<ExpressionAstNode> (element_expression));

      if (parser.current_token.info.kind == TokenKind::COMMA)
        consume_token(parser, TokenKind::COMMA, "Expected `,` separating array elements");
    }

    auto close_square = consume_token(parser, TokenKind::CLOSE_SQUARE, "Expected `]` at the end of array literal");

    dest_node = ArrayLiteralExpression::make(open_square, array_elements, close_square);
  }
  else {
    internal_error("Unsupported literal token type.");
  }

  (void)can_assign; // Can't asign to a literal
}

static void parse_expression_grouping(Parser& parser, Ref<ExpressionAstNode>& dest_node, bool can_assign) {
  auto open_paren = parser.previous_token;
  log_assert(open_paren.info.kind == TokenKind::OPEN_PAREN, "Grouping expression didn't start with a `(`");

  auto grouped_expression = parse_expression(parser);

  auto close_paren = consume_token(parser, TokenKind::CLOSE_PAREN, "Expected `)` to end grouping.");

  dest_node = GroupingExpression::make(open_paren, grouped_expression, close_paren);

  parse_array_index_if_possible(parser, dest_node);
  parse_assignment_if_possible(parser, dest_node, can_assign);
}

static void parse_expression_unary(Parser& parser, Ref<ExpressionAstNode>& dest_node, bool can_assign) {
  Token op = parser.previous_token;

  if (!(TOKEN_FLAG_UNARY_OPERATOR & op.info.flags)) {
    assert(0 && "Token isn't a unary operator");
  }

  auto rhs_node = parse_expression(parser);

  dest_node = UnaryExpression::make(op, op, std::dynamic_pointer_cast<ExpressionAstNode> (rhs_node));

  parse_array_index_if_possible(parser, dest_node);
  parse_assignment_if_possible(parser, dest_node, can_assign);
}

static void parse_expression_binary(Parser& parser, Ref<ExpressionAstNode>& dest_node, bool can_assign) {
  Token op = parser.previous_token;

  log_assert((TOKEN_FLAG_BINARY_OPERATOR & op.info.flags), "Token isn't a binary operator");

  auto lhs_node = dest_node->copy();

  if (op.info.kind == TokenKind::AS) {
    // @Note: Casts look like normal binary expressions (`x as f32`), but since the right hand side is a
    //        type, it's useful to have a separate AST node kind for it.
    auto unparsed_type = consume_type(parser);

    dest_node = CastExpression::make(lhs_node->start_token, std::dynamic_pointer_cast<ExpressionAstNode> (lhs_node), unparsed_type);

    // @Incomplete: should you be able to assign to a cast? (i.e. `x as f32 = 1.0`)

    return; // Early out if it's a cast.
  }


  // if (op.info.kind == TokenKind::DOT) parser.is_on_rhs_of_dot = true;
  auto rhs_expression = InvalidExpression::make(parser.current_token);
  parse_expression_using_precedence_rule(parser, rhs_expression, PREC_NONE);
  // if (op.info.kind == TokenKind::DOT) parser.is_on_rhs_of_dot = false;

  if (op.info.kind == TokenKind::DOT) {
    if (rhs_expression->expression_kind == ExpressionAstNode::Kind::VARIABLE_REFERENCE) {
      std::dynamic_pointer_cast<VariableReferenceExpression>(rhs_expression)->is_struct_member_reference = true;
    }
    else if (rhs_expression->expression_kind == ExpressionAstNode::Kind::PROCEDURE_CALL) {
      // this is probably a boxed enum member reference (like `BoxedEnum.member(str)`)
    }
    else {
      parser_error_at_token(parser, rhs_expression->start_token, "Can't have {} on right hand side of `.`.", rhs_expression->expression_kind);
    }
  }

  dest_node = BinaryExpression::make(lhs_node->start_token, std::dynamic_pointer_cast<ExpressionAstNode> (lhs_node), op, rhs_expression);

  parse_array_index_if_possible(parser, dest_node);
  parse_assignment_if_possible(parser, dest_node, can_assign);
}

static void parse_expression_string(Parser& parser, Ref<ExpressionAstNode>& dest_node, bool can_assign) {
  Token string_token = parser.previous_token;

  log_assert(string_token.info.kind == TokenKind::STRING_LITERAL, "parse_expression_using_precedence_rule has fucked up");

  dest_node = StringLiteral::make(string_token);
  dest_node->start_token = string_token;

  auto dest = std::dynamic_pointer_cast<LiteralExpression>(dest_node);
  dest->literal_token = string_token;

  (void)can_assign; // Can't assign to string literal
  parse_array_index_if_possible(parser, dest_node);
}

static void parse_expression_proc_call(Parser& parser, Ref<ExpressionAstNode>& dest_node, bool can_assign) {
  log_assert(parser.previous_token.info.kind == TokenKind::OPEN_PAREN, "Proc call arguments didn't start with `(` (got {} instead)", parser.previous_token.info.kind);

  auto open_paren = parser.previous_token;

  auto called_node = dest_node->copy();

  Vec<Ref<ExpressionAstNode>> arguments = {};
  if (parser.current_token.info.kind != TokenKind::CLOSE_PAREN) {
    do {
      Ref<AstNode> arg = parse_expression(parser);
      arguments.push_back(std::dynamic_pointer_cast<ExpressionAstNode> (arg));

      if (parser.current_token.info.kind == TokenKind::COMMA)
        consume_token(parser, TokenKind::COMMA, "Expected comma between procedure arguments.");
    } while (parser.previous_token.info.kind == TokenKind::COMMA);
  }

  auto close_paren = consume_token(parser, TokenKind::CLOSE_PAREN, "Expected `)` to end proc call.");

  dest_node = ProcedureCallExpression::make(called_node->start_token, std::dynamic_pointer_cast<ExpressionAstNode> (called_node), open_paren, arguments, close_paren);

  parse_array_index_if_possible(parser, dest_node);
  parse_assignment_if_possible(parser, dest_node, can_assign);
}

static void parse_expression_var_ref(Parser& parser, Ref<ExpressionAstNode> &dest_node, bool can_assign) {
  Token name = parser.previous_token;

  dest_node = VariableReferenceExpression::make(name);

  parse_array_index_if_possible(parser, dest_node);
  parse_assignment_if_possible(parser, dest_node, can_assign);
}

static void parse_expression_using_precedence_rule(Parser& parser, Ref<ExpressionAstNode>& dest_node, Precedence prec) {
  parser_advance(parser);

  Expr_Parse_Fn prefix_rule = get_parse_rule(parser.previous_token.info.kind).prefix;

  if (prefix_rule == NULL) {
    parser_error_at_token(parser, parser.previous_token, "Can't start an expression with `{}`", parser.previous_token);
    return;
  }

  bool can_assign = prec <= PREC_ASSIGNMENT;

  prefix_rule(parser, dest_node, can_assign);

  while (prec <= get_parse_rule(parser.current_token.info.kind).precedence) {
    parser_advance(parser);

    Expr_Parse_Fn infix_rule = get_parse_rule(parser.previous_token.info.kind).infix;
    log_assert(infix_rule != NULL, "No parse rule has been declared for {}", parser.previous_token.info.kind);

    infix_rule(parser, dest_node, can_assign);
  }

  if (!is_expression(dest_node))
    internal_error("parse_expression_using_precedence_rule() didn't make an expression.");
}

static Ref<CodeBlockStatement> parse_code_block_statement(Parser& parser) {
  Vec<Ref<StatementAstNode>> statement_nodes{ };
  Vec<Ref<StatementAstNode>> deferred_statement_nodes{ };

  ++parser.scope;

  auto open_curly_token = consume_token(parser, TokenKind::OPEN_CURLY, "Expected `{` to start procedure body.");

  while (parser.current_token.info.kind != TokenKind::CLOSE_CURLY) {
    auto statement = parse_statement(parser);
    if (statement->statement_kind == StatementAstNode::Kind::DEFER)
      deferred_statement_nodes.push_back(statement);
    else
      statement_nodes.push_back(statement);
  }

  auto close_curly = consume_token(parser, TokenKind::CLOSE_CURLY, "Expected `}` to end procedure body.");

  --parser.scope;

  return std::dynamic_pointer_cast<CodeBlockStatement> (CodeBlockStatement::make(open_curly_token, statement_nodes, deferred_statement_nodes, close_curly));
}

static Ref<TypeAliasStatement> parse_type_alias_statement(Parser& parser) {
  auto alias_keyword = consume_token(parser, TokenKind::ALIAS, "Expected `alias` at the start of type alias.");

  auto alias_name = consume_token(parser, TokenKind::IDENTIFIER, "Expected type alias name after `type` keyword.");
  consume_token(parser, TokenKind::COLON, "Expected `:` between type alias name and original type");

  auto type_tokens = consume_type(parser);
  consume_token(parser, TokenKind::SEMI, "Expected `;` at the end of type alias");

  Ref<TypeAliasStatement> type_alias = TypeAliasStatement::make(alias_keyword, alias_name, type_tokens);

  parser.ast.type_aliases.push_back(type_alias);

  return type_alias;
}

static Ref<DeferStatement> parse_defer_statement(Parser& parser) {
  consume_token(parser, TokenKind::DEFER, "Expected `defer` keyword at the start of derer statement.");

  auto deferred_node = parse_statement(parser);

  auto defer_statement = DeferStatement::make(parser.current_token, deferred_node);

  return defer_statement;
}

static Maybe<Ref<ForLoop>> parse_for_loop_statement(Parser& parser) {
  // @Incomplete: other for loop kinds
  // `for it: some_array { print("{}", it); }
  auto for_keyword_token = consume_token(parser, TokenKind::FOR, "Expected `for` keyword at the start of for-loop.");
  auto initialisation_statement = parse_statement(parser, true);

  Ref<StatementAstNode> result_node = InvalidStatement::make(for_keyword_token);

  if (parser.previous_token.info.kind != TokenKind::SEMI) {
    // If we don't hit a semi-colon at the end of the first statement after the `for` keyword
    // like in: `for XXXXXX { }`, the statement we just parsed MUST be an expression.
    // Right now, the only thing it can be is a range binary-expression.
    if (initialisation_statement->statement_kind != StatementAstNode::Kind::EXPRESSION) {
      parser_error_at_token(parser, initialisation_statement->start_token, "Range of for loop is not an expression");
      result_node = InvalidStatement::make(for_keyword_token);
    }
    else {
      auto expression_statement = std::dynamic_pointer_cast<ExpressionStatement>(initialisation_statement);

      auto initialisation_expression = std::dynamic_pointer_cast<ExpressionAstNode>(expression_statement->expression_node);

      if (initialisation_expression->expression_kind != ExpressionAstNode::Kind::BINARY) {
        parser_error_at_token(parser, initialisation_statement->start_token, "Expression is not a range, it's a {}.", initialisation_expression->expression_kind);
        return nothing<Ref<ForLoop>>();
        // result_node                        = Invalid_Statement::make(for_keyword_token);
        // initialisation_expression_is_valid = false;
      }
      else if (std::dynamic_pointer_cast<BinaryExpression>(initialisation_expression)->operatorr.info.kind != TokenKind::DOTDOT) {
        parser_error_at_token(parser, initialisation_statement->start_token, "Expression is not a range.");
        return nothing<Ref<ForLoop>>();
        // result_node                        = Invalid_Statement::make(for_keyword_token);
        // initialisation_expression_is_valid = false;
      }

      auto body_node = parse_statement(parser);

      auto ranged_for_statement = RangedForLoop::make(for_keyword_token, expression_statement, body_node);
      return just(std::dynamic_pointer_cast<ForLoop>(ranged_for_statement));
    }
  }
  else {
    // If we DO hit a semi-colon at the end of the first statement after the `for` keyword
    // like in: `for XXXXXX; { }`, the statement we just parsed MUST be a declaration, because it's a c-style for loop.
    // @Todo: maybe we should allow any statement here, so you can do stuff like `i := 0; for ; i<10; ++i { print("{}",
    // i); } print("Still have access to i: {}", i);`
    if (initialisation_statement->statement_kind != StatementAstNode::Kind::VARIABLE_DECLARATION) {
      parser_error_at_token(parser, initialisation_statement->start_token, "Expected variable initialisation at the start of C-style for loop (found a {}).", initialisation_statement->statement_kind);
      return nothing<Ref<ForLoop>>();
    }

    auto condition_node = parse_statement(parser);
    if (condition_node->statement_kind != StatementAstNode::Kind::EXPRESSION) {
      parser_error_at_token(parser, condition_node->start_token, "Expected expession for C-style for loop's condition (found a {}).", condition_node->statement_kind);
      return nothing<Ref<ForLoop>>();
    }

    auto increment_node = parse_expression(parser);
    auto body_node      = parse_statement(parser);

    auto c_style_for_loop = CStyleForLoop::make(
        for_keyword_token, std::dynamic_pointer_cast<VariableDeclaration>(initialisation_statement),
        std::dynamic_pointer_cast<ExpressionStatement>(condition_node),
        std::dynamic_pointer_cast<ExpressionAstNode>(increment_node), body_node);
    return just(std::dynamic_pointer_cast<ForLoop>(c_style_for_loop));
  }

  return nothing<Ref<ForLoop>>();
}


static Ref<VariableDeclaration> parse_variable_declaration(Parser& parser, Token name_token, bool is_external) {
  Vec<Token> type_tokens{ };
  Ref<AstNode> value_node{ nullptr };

  if (parser.current_token.info.kind != TokenKind::EQ)
    type_tokens = consume_type(parser);

  if (type_tokens.size() == 0) {
    consume_token(parser, TokenKind::EQ, "Expected a value since variable was declared without a type.");

    value_node = parse_expression(parser);
  }
  else if (parser.current_token.info.kind == TokenKind::EQ) {
    parser_advance(parser); // Past the `=`

    value_node = parse_expression(parser);
  }

  if (value_node && value_node->node_kind != AstNode::Kind::EXPRESSION) {
    parser_error_at_token(parser, name_token, "The value of variable `{}` is not an expression.", name_token.contents);
  }

  return VariableDeclaration::make(name_token, name_token, type_tokens, std::dynamic_pointer_cast<ExpressionAstNode> (value_node), is_external);
}



static Ref<ProcedureDeclaration> parse_procedure_declaration(Parser& parser, Token name_token, bool is_external) {
  consume_token(parser, TokenKind::OPEN_PAREN, "Expected `(` to begin procedure arguments.");

  Vec<Ref<VariableDeclaration>> arguments = {};
  while (parser.current_token.info.kind != TokenKind::CLOSE_PAREN) {
    auto argument_name = consume_token(parser, TokenKind::IDENTIFIER, "Expected argument name.");
    auto colon = consume_token(parser, TokenKind::COLON, "Expected `:` separating argument name and type.");
    auto argument_declaration = parse_variable_declaration(parser, argument_name, false);
    if (argument_declaration->value != nullptr) {
      parser_error_at_token(parser, argument_declaration->start_token, "Default values for procedure arguments not supported yet.");
    }

    arguments.push_back(argument_declaration);

    if (parser.current_token.info.kind == TokenKind::COMMA) {
      consume_token(parser, TokenKind::COMMA, "Expected comma separating procedure arguments.");
    }
  }

  consume_token(parser, TokenKind::CLOSE_PAREN, "Expected `)` to end procedure arguments.");

  Vec<Token> return_type_tokens = {};

  if (is_external) {
    if (parser.current_token.info.kind != TokenKind::SEMI) {
      return_type_tokens = consume_type(parser);
    }

    consume_token(parser, TokenKind::SEMI, "Expected `;` to end external procedure declaration. External procedures must not have a body.");

    return ProcedureDeclaration::make(name_token, arguments, return_type_tokens, nullptr, true);
  }
  else {
    if (parser.current_token.info.kind != TokenKind::OPEN_CURLY) {
      return_type_tokens = consume_type(parser);
    }

    auto procedure_body_node = parse_code_block_statement(parser);

    return ProcedureDeclaration::make(name_token, arguments, return_type_tokens, procedure_body_node, false);
  }
}



static Ref<IfStatement> parse_if_statement(Parser& parser) {
  auto if_keyword_token = consume_token(parser, TokenKind::IF, "Expected `if` keyword at the start of if-statement.");

  auto condition_node = parse_expression(parser);
  auto body_node = parse_statement(parser);

  if (parser.current_token.info.kind == TokenKind::ELSE) {
    consume_token(parser, TokenKind::ELSE, "Expected `else` to start else-block.");

    auto else_node = parse_statement(parser);

    return IfStatement::make(if_keyword_token, condition_node, body_node, else_node);
  }

  return IfStatement::make(if_keyword_token, condition_node, body_node);
}



static Ref<MatchStatement> parse_match_statement(Parser& parser) {
  auto match_keyword = consume_token(parser, TokenKind::MATCH, "Expected `match` keyword at the start of match statement.");

  auto match_on = parse_expression(parser);
  
  consume_token(parser, TokenKind::OPEN_CURLY, "Expected `{` after match statement expression.");

  Vec<MatchStatement::Case> cases = {};
  Ref<CodeBlockStatement>     else_case = nullptr;

  while (parser.current_token.info.kind == TokenKind::IS) {
    auto the_is_token = consume_token(parser, TokenKind::IS, "Expected `is` at the start of each match statement case.");
    auto case_value = parse_expression(parser);
    
    auto case_then = parse_code_block_statement(parser);

    auto match_case = MatchStatement::Case(case_value, case_then);
    cases.push_back(match_case);
  }

  if (parser.current_token.info.kind == TokenKind::ELSE) {
    auto else_keyword = consume_token(parser, TokenKind::ELSE, "Expected `else` at the start of the default case of match statement.");

    else_case = parse_code_block_statement(parser);
  }
  else {
    parser_error_at_token(parser, parser.current_token, "Match statement missing `else` case.");
  }

  consume_token(parser, TokenKind::CLOSE_CURLY, "Expected `}` to end match statement.");

  return MatchStatement::make(match_keyword, match_on, else_case, cases);
}


static Ref<ReturnStatement> parse_return_statement(Parser& parser) {
  auto return_keyword_token = consume_token(parser, TokenKind::RETURN, "Expected `return` keyword at the start of return statement.");
  auto returned_node = parse_expression(parser);
  consume_token(parser, TokenKind::SEMI, "Expected `;` to end return statement.");

  return ReturnStatement::make(return_keyword_token, returned_node);
}

static Ref<BreakStatement> parse_break_statement(Parser& parser) {
  auto break_keyword_token = consume_token(parser, TokenKind::BREAK, "Expected `break` keyword at the start of break statement.");
  consume_token(parser, TokenKind::SEMI, "Expected `;` to end break statement.");

  return BreakStatement::make(break_keyword_token);
}



static Ref<IncludeStatement> parse_include_statement(Parser& parser) {
  auto include_keyword_token = consume_token(parser, TokenKind::INCLUDE, "Expected `include` keyword at the start of include statement.");
  auto included_node = parse_expression(parser);

  auto include_statement = IncludeStatement::make(include_keyword_token, included_node);

  if (!is_expression(include_statement->included_node))  parser_error_at_token(parser, include_statement->start_token, "You can only include an expression");

  auto the_include_expr = std::dynamic_pointer_cast<ExpressionAstNode> (include_statement->included_node);

  if (the_include_expr->expression_kind != ExpressionAstNode::Kind::LITERAL) {
    parser_error_at_token(parser, include_statement->start_token, "Expected a literal expression (string) after include statement. (Got {})", the_include_expr->expression_kind);
  }
  else {
    auto literal = std::dynamic_pointer_cast<LiteralExpression>(the_include_expr);
    if (literal->literal_kind != LiteralExpression::Kind::STRING) {
      parser_error_at_token(parser, include_statement->start_token, "Expected a string after include statement.");
    }
    else {
      auto include_filepath = std::string(literal->literal_token.contents);
      if (parser.ast.included_file_infos.find(include_filepath) == parser.ast.included_file_infos.end()) {
        // @Correctness: should I ignore double includes?
        parser.ast.included_file_infos[include_filepath].loaded = false;
      }
    }
  }

  consume_token(parser, TokenKind::SEMI, "Expected `;` to end include statement.");

  return include_statement;
}


static Ref<WhileLoop> parse_while_statement(Parser& parser) {
  consume_token(parser, TokenKind::WHILE, "Expected `while` keyword at the start of while-loop.");
  auto condition_node = parse_expression(parser);
  auto then_node = parse_statement(parser);

  return WhileLoop::make(parser.current_token, condition_node, then_node);
}




static Ref<ExpressionStatement> parse_expression_statement(Parser& parser, bool ignore_semicolon_at_the_end_of_expr_statement) {
  auto expression_node = parse_expression(parser);

  if (!ignore_semicolon_at_the_end_of_expr_statement)  consume_token(parser, TokenKind::SEMI, "Expected `;` at the end of expression statement.");

  return ExpressionStatement::make(expression_node->start_token, std::dynamic_pointer_cast<ExpressionAstNode> (expression_node));
}

static Ref<StatementAstNode> parse_statement(Parser& parser, bool ignore_semicolon_at_the_end_of_expr_statement) {
  Ref<StatementAstNode> result_node = InvalidStatement::make(parser.current_token);

  switch (parser.current_token.info.kind) {
  case TokenKind::ALIAS:      { result_node = parse_type_alias_statement(parser); } break;
  case TokenKind::BREAK:      { result_node = parse_break_statement(parser);      } break;
  case TokenKind::DEFER:      { result_node = parse_defer_statement(parser);      } break;
  case TokenKind::IF:         { result_node = parse_if_statement(parser);         } break;
  case TokenKind::INCLUDE:    { result_node = parse_include_statement(parser);    } break;
  case TokenKind::MATCH:      { result_node = parse_match_statement(parser);      } break;
  case TokenKind::RETURN:     { result_node = parse_return_statement(parser);     } break;
  case TokenKind::WHILE:      { result_node = parse_while_statement(parser);      } break;
  case TokenKind::OPEN_CURLY: { result_node = parse_code_block_statement(parser); } break;

  case TokenKind::FOR: {
    auto maybe_for_statement = parse_for_loop_statement(parser);
    if (maybe_for_statement.has_value) {
      result_node = maybe_for_statement.value;
    }
    else {
      // @Todo: do I need to report failure here instead?
    }
  } break;

  case TokenKind::IDENTIFIER: {
    if (parser.next_token.info.kind != TokenKind::COLON) {
      result_node = parse_expression_statement(parser, ignore_semicolon_at_the_end_of_expr_statement);
    }
    else {
      Token name_token = consume_token(parser, TokenKind::IDENTIFIER, "Expected identifier.");

      consume_token(parser, TokenKind::COLON, "Expected `:` after name in declaration.");

      bool is_external = parser.current_token.info.kind == TokenKind::EXTERNAL;
      if (is_external)  consume_token(parser, TokenKind::EXTERNAL, "Expected `external` keyword for external declaration.");

      if (parser.current_token.info.kind == TokenKind::OPEN_PAREN) {
        // name : (
        //        ^
        //        If we see a ( at the start of the type declaration, we must be parsing a procedure declaration.
        result_node = parse_procedure_declaration(parser, name_token, is_external);
        // parser.ast.proc_decls.push_back(node);
      }
      else if (parser.current_token.info.kind == TokenKind::STRUCT) {
        // name : struct
        //        ^
        //        This is pretty obviously a struct declaration.

        auto struct_keyword = consume_token(parser, TokenKind::STRUCT, "Expected `struct` keyword at the start of struct declaration.");
        auto open_curly = consume_token(parser, TokenKind::OPEN_CURLY, "Expected `{` to start struct member declarations.");

        Vec<Ref<VariableDeclaration>> members = {};
        while (parser.current_token.info.kind != TokenKind::CLOSE_CURLY) {
          auto member_name = consume_token(parser, TokenKind::IDENTIFIER, "Expected struct member name.");
          auto colon = consume_token(parser, TokenKind::COLON, "Expected `:` separating member name and type.");
          auto variable_declaration = parse_variable_declaration(parser, member_name, false); // @Todo: do I need to pass is_external here?
          auto semi = consume_token(parser, TokenKind::SEMI, "Expected `;` to end member declaration.");
          members.push_back(variable_declaration);
        }

        auto close_curly = consume_token(parser, TokenKind::CLOSE_CURLY, "Expected `}` to start struct member declarations.");

        result_node = StructDeclaration::make(name_token, name_token, members, is_external);

        // parser.ast.struct_defs.push_back(node);
      }
      else if (parser.current_token.info.kind == TokenKind::ENUM) {
        // name : enum
        //        ^
        //        This is pretty obviously an enum declaration.

        auto type_tokens = consume_type(parser);

        result_node = EnumDeclaration::make(name_token, name_token, type_tokens, is_external);

        // parser.ast.struct_defs.push_back(node);
      }
      else { // If it's not a struct, procedure or enum declaration, it must be a variable.
        result_node = parse_variable_declaration(parser, name_token, is_external);
        consume_token(parser, TokenKind::SEMI, "Expected `;` at the end of variable declaration.");
      }
    }
  } break;

  case TokenKind::STRING_LITERAL:
  case TokenKind::INTEGER_LITERAL:
  case TokenKind::FLOAT_LITERAL:
  case TokenKind::OPEN_PAREN:
  case TokenKind::OPEN_SQUARE:
  case TokenKind::AS:
  case TokenKind::EQ:
  case TokenKind::COMMA:
  case TokenKind::SEMI:
  case TokenKind::COLON:
  case TokenKind::EQEQ:
  case TokenKind::NOTEQ:
  case TokenKind::LT:
  case TokenKind::GT:
  case TokenKind::LE:
  case TokenKind::GE:
  case TokenKind::ADD:
  case TokenKind::SUB:
  case TokenKind::MUL:
  case TokenKind::DIV:
  case TokenKind::MOD:
  case TokenKind::DOT:
  case TokenKind::DOTDOT:
  case TokenKind::AND:
  case TokenKind::ANDAND:
  case TokenKind::OR:
  case TokenKind::OROR:
  case TokenKind::ADD_EQ:
  case TokenKind::SUB_EQ:
  case TokenKind::MUL_EQ:
  case TokenKind::DIV_EQ:
  case TokenKind::MOD_EQ:
  case TokenKind::AND_EQ:
  case TokenKind::OR_EQ:
  case TokenKind::BANG:
  case TokenKind::POINTER:
  case TokenKind::DEREFERENCE:
  case TokenKind::TRUE:
  case TokenKind::FALSE: {
    result_node = parse_expression_statement(parser, ignore_semicolon_at_the_end_of_expr_statement);
  } break;

  case TokenKind::END_OF_FILE:
  case TokenKind::CLOSE_PAREN:
  case TokenKind::CLOSE_CURLY:
  case TokenKind::CLOSE_SQUARE:
  case TokenKind::ELSE:
  case TokenKind::ENUM:
  case TokenKind::STRUCT:
  case TokenKind::IS:
  case TokenKind::EXTERNAL:
  case TokenKind::INVALID: 
  default: {
    internal_error("Could not parse statement starting with {}", parser.current_token);
  } break;
  }

  if (parser.had_error) {
    synchronise(parser);
  }

  return result_node;
}
