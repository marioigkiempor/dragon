
///
/// @Note: The parsing architecture is based on the Crafting Interpreters book
/// by Rober Nystrom. Found at: http://craftinginterpreters.com/
///

#ifndef DRAGON_PARSER_H_
#define DRAGON_PARSER_H_

#include <assert.h>
#include <map>
#include <stdint.h>

#include "defines.h"

#include "ast.h"
#include "lexer.h"
#include "typechecker.h"
#include "types.h"

struct Parser {
  Vec<Token> tokens      = {};
  usize      token_index = 0;

  Token current_token  = {};
  Token previous_token = {};
  Token next_token     = {};

  s32 scope = 0;

  bool is_on_rhs_of_dot = false;

  bool had_error  = false;
  bool panic_mode = false;

  Ast ast = {};

  Parser(Vec<Token> tokens, File& file)
  : tokens(tokens) {
    if (!tokens.empty()) {
      current_token = tokens[0];
      previous_token = tokens[0];
      next_token = tokens[0];
    }

    ast.included_file_infos.insert({ file.path.string(), { true, file } });
  }
};

enum class Parse_Code {
  OK = 0,
  EMPTY_TOKENS,
  HAD_ERROR,
  COULDNT_OPEN_INCLUDED_FILE,
};

struct Parse_Result {
  Parse_Code code = Parse_Code::HAD_ERROR;
  Ast ast = {};
};

Parse_Result parse_file(Vec<Token> &tokens, File file);

#endif // DRAGON_PARSER_H_
