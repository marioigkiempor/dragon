
#include "system_helpers.h"

#include <cstdio>
#include <stdexcept>
#include <array>
#include <fstream>
#include <sstream>

#include "logging.h"


bool open(File& file, std::filesystem::path path) {
  std::ifstream file_stream(path);
  if (!file_stream.is_open()) {
    return false;
  }

  std::stringstream buffer;
  buffer << file_stream.rdbuf();

  auto str = buffer.str();
  auto str_len = str.size() + 1;
  char *data = new char[str_len]; // @Leak
#define _CRT_SECURE_NO_WARNINGS
  strncpy(data, str.c_str(), str.size());
  data[str.size()] = '\0';

  file.path = path;
  file.data = data;
  file.size = str.size();

  return true;
}

bool delete_file(Path path) {
  try {
    std::filesystem::remove(path);
    return true;
  }
  catch (const std::filesystem::filesystem_error& error) {
    // @Todo: better error handling.
    // internal_error("Couldn't delete file.");
    return false;
  }
}




// Note: returns the exit code of the process.
s32 run_process_and_wait(const std::string& command) {
    log_command("{}", command);

    auto command_cstr = command.c_str();

    return system(command_cstr);
}







bool is_space(char c) { return is_newline(c) || isspace(c); }
bool is_newline(char c) { return (c == '\r') || (c == '\n'); }
