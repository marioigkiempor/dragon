
#pragma once

#ifndef SYSTEM_HELPERS_H_
#define SYSTEM_HELPERS_H_


#include <filesystem>

#include "defines.h"

struct File {
	Path path = "invalid-path";
	char* data = nullptr;
	u64 size = 0;
};

bool open(File& file, Path path);

bool delete_file(Path path);


struct FileLoc {
  usize byte_offset = 0;
	Path path = "invalid-path";
	u64  row  = 0;
	u64  col  = 0;

  // FileLoc(const FileLoc& other)
  // : path(other.path)
  // , row(other.row)
  // , col(other.col) {
  // }

	FileLoc(usize byte_offset, Path path, u64 row, u64 col) {
    this->byte_offset = byte_offset;
		this->path = path;
		this->row  = row;
		this->col  = col;
	}

  FileLoc() = default;

	bool operator==(const FileLoc& other) const {
		return this->path == other.path && this->row == other.row && this->col == other.col;
	}

	bool operator!=(const FileLoc& other) const { return !(*this == other); }
};

#include "fmt/core.h"
#include "fmt/format.h"

template <>
struct fmt::formatter<FileLoc> {
	constexpr auto parse(fmt::format_parse_context& ctx) { return ctx.end(); }

	template <typename FormatContext>
	auto format(const FileLoc& file_loc, FormatContext& ctx) {
		if (file_loc.path.empty()) return fmt::format_to(ctx.out(), "<invalid file>:0:0");

		return fmt::format_to(ctx.out(), "{}:{}:{}", file_loc.path, file_loc.row+1, file_loc.col+1);
	}
};



// Note: returns the exit code of the process.
s32 run_process_and_wait(const std::string& command);



bool is_space(char c);
bool is_newline(char c);




// fmt formatter for C++ standard library types
template <>
struct fmt::formatter<std::filesystem::path> {
	constexpr auto parse(fmt::format_parse_context& ctx) { return ctx.end(); }

	template <typename FormatContext>
	auto format(const std::filesystem::path& path, FormatContext& ctx) {
		return fmt::format_to(ctx.out(), "{}", path.string());
	}
};

#endif // SYSTEM_HELPERS_H_
