
#include "typechecker.h"

#include <iostream>
#include <memory>
#include <queue>

#include "defines.h"

#include "builtins.h"
#include "logging.h"
#include "ast.h"
#include "lexer.h"
#include "types.h"

static Ref<Type> typecheck_expression(Typer& typer, Ref<ExpressionAstNode>& node);

template <typename S, typename... Args>
static void typer_error_at(Typer& typer, Ref<AstNode> node, const S& fmt, Args &&...args) {
  if (!typer.had_error)
    typer.had_error = true;

  if (node->had_type_error)  return; // Each node will only display 1 error, to reduce spam.

  node->had_type_error = true;

  fmt::print("{}\n", node->start_token.file_loc);

  const auto error_filepath = node->start_token.file_loc.path.string();

  const auto [source_has_been_loaded, source] = typer.ast->included_file_infos.at(error_filepath);
  log_assert(source_has_been_loaded, "Cannot report parse error in file which has not been loaded (filepath `{}`)", error_filepath);

  dragon_annotated_source(node, source, fmt, args...);
}

static void typer_error_redeclaration(Typer& typer, Ref<AstNode> stmt, Typer::Binding binding) {
  if (binding.kind == Typer::Binding::Kind::USER_DEFINED_TYPE) {
    typer_error_at(typer, stmt, "Redeclaration of `{}`. It refers to a type.", binding.name);
  }
  else {
    typer_error_at(typer, stmt, "Redeclaration of `{}`. It's a variable (type `{}`)", binding.name, binding.type);
    log_info("{}: Declared here.", binding.name.file_loc);
  }
}

static bool type_from_name(Typer& typer, StringView name, Ref<Type> &type) {
  for (auto [type_name, found_type] : global_builtins.type_names) {
    if (name == type_name) {
      type = found_type->copy();
      return true;
    }
  }

  for (auto [type_name, found_type] : typer.user_defined_types) {
    if (name == type_name) {
      type = found_type->copy();
      return true;
    }
  }

  for (auto &[binding_name, binding] : typer.environment.top().bindings) {
    if (binding_name == name) {
      switch (binding.kind) {
      case Typer::Binding::Kind::USER_DEFINED_TYPE: {
        type = binding.type->copy();
        return true;
      } break;
      case Typer::Binding::Kind::TYPE_ALIAS: {
        type = binding.type->copy();
        return true;
      } break;
      case Typer::Binding::Kind::VARIABLE: {
        return false;
      } break;
      default: internal_error("Invalid Binding::Kind: {}", (int)binding.kind);
      }
    }
  }

  return false;
}

static bool get_bound_name(Typer& typer, StringView needle, OUT Typer::Binding *result) {
  if (typer.environment.empty()) {
    return false;
  }

  for (auto &[name, binding] : typer.environment.top().bindings) {
    if (name == needle) {
      *result = binding;
      return true;
    }
  }

  return false;
}

static bool get_expr_binding_name_if_possible(Ref<AstNode> expr_node, OUT Token* token) {
  assert(is_expression(expr_node));
  auto expr = std::dynamic_pointer_cast<ExpressionAstNode>(expr_node);

  switch(expr->expression_kind) {
    case ExpressionAstNode::Kind::LITERAL: return false;

    case ExpressionAstNode::Kind::GROUPING: {
      auto expr_as_grouping = std::dynamic_pointer_cast<GroupingExpression>(expr);
      return get_expr_binding_name_if_possible(expr_as_grouping->grouped_expression, token);
    } break;

    case ExpressionAstNode::Kind::BINARY: {
      auto expr_as_binary = std::dynamic_pointer_cast<BinaryExpression>(expr);
      return get_expr_binding_name_if_possible(expr_as_binary->lhs_expression, token);
    } break;

    case ExpressionAstNode::Kind::UNARY: {
      auto expr_as_unary = std::dynamic_pointer_cast<UnaryExpression>(expr);
      return get_expr_binding_name_if_possible(expr_as_unary->operand_node, token);
    } break;

    case ExpressionAstNode::Kind::VARIABLE_REFERENCE: {
      auto expr_as_var_ref = std::dynamic_pointer_cast<VariableReferenceExpression>(expr);

      if (expr_as_var_ref->is_struct_member_reference)  return false;

      *token = expr_as_var_ref->name;
      return true;
    } break;

    case ExpressionAstNode::Kind::ARRAY_REFERENCE: {
      auto expr_as_array_ref = std::dynamic_pointer_cast<ArrayReferenceExpression>(expr);
      return get_expr_binding_name_if_possible(expr_as_array_ref->array_expression, token);
    } break;

    case ExpressionAstNode::Kind::ARRAY_LITERAL: {
      return false;
    } break;

    case ExpressionAstNode::Kind::PROCEDURE_CALL: {
      auto expr_as_proc_call = std::dynamic_pointer_cast<ProcedureCallExpression>(expr);
      return get_expr_binding_name_if_possible(expr_as_proc_call->called_node, token);
    } break;

    case ExpressionAstNode::Kind::ASSIGNMENT: {
      auto expr_as_assignment = std::dynamic_pointer_cast<AssignmentExpression>(expr);
      return get_expr_binding_name_if_possible(expr_as_assignment->lhs_node, token);
    } break;

    case ExpressionAstNode::Kind::CAST: {
      auto cast = std::dynamic_pointer_cast<CastExpression> (expr);
      return get_expr_binding_name_if_possible(cast->casted_node, token);
    } break;

    case ExpressionAstNode::Kind::INVALID:
    default:
      internal_error("Couldn't get the binding name for invalid expression kind: {}", (int)expr->expression_kind);
  }

  internal_error("Couldn't get the binding name for invalid expression kind: {}", (int)expr->expression_kind);
  return false;
}

//
// typer_consume_type() is a mini-parser, which takes an Vec<Token> representing the text of a given
// "type declaration" (the signature of a procedure, the type of a variable, the type of a [generic] struct member, etc).
// If there is an error, it will try to give useful information in the form of a Type_Parsing_Error.
// It is the responsibility of the caller to use this enumeration to provide a useful error message to the user (if need be).
// The caller may not have to report errors; in some cases, it is expected that the types parsing process _will_ fail,
// like in the case of parsing top-level declarations, which may appear out of order and the Typer may be waiting for another
// declaration's type to be resolved. This is why this function doesn't do any user-facing error reporting.
//
// @Todo: report where in the Vec<Token> the error occured.
//

enum Type_Parsing_Error {
    OK = 0,
    TYPE_WAS_EMPTY,

    UNRECOGNISED_TYPE_NAME,

    UNRECOGNISED_PROCEDURE_ARGUMENT_TYPE,
    UNRECOGNISED_PROCEDURE_RETURN_TYPE,

    UNRECOGNISED_STRUCT_PARAM_TYPE,
    UNRECOGNISED_STRUCT_MEMBER_TYPE,
    UNRECOGNISED_STRUCT_INSTANCE_ARGUMENT_TYPE,
    NOT_ENOUGH_STRUCT_PARAMETERS,
    STRUCT_EXPECTED_PARAMETERS_BUT_NONE_GIVEN,
    BASIC_STRUCT_GIVEN_PARAMETERS,

    BOXED_ENUM_VALUE_TYPE_NOT_VALID,
};

struct Type_Parsing_Result {
  Type_Parsing_Error error;
  Ref<Type> type;
};

static Type_Parsing_Result consume_type(Typer& typer, Token declaration_name, Vec<Token>& type_tokens, OUT size_t& token_index) {
  if (type_tokens.empty() || token_index >= type_tokens.size()) {
    // @Note: this is needed because when we parse the "void" is optional at the
    // end of proc signatures
    auto result = std::make_shared<BuiltinType>(BuiltinType::Kind::V0ID);
    return { TYPE_WAS_EMPTY, result };
  }

  bool is_pointer = false;
  if (type_tokens[token_index].info.kind == TokenKind::POINTER) {
    ++token_index; // Pointer
    is_pointer = true;
  }

  if (type_tokens[token_index].info.kind == TokenKind::ENUM) {
    auto enum_keyword = type_tokens[token_index++];
    auto open_curly   = type_tokens[token_index++];

    Vec<Ref<EnumType::Member>> members = {};
    bool is_boxed_enum = false;

    while (type_tokens[token_index].info.kind != TokenKind::CLOSE_CURLY) {
      auto member_name = type_tokens[token_index++];

      Vec<BoxedEnumType::Member::BoxedValue> member_values = {};
      if (type_tokens[token_index].info.kind == TokenKind::OPEN_PAREN) {
        token_index++; // Open paren

        do {
          auto name = type_tokens[token_index++];
          ++token_index; // Colon

          auto arg_type_parsing_result = consume_type(typer, declaration_name, type_tokens, token_index);
          if (arg_type_parsing_result.error != OK || arg_type_parsing_result.type->is_unresolved()) {
            return {BOXED_ENUM_VALUE_TYPE_NOT_VALID, UnresolvedType::make()};
          }

          BoxedEnumType::Member::BoxedValue value = { name, arg_type_parsing_result.type };
          member_values.push_back(value);

          if (type_tokens[token_index].info.kind == TokenKind::COMMA)  ++token_index; // Comma
        } while (type_tokens[token_index].info.kind != TokenKind::CLOSE_PAREN);

        token_index++; // Close paren
      }

      if (!member_values.empty()) {
        auto boxed_member = std::make_shared<BoxedEnumType::Member>(member_name, member_values);
        members.push_back(boxed_member);
        is_boxed_enum = true;
      }
      else {
        auto simple_member = std::make_shared<EnumType::Member>(member_name);
        members.push_back(simple_member);
      }
      
      if (type_tokens[token_index].info.kind == TokenKind::COMMA) {
        token_index++; // Comma
      }
    }

    [[maybe_unused]] auto close_curly   = type_tokens[token_index++];

    if (is_boxed_enum) {
      return { OK, BoxedEnumType::make(declaration_name, members) };
    }

    return { OK, SimpleEnumType::make(declaration_name, members) };
  }


  bool is_array = false;
  bool is_vector = false;
  size_t array_size = 0;

  if (type_tokens[token_index].info.kind == TokenKind::OPEN_SQUARE) {
    is_array = true;

    ++token_index; // [

    if (type_tokens[token_index].info.kind == TokenKind::INTEGER_LITERAL) {
      Token array_size_token = type_tokens[token_index];
      array_size = string_view_to_u64(array_size_token.contents);

      ++token_index; // Int literal
    } else {
      is_vector = true;
    }

    ++token_index; // ]
  }

  auto type_name_token = type_tokens[token_index++];
  auto type_name = type_name_token.contents;

  Ref<Type> type = nullptr;
  if (type_name.size() == 0) {
    type = BuiltinType::make(BuiltinType::Kind::V0ID);
  }
  else if (!type_from_name(typer, type_name, type)) {
    return { UNRECOGNISED_TYPE_NAME, UnresolvedType::make() };
  }

  // If the struct expects parameters, make sure you get them
  // @Todo: eventually, we may be able to infer the type.
  if (type->is_struct()) {
    auto struct_type = std::dynamic_pointer_cast<StructType>(type->copy());
    assert(struct_type);

    switch(struct_type->struct_kind) {
    case StructType::Kind::PARAMETERISED: {
      auto parameterised_struct_type = std::dynamic_pointer_cast<ParameterisedStructType> (type);

      if (type_tokens[token_index].info.kind != TokenKind::OPEN_PAREN) {
        return { STRUCT_EXPECTED_PARAMETERS_BUT_NONE_GIVEN, UnresolvedType::make() };
      }

      Vec<StructInstanceType::Argument> instance_arguments{};

      if (token_index < type_tokens.size() && type_tokens[token_index].info.kind == TokenKind::OPEN_PAREN) {
        ++token_index; // Open paren

        if (type_tokens[token_index].info.kind != TokenKind::CLOSE_PAREN) {
          do {
            auto arg_type_parse_result = consume_type(typer, declaration_name, type_tokens, token_index);
            if (arg_type_parse_result.error != OK || arg_type_parse_result.type->is_unresolved()) {
              return { UNRECOGNISED_STRUCT_INSTANCE_ARGUMENT_TYPE, UnresolvedType::make() };
            }

            instance_arguments.push_back(StructInstanceType::Argument{ arg_type_parse_result.type });

            if (type_tokens[token_index].info.kind == TokenKind::COMMA)  ++token_index; // Comma
          } while (type_tokens[token_index].info.kind != TokenKind::CLOSE_PAREN);
        }


        ++token_index; // Close paren

        // Check the struct parameters.
        if (instance_arguments.size() != parameterised_struct_type->expected_parameters.size()) {
          return { NOT_ENOUGH_STRUCT_PARAMETERS, UnresolvedType::make() };
        }

        type = StructInstanceType::make(struct_type->name_token, parameterised_struct_type, struct_type->members, instance_arguments);
        // for (size_t i = 0; i < instance_arguments.size(); ++i) {
        //   struct_type->given_parameter_types.push_back(instance_arguments[i]);
        // }
      }

    } break;

    case StructType::Kind::BASIC: {
      // if (type_tokens[token_index].info.kind == TokenKind::OPEN_PAREN) {
      //   return {BASIC_STRUCT_GIVEN_PARAMETERS, Unresolved_Type::make()};
      // }
    } break;

    case StructType::Kind::INSTANCE: {

    } break;

    default: internal_error("Cannot parse the type of struct params for invalid struct type {}", (int)struct_type->struct_kind);
    }
  }

  if (is_array) {
    return { OK, ArrayType::make(array_size, type, is_vector) };
  }

  if (is_pointer) {
    return { OK, PointerType::make(type) };
  }

  return {OK, type};
}

static bool is_cast_possible(Typer& typer, Ref<Type> from, Ref<Type> to) {
  (void)typer;
  // @Todo: will need to take in the from and to Ast_Nodes to check the context
  // of the value
  //     eg `fopen("hello", "w")` is fine but
  //        `var s:str = "hello"; fopen(s, "w");` isn't fine.
  if (*from == *to)
    return true;


  // From string -> ^s8
  if (is_string(from) && to->is_pointer()) {
    auto to_pointer = std::dynamic_pointer_cast<PointerType>(to);
    if (to_pointer->pointed->is_builtin()) {
      auto pointed = std::dynamic_pointer_cast<BuiltinType>(to_pointer->pointed);

      if (pointed->builtin_kind == BuiltinType::Kind::S8) {
        return true;
      }
    }
  }

  // @Incomplete!!!
  if (is_integral(from) && is_integral(to)) return true;

  if (is_integral(from) && is_floating(to)) return true;

  if (is_floating(from) && is_floating(to)) return true;

  if (is_floating(from) && is_integral(to)) return true;

  if (from->is_pointer() && is_boolean(to))   return true;

  if ((is_boolean(from) && is_integral(to)) || (is_integral(from) && is_boolean(to))) return true;

  if (from->is_pointer() && to->is_pointer()) {
    auto from_pointer = std::dynamic_pointer_cast<PointerType>(from);
    auto to_pointer = std::dynamic_pointer_cast<PointerType>(to);
    if (from_pointer->pointed && *from_pointer->pointed == BuiltinType(BuiltinType::Kind::V0ID)) return true;
    if (to_pointer->pointed && *to_pointer->pointed == BuiltinType(BuiltinType::Kind::V0ID)) return true;

    if (from_pointer->pointed && to_pointer->pointed) {
      log_warning("Casting pointer to poiner {} -> {}", from_pointer->pointed, to_pointer->pointed);
      if (*from_pointer->pointed == *to_pointer->pointed) {
        return true;
      }
    }
  }

  if (from->is_struct() && to->is_struct()) {
    auto can_cast_structs = *from == *to;
    log_info("Can cast structs: {}", can_cast_structs);
    return can_cast_structs;
  }

  return false;
}

static bool is_assignment_possible(Typer& typer, Ref<Type> from, Ref<Type> to) {
  if (*to == *from)  return true;

  // @Incomplete!!!
  if (is_integral(to) && is_integral(from))  return true;
  if (is_integral(to) && is_floating(from))  return true;

  if (to->is_pointer() && is_integral(from)) return true;

  if (is_cast_possible(typer, from, to))     return true;

  return false;
}

static bool set_binding(Typer& typer, Token name, Typer::Binding::Kind kind, Ref<Type> type) {
  Typer::Binding existing_binding = {};
  if (get_bound_name(typer, name.contents, &existing_binding)) {
    return false;
  }

  typer.environment.top().bindings[name.contents].name = name;
  typer.environment.top().bindings[name.contents].kind = kind;
  typer.environment.top().bindings[name.contents].type = type;

  return true;
}

static bool clear_binding(Typer& typer, StringView name) {
  Typer::Binding existing_binding = {};
  if (get_bound_name(typer, name, &existing_binding)) {
    typer.environment.top().bindings.erase(name);
    return true;
  }

  return false;
}

static void typecheck_statement(Typer& , Ref<StatementAstNode>);

static void typecheck_code_block_statement(Typer& typer, Ref<CodeBlockStatement> code_block) {
  // @Todo: check all paths return the same type of value

  if (code_block->type_checked)  return;

  assert(is_statement(code_block));

  auto type_scope = typer.environment.top();
  type_scope.node = code_block;
  typer.environment.push(type_scope);

  for (size_t i = 0; i < code_block->statement_nodes.size(); ++i) {
    typecheck_statement(typer, code_block->statement_nodes[i]);
  }

  typer.environment.pop();

  code_block->type_checked = true;
}

static void typecheck_expression_statement(Typer& typer, Ref <ExpressionStatement> expression_statement) {;
  typecheck_expression(typer, expression_statement->expression_node);
}

static void typecheck_defer_statement(Typer& typer, Ref<DeferStatement> defer_statement) {
  typecheck_statement(typer, defer_statement->deferred_node);
}


static void typecheck_if_statement(Typer& typer, Ref<IfStatement> if_stmt) {
  auto cond_type = typecheck_expression(typer, if_stmt->condition_node);
  auto bool_type = std::make_shared<BuiltinType>(BuiltinType::Kind::BOOLEAN);
  if (!is_cast_possible(typer, cond_type, bool_type)) {
    typer_error_at(typer, if_stmt->condition_node, "If statement condition should be of type `bool`. It is currently `{}`", cond_type);
  }

  typecheck_statement(typer, if_stmt->body_node);

  if (if_stmt->else_node)  typecheck_statement(typer, if_stmt->else_node);

  // @Todo: check all paths return the same type of value
}

static void typecheck_for_statement(Typer& typer, Ref<ForLoop> for_stmt) {
  switch (for_stmt->for_statement_kind) {
  case ForLoop::Kind::RANGED: {
    auto ranged_for_loop = std::dynamic_pointer_cast<RangedForLoop> (for_stmt);
    auto range_expr_stmt = std::dynamic_pointer_cast<ExpressionStatement>(ranged_for_loop->range_expression_statement);
    typecheck_statement(typer, range_expr_stmt);

    // @Todo: Pretty sure I check this at parse-time.
    if (!is_expression(range_expr_stmt->expression_node)) {
      typer_error_at(typer, range_expr_stmt, "Ranged for loop condition is not an expression.");
    }

    auto range_expr = std::dynamic_pointer_cast<ExpressionAstNode>(range_expr_stmt->expression_node);

    // @Todo: Pretty sure I check this at parse-time.
    if (range_expr->expression_kind != ExpressionAstNode::Kind::BINARY) {
      typer_error_at(typer, range_expr_stmt, "Ranged for loop condition is not a binary expression.");
    }

    auto range_binary_expr = std::dynamic_pointer_cast<BinaryExpression>(range_expr);

    // @Todo: Pretty sure I check this at parse-time.
    if (range_binary_expr->operatorr.info.kind != TokenKind::DOTDOT) {
      typer_error_at(typer, range_expr_stmt, "Ranged for loop condition is not a range expression (it should look like `<x> .. <y>`)");
    }

    if (!is_integral(range_expr->type)) {
      typer_error_at(typer, range_expr_stmt, "Ranged for loops must be used over integral range.");
    }

    // @Incomplete: "it" may shadow another "it" variable. If it does shadow, clearing its binding will remove the original "it" binding as well.
    if (!set_binding(typer, make_builtin_token("it"), Typer::Binding::Kind::VARIABLE, range_expr->type)) {
      internal_error("Shadowing variables not implemented in typechecker.");
    }

    typecheck_statement(typer, for_stmt->body_node);

    clear_binding(typer, "it");
  } break;

  case ForLoop::Kind::C_STYLE: {
    auto c_style_for_loop = std::dynamic_pointer_cast<CStyleForLoop> (for_stmt);
    auto init_node = c_style_for_loop->initialisation_node;
    assert(is_statement(init_node));

    auto init_stmt = std::dynamic_pointer_cast<StatementAstNode>(init_node);

    typecheck_statement(typer, init_node);

    assert(init_stmt->statement_kind == StatementAstNode::Kind::VARIABLE_DECLARATION);
    auto declaration = std::dynamic_pointer_cast<VariableDeclaration>(init_stmt);

    if (!is_integral(declaration->type) || declaration->type->is_pointer()) {
      typer_error_at(typer, init_node, "Can only loop over integral types or pointers. You declared a {}", declaration->type);
    }

    auto condition_node = c_style_for_loop->condition_node;
    assert(is_statement(condition_node));
    auto condition_stmt = std::dynamic_pointer_cast<StatementAstNode>(condition_node);

    typecheck_statement(typer, condition_node);

    log_assert(is_statement(condition_node) && condition_stmt->statement_kind == StatementAstNode::Kind::EXPRESSION, "Condition must be an expression");

    auto condition_expr_stmt = std::dynamic_pointer_cast<ExpressionStatement>(condition_stmt);
    assert(is_expression(condition_expr_stmt->expression_node));
    auto condition_expr = std::dynamic_pointer_cast<ExpressionAstNode> (condition_expr_stmt->expression_node);

    auto bool_type = std::make_shared<BuiltinType>(BuiltinType::Kind::BOOLEAN);
    if (!is_cast_possible(typer, condition_expr->type, bool_type)) {
      typer_error_at(typer, condition_node, "For statement condition is not converible to boolean.");
    }

    log_assert(c_style_for_loop->increment_node->node_kind == AstNode::Kind::EXPRESSION, "C-style for loop increment node should be an expression");
    typecheck_expression(typer, c_style_for_loop->increment_node);

    typecheck_statement(typer, c_style_for_loop->body_node);

    clear_binding(typer, declaration->name_token.contents);
  } break;

  case ForLoop::Kind::INVALID:
  default:
    internal_error("Tried to typecheck invalid for-loop kind: {}.", for_stmt->for_statement_kind);
  }

  //internal_errortyper, stmt.while_stmt.then);
}

static void typecheck_while_statement(Typer& typer, Ref<WhileLoop> while_stmt) {
  auto cond_type = typecheck_expression(typer, while_stmt->condition_node);
  auto bool_type = std::make_shared<BuiltinType>(BuiltinType::Kind::BOOLEAN);
  if (!is_cast_possible(typer, cond_type, bool_type)) {
    typer_error_at(typer, while_stmt->condition_node, "While statement condition should be a `bool`. It is a `{}`", cond_type);
  }

  typecheck_statement(typer, while_stmt->body_node);
}

static bool expect_statement_to_return(Ref<StatementAstNode>& statement, Ref<Type> expected_type, OUT Ref<StatementAstNode>& error_at, OUT Ref<Type>& the_wrong_return_type) {
  switch(statement->statement_kind) {
    case StatementAstNode::CODE_BLOCK: {
      auto code_block = std::dynamic_pointer_cast<CodeBlockStatement> (statement);
      for (auto& code_block_statement : code_block->statement_nodes) {
        if (!expect_statement_to_return(code_block_statement, expected_type, error_at, the_wrong_return_type)) {
          return false;
        }
      }

      return true;
    } break;

    case StatementAstNode::IF: {
      auto if_statement = std::dynamic_pointer_cast<IfStatement> (statement);
      if (!expect_statement_to_return(if_statement->body_node, expected_type, error_at, the_wrong_return_type)) {
        return false;
      }

      if (if_statement->else_node) {
        if (!expect_statement_to_return(if_statement->else_node, expected_type, error_at, the_wrong_return_type)) {
          return false;
        }
      }

      return true;
    } break;

    case StatementAstNode::Kind::MATCH: {
      auto match_statement = std::dynamic_pointer_cast<MatchStatement>(statement);

      for (auto& match_case : match_statement->cases) {
        auto then_statement = std::dynamic_pointer_cast<StatementAstNode>(match_case.then);
        if (!expect_statement_to_return(then_statement, expected_type, error_at, the_wrong_return_type)) return false;
      }

      auto else_case = std::dynamic_pointer_cast<StatementAstNode>(match_statement->else_case);
      if (!expect_statement_to_return(else_case, expected_type, error_at, the_wrong_return_type)) return false;

      return true;
    } break;

    case StatementAstNode::FOR: {
      auto for_loop = std::dynamic_pointer_cast<ForLoop> (statement);
      return expect_statement_to_return(for_loop->body_node, expected_type, error_at, the_wrong_return_type);
    } break;

    case StatementAstNode::WHILE: {
      auto while_loop = std::dynamic_pointer_cast<WhileLoop> (statement);
      return expect_statement_to_return(while_loop->body_node, expected_type, error_at, the_wrong_return_type);
    } break;

    case StatementAstNode::DEFER: {
      auto defer_statement = std::dynamic_pointer_cast<DeferStatement> (statement);
      return expect_statement_to_return(defer_statement->deferred_node, expected_type, error_at, the_wrong_return_type);
    } break;

    case StatementAstNode::RETURN: {
      auto return_statement = std::dynamic_pointer_cast<ReturnStatement> (statement);
      if (*return_statement->returned_expression->type != *expected_type) {
        error_at = return_statement;
        the_wrong_return_type = return_statement->returned_expression->type;
        return false;
      }

      return true;
    } break;

    case StatementAstNode::TYPE_ALIAS:             return true; // Type aliases cannot return.
    case StatementAstNode::INCLUDE:                return true; // Include statements cannot return.
    case StatementAstNode::EXPRESSION:             return true; // Expression statements cannot return, so this function shouldn't cause a failure at the call site.
    case StatementAstNode::BREAK:                  return true; // Break statements cannot return.

    // @Correctness: can declarations return? I guess procedure declarations can have return statements in them,
    // but I think atm we only enter this function from whithin the context of parsing another procedure body.
    // Do I need to check nested proc decls here?
    case StatementAstNode::VARIABLE_DECLARATION:   return true; 
    case StatementAstNode::PROCEDURE_DECLARATION:  return true; 
    case StatementAstNode::STRUCT_DECLARATION:     return true; 
    case StatementAstNode::ENUM_DECLARATION:       return true; 

    case StatementAstNode::INVALID: 
    default: 
      internal_error("Cannot expect invalid statement kind `{}` to return anything.", (int)statement->statement_kind);
  }

  internal_error("Fell through the switch checking whether a statement returns the expected type.");
  return false;
};


static bool statement_returns_on_all_control_paths(Ref<StatementAstNode> statement);

static bool check_all_code_paths_return(Vec<Ref<StatementAstNode>> statements) {
  if (statements.empty())  return false;

  if (statement_returns_on_all_control_paths(statements[0])) {
    return true;
  }

  return check_all_code_paths_return(std::vector(statements.begin()+1, statements.end()));
}

static bool statement_returns_on_all_control_paths(Ref<StatementAstNode> statement) {
  switch(statement->statement_kind) {
    case StatementAstNode::RETURN: return true;

    case StatementAstNode::CODE_BLOCK: {
      auto code_block = std::dynamic_pointer_cast<CodeBlockStatement>(statement);
      return check_all_code_paths_return(code_block->statement_nodes);
    } break;

    case StatementAstNode::DEFER: {
      auto defer_statement = std::dynamic_pointer_cast<DeferStatement>(statement);
      return statement_returns_on_all_control_paths(defer_statement->deferred_node);
    } break;

    case StatementAstNode::IF: {
      auto if_statement = std::dynamic_pointer_cast<IfStatement>(statement);

      if (statement_returns_on_all_control_paths(if_statement->body_node)) {
        // If the "then" branch of the else branch returns, but there's an else statement, we need to check that the
        // else statement also returns. Otherwise, we would have to know that the condition always evaluates to true.
        if (!if_statement->else_node) {
          // @Todo: check if the condition of the if statement always evaluates to true.
          // if (condition is always true) return true;
        }

        // @Copypasta
        if (if_statement->else_node) {
          if (statement_returns_on_all_control_paths(if_statement->else_node)) return true;
        }
      }

      // @Copypasta
      if (if_statement->else_node) {
        if (statement_returns_on_all_control_paths(if_statement->else_node)) return true;
      }

      return false;
    } break;

    case StatementAstNode::Kind::MATCH: {
      auto match_statement = std::dynamic_pointer_cast<MatchStatement>(statement);

      auto one_case_returns = false;
      for (auto& match_case : match_statement->cases) {
        if (statement_returns_on_all_control_paths(match_case.then)) one_case_returns = true;
      }

      if (one_case_returns) {
        for (auto& match_case : match_statement->cases) {
          if (!statement_returns_on_all_control_paths(match_case.then)) return false;
        }
      }

      if (one_case_returns && !statement_returns_on_all_control_paths(match_statement->else_case)) return false;

      return true;
    } break;

    case StatementAstNode::FOR: {
      auto for_loop = std::dynamic_pointer_cast<ForLoop>(statement);
      return statement_returns_on_all_control_paths(for_loop->body_node);
    } break;

    case StatementAstNode::WHILE: {
      auto while_loop = std::dynamic_pointer_cast<WhileLoop>(statement);
      return statement_returns_on_all_control_paths(while_loop->body_node);
    } break;

    case StatementAstNode::VARIABLE_DECLARATION:  return true; 
    case StatementAstNode::PROCEDURE_DECLARATION: return true; 
    case StatementAstNode::STRUCT_DECLARATION:    return true; 
    case StatementAstNode::ENUM_DECLARATION:      return true; 

    case StatementAstNode::EXPRESSION:            return false;
    case StatementAstNode::TYPE_ALIAS:            return false;
    case StatementAstNode::INCLUDE:               return false;
    case StatementAstNode::BREAK:                 return false;

    case StatementAstNode::INVALID:
    default:
    internal_error("Invalid statement cannot return anything.");
      break;
  }

  return false;
}

static void typecheck_procedure_returns(Typer& typer, Ref<ProcedureDeclaration>& proc_decl) {
  auto& return_type = std::dynamic_pointer_cast<ProcedureType> (proc_decl->type)->return_type;
  if (return_type->is_void()) return; // @Todo: not technically correct, need to check if any statements actually return a value.

  auto& body = proc_decl->body;

  for (auto& statement : body->statement_nodes) {
    Ref<StatementAstNode> statement_that_returned_the_wrong_type = nullptr;
    Ref<Type> the_wrong_return_type = nullptr;
    if (!expect_statement_to_return(statement, return_type, statement_that_returned_the_wrong_type, the_wrong_return_type)) {
        typer_error_at(typer, statement_that_returned_the_wrong_type, "Cannot return {} - function expected to return {}.", the_wrong_return_type, return_type);
    }
  }

  // Check all code paths return a value.

  if (!statement_returns_on_all_control_paths(body)) {
    typer_error_at(typer, proc_decl, "Not all code paths return a value.");
  }
}

static void typecheck_procedure_declaration(Typer& typer, Ref<ProcedureDeclaration> procedure_declaration) {
  typer.current_proc = procedure_declaration;

  assert(procedure_declaration->type->is_procedure());

  auto proc_type = std::dynamic_pointer_cast<ProcedureType>(procedure_declaration->type);

  if (procedure_declaration->body) {
    for (auto& arg : proc_type->arguments) {
      log_assert(arg.type != nullptr, "Argument type should have been checked already.");
      log_assert(!arg.type->is_unresolved(), "Argument type should have been checked already.");

      set_binding(typer, arg.name, Typer::Binding::Kind::VARIABLE, arg.type);
    }

    typecheck_statement(typer, procedure_declaration->body);

    for (auto& arg : proc_type->arguments) {
      clear_binding(typer, arg.name.contents);
    }

    typecheck_procedure_returns(typer, procedure_declaration);
  }

  typer.current_proc = nullptr;
}

static void typecheck_variable_declaration(Typer& typer, Ref<VariableDeclaration>& var_decl) {
  if (!var_decl->type_tokens.empty()) {
    size_t tokens_parsed = 0;
    auto var_decl_type_parsing_result = consume_type(typer, var_decl->name_token, var_decl->type_tokens, tokens_parsed);
    switch (var_decl_type_parsing_result.error) {
    case OK: break;
    case TYPE_WAS_EMPTY:                             typer_error_at(typer, var_decl, "No type provided."); break;

    case UNRECOGNISED_TYPE_NAME:                     typer_error_at(typer, var_decl, "Unrecognised type name."); break;

    case UNRECOGNISED_STRUCT_INSTANCE_ARGUMENT_TYPE: typer_error_at(typer, var_decl, "One of the struct parameters provided had an unrecognised type."); break;
    case NOT_ENOUGH_STRUCT_PARAMETERS:               typer_error_at(typer, var_decl, "`{}` declared as a parameterised struct, but not enough parameters were provided.", var_decl->name_token.contents); break;
    case STRUCT_EXPECTED_PARAMETERS_BUT_NONE_GIVEN:  typer_error_at(typer, var_decl, "`{}` declared as a parameterised struct, but no parameters were provided.", var_decl->name_token.contents);  break;

    case BASIC_STRUCT_GIVEN_PARAMETERS:              typer_error_at(typer, var_decl, "`{}` is a basic struct but it was given parameters.", var_decl->name_token.contents);  break;

    case BOXED_ENUM_VALUE_TYPE_NOT_VALID:            internal_error("Shouldn't be trying to typecheck enum declarations."); break;

    case UNRECOGNISED_PROCEDURE_ARGUMENT_TYPE:       internal_error("Shouldn't be trying to typecheck procedure declarations."); break;
    case UNRECOGNISED_PROCEDURE_RETURN_TYPE:         internal_error("Shouldn't be trying to typecheck procedure declarations."); break;

    case UNRECOGNISED_STRUCT_PARAM_TYPE:             internal_error("Shouldn't be trying to typecheck struct declarations."); break;
    case UNRECOGNISED_STRUCT_MEMBER_TYPE:            internal_error("Shouldn't be trying to typecheck struct declarations."); break;


    default: internal_error("Invalid type parsing error."); break;
    }

    if (var_decl_type_parsing_result.error == Type_Parsing_Error::OK) {
      var_decl->type = var_decl_type_parsing_result.type;
    }
    else {
      var_decl->type = UnresolvedType::make();
    }
  }
  else {
    log_assert(var_decl->value != nullptr, "If the user didn't give a variable declaration a type (like `x := 10`), expect a value.");
  }

  if (var_decl->value) {
    auto value_expression = std::dynamic_pointer_cast<ExpressionAstNode> (var_decl->value);
    auto value_type = typecheck_expression(typer, value_expression);

    if (!var_decl->type_tokens.empty()) {
      if (!is_assignment_possible(typer, value_type, var_decl->type)) {
        typer_error_at(typer, var_decl, "`{}` declared as `{}` but initialised with a {}.", var_decl->name_token.contents, var_decl->type, value_type);
      }
    }
    else {
      var_decl->type = value_type; // Type inference!
    }
  }

  // Global variables are added to the global context before type checking
  // starts. It would be redundant to check if a global variables exists
  // when the environment only has one scope (ie we are typechecking a var
  // decl in the global scope).
  Typer::Binding binding{};
  if (get_bound_name(typer, var_decl->name_token.contents, &binding)) {
    typer_error_redeclaration(typer, var_decl, binding);
  }
  else {
  }
}

static void typecheck_return_statement(Typer& typer, Ref<ReturnStatement> stmt) {
  if (!typer.current_proc) {
    typer_error_at(typer, stmt, "return statement only allowed inside procedure");
  }

  // assert(typer.current_proc);

  typecheck_expression(typer, stmt->returned_expression);
}

static void typecheck_match_statement(Typer& typer, Ref<MatchStatement> match_statement) {
  auto matched_type = typecheck_expression(typer, match_statement->match_on);

  for (auto& match_case : match_statement->cases) {
    auto case_type = typecheck_expression(typer, match_case.value);
    if (*case_type != *matched_type) {
      typer_error_at(typer, match_case.value, "Match statement case has incorrect type (got {}, expected {})", case_type, matched_type);
    }

    if (case_type->is_boxed_enum()) {
      auto boxed_enum_type = std::dynamic_pointer_cast<BoxedEnumType>(case_type);
      
      auto enum_access_binary_expression = std::dynamic_pointer_cast<BinaryExpression>(match_case.value);
      if(enum_access_binary_expression->rhs_expression->expression_kind == ExpressionAstNode::Kind::PROCEDURE_CALL) { //, "Typechecking a match statement, a boxed member's case value does not look like a procedure call.");
        auto boxed_member_proc_call = std::dynamic_pointer_cast<ProcedureCallExpression>(enum_access_binary_expression->rhs_expression);

        // if (boxed_member_proc_call->argument_nodes.size() != )
        log_assert(boxed_member_proc_call->called_node->expression_kind == ExpressionAstNode::Kind::VARIABLE_REFERENCE, "Typechecking match statement, a boxed members rhs procedure called node is not a variable reference.");
        auto member_name = std::dynamic_pointer_cast<VariableReferenceExpression>(boxed_member_proc_call->called_node);

        auto current_member = boxed_enum_type->find_member(member_name->name.contents);
        log_assert(current_member != nullptr, "Typechecking match statement, boxed enum member {} was not found.", member_name->name);
        log_assert(current_member->kind == EnumType::Member::Kind::BOXED, "Typechecking match statement, boxed enum member {} is not actually boxed.", member_name->name);
        auto boxed_member = std::dynamic_pointer_cast<BoxedEnumType::Member>(current_member);

        for (size_t boxed_value_index = 0; boxed_value_index < boxed_member_proc_call->argument_nodes.size(); ++boxed_value_index) {
          auto& boxed_value = boxed_member_proc_call->argument_nodes[boxed_value_index];
          log_assert(boxed_value->expression_kind == ExpressionAstNode::Kind::VARIABLE_REFERENCE, "Typechecking match statement, a boxed value procedure argument is not a variable reference expression. It should be a variable reference expression so that we can inject the variable name into the case body.");
          auto variable = std::dynamic_pointer_cast<VariableReferenceExpression>(boxed_value);
          log_warning("variable is {}: {}", variable->name, boxed_member->type_of_named_value(variable->name.contents));
          set_binding(typer, variable->name, Typer::Binding::Kind::VARIABLE, boxed_member->type_of_named_value(member_name->name.contents));
        }
      }
    }
    typecheck_statement(typer, match_case.then);
  }

  typecheck_statement(typer, match_statement->else_case);
}

static void typecheck_statement(Typer& typer, Ref<StatementAstNode> statement) {
  if (statement->type_checked)  return;

  switch (statement->statement_kind) {
  case StatementAstNode::Kind::CODE_BLOCK: {
    typecheck_code_block_statement(typer, std::dynamic_pointer_cast<CodeBlockStatement> (statement));
  } break;

  case StatementAstNode::Kind::EXPRESSION: {
    typecheck_expression_statement(typer, std::dynamic_pointer_cast<ExpressionStatement> (statement));
  } break;

  case StatementAstNode::Kind::DEFER: {
    typecheck_defer_statement(typer, std::dynamic_pointer_cast<DeferStatement>(statement));
  } break;

  case StatementAstNode::Kind::IF: {
    typecheck_if_statement(typer, std::dynamic_pointer_cast<IfStatement>(statement));
  } break;

  case StatementAstNode::Kind::MATCH: {
    typecheck_match_statement(typer, std::dynamic_pointer_cast<MatchStatement>(statement));
  } break;

  case StatementAstNode::Kind::FOR: {
    typecheck_for_statement(typer, std::dynamic_pointer_cast<ForLoop>(statement));
  } break;

  case StatementAstNode::Kind::WHILE: {
    typecheck_while_statement(typer, std::dynamic_pointer_cast<WhileLoop>(statement));
  } break;

  case StatementAstNode::Kind::PROCEDURE_DECLARATION: {
    auto procedure_declaration = std::dynamic_pointer_cast<ProcedureDeclaration> (statement);
    typecheck_procedure_declaration(typer, procedure_declaration);
  } break;

  case StatementAstNode::Kind::VARIABLE_DECLARATION: {
    // Global variables should already have been dealt with.
    if (typer.environment.size() > 1) {
      auto var_decl = std::dynamic_pointer_cast<VariableDeclaration> (statement);
      typecheck_variable_declaration(typer, var_decl);
      set_binding(typer, var_decl->name_token, Typer::Binding::Kind::VARIABLE, var_decl->type);
    }
  } break;

  case StatementAstNode::Kind::STRUCT_DECLARATION: {
    auto struct_declaration = std::dynamic_pointer_cast<StructDeclaration> (statement);
    log_assert(struct_declaration->type != nullptr, "Struct declaration types should be resolved before we start walking the rest of the Ast.");
  } break;

  case StatementAstNode::Kind::ENUM_DECLARATION: {
    auto enum_declaration = std::dynamic_pointer_cast<EnumDeclaration> (statement);
    log_assert(enum_declaration->type != nullptr, "Enum declaration types should be resolved before we start walking the rest of the Ast.");
  } break;


  case StatementAstNode::Kind::INCLUDE: {
    // @Todo: typechecking include?
  } break;

  case StatementAstNode::Kind::TYPE_ALIAS: {
    // @Todo: typechecking type aliases?
  } break;

  case StatementAstNode::Kind::BREAK: {
    // @Todo: typechecking type aliases?
  } break;

  case StatementAstNode::Kind::RETURN: {
    typecheck_return_statement(typer, std::dynamic_pointer_cast<ReturnStatement>(statement));
  } break;

  case StatementAstNode::Kind::INVALID:
  default:
    internal_error("Tried to typecheck invalid Statement_Node::Kind {}.", (int)statement->statement_kind);
  }

  statement->type_checked = true;
}

TypecheckResult typecheck_ast(Ast& ast) {
  Typer typer = {};
  typer.ast = &ast;

  bool entry_point_exists = false;

  typer.environment = {};
  typer.environment.push({}); // Global context

  {
    // Resolve the types of all the top level declarations.
    // Declarations may happen in order, so we may need to go over the program
    // multiple times to resolve the types declared out-of-order.
    std::deque<Ref<StatementAstNode>> unresolved_declarations;

    // Initially, all top level declarations are unresolved.
    for (auto& global_declaration : ast.global_declarations) {
      unresolved_declarations.push_back(global_declaration);
    }

    u64 unresolved_declarations_count = unresolved_declarations.size();
    u32 iterations_since_last_resolution = 0;
    u32 max_iterations_between_resolutions = 1024;

    while (!unresolved_declarations.empty()) {
      auto statement = unresolved_declarations.front();
      unresolved_declarations.pop_front();

      Type_Parsing_Result type_parsing_result;

      switch(statement->statement_kind) {
      case StatementAstNode::Kind::PROCEDURE_DECLARATION:  {
        type_parsing_result.error = Type_Parsing_Error::OK;

        auto procedure_declaration = std::dynamic_pointer_cast<ProcedureDeclaration>(statement);
        if (procedure_declaration->type && !procedure_declaration->type->is_unresolved())  continue;

        Vec<ProcedureType::Argument> arguments = {};
        for (auto& declaration_argument : procedure_declaration->arguments) {
          typecheck_variable_declaration(typer, declaration_argument);
          if (declaration_argument->type != nullptr && !declaration_argument->type->is_unresolved()) {
            arguments.push_back(ProcedureType::Argument( declaration_argument->name_token, declaration_argument->type));
          }
          else {
            type_parsing_result.error = Type_Parsing_Error::UNRECOGNISED_PROCEDURE_ARGUMENT_TYPE;
          }
        }

        if (type_parsing_result.error != Type_Parsing_Error::OK) {
          unresolved_declarations.push_back(procedure_declaration);
          break;
        }

        if (procedure_declaration->return_type_tokens.empty()) {
          type_parsing_result.type = ProcedureType::make(arguments, BuiltinType::make(BuiltinType::Kind::V0ID));
          break;
        }

        size_t tokens_parsed = 0;
        auto return_type_parsing_result = consume_type(typer, procedure_declaration->name_token, procedure_declaration->return_type_tokens, tokens_parsed);

        if (return_type_parsing_result.error != OK) {
          type_parsing_result.error = Type_Parsing_Error::UNRECOGNISED_PROCEDURE_RETURN_TYPE;
          unresolved_declarations.push_back(procedure_declaration);
          break;
        }

        type_parsing_result.type = ProcedureType::make(arguments, return_type_parsing_result.type);
      } break;

      case StatementAstNode::Kind::VARIABLE_DECLARATION:  {
        auto variable_declaration = std::dynamic_pointer_cast<VariableDeclaration>(statement);
        if (variable_declaration->type && !variable_declaration->type->is_unresolved())  continue;

        typecheck_variable_declaration(typer, variable_declaration);
        if (variable_declaration->type == nullptr || variable_declaration->type->is_unresolved()) {
          type_parsing_result.error = Type_Parsing_Error::UNRECOGNISED_TYPE_NAME;
          unresolved_declarations.push_back(variable_declaration);
          break;
        }

        type_parsing_result.type = variable_declaration->type;

        Typer::Binding binding;
        if (get_bound_name(typer, variable_declaration->name_token.contents, &binding)) {
          typer_error_redeclaration(typer, variable_declaration, binding);
        }
        else {
          // set_binding(typer, variable_declaration->name_token, Typer::Binding::Kind::VARIABLE, variable_declaration->type);
        }
      } break;

      case StatementAstNode::Kind::STRUCT_DECLARATION:  {
        auto struct_declaration = std::dynamic_pointer_cast<StructDeclaration>(statement);
        if (struct_declaration->type && !struct_declaration->type->is_unresolved())  continue;

        type_parsing_result.error = Type_Parsing_Error::OK;

        Vec<StructType::Member> members = {};
        for (auto& member : struct_declaration->members) {
          typecheck_variable_declaration(typer, member);
          if (member->type != nullptr && !member->type->is_unresolved()) {
            members.push_back(StructType::Member(member->name_token, member->type));
          }
          else {
            type_parsing_result.error = Type_Parsing_Error::UNRECOGNISED_PROCEDURE_ARGUMENT_TYPE;
          }
        }

        if (type_parsing_result.error != OK) {
          unresolved_declarations.push_back(statement);
          break;
        }

        type_parsing_result.type = StructType::make(struct_declaration->name_token, members);
      } break;

      case StatementAstNode::Kind::ENUM_DECLARATION:  {
        auto enum_declaration = std::dynamic_pointer_cast<EnumDeclaration>(statement);
        if (enum_declaration->type && !enum_declaration->type->is_unresolved())  continue;

        size_t tokens_parsed = 0;
        type_parsing_result = consume_type(typer, enum_declaration->name_token, enum_declaration->type_tokens, tokens_parsed);

        if (type_parsing_result.error != OK || type_parsing_result.type->is_unresolved()) {
          unresolved_declarations.push_back(enum_declaration);
        }
      } break;


      case StatementAstNode::Kind::INVALID:     
      case StatementAstNode::Kind::CODE_BLOCK:  
      case StatementAstNode::Kind::EXPRESSION: 
      case StatementAstNode::Kind::IF:        
      case StatementAstNode::Kind::FOR:      
      case StatementAstNode::Kind::BREAK:      
      case StatementAstNode::Kind::WHILE:   
      case StatementAstNode::Kind::RETURN: 
      case StatementAstNode::Kind::TYPE_ALIAS:  
      case StatementAstNode::Kind::INCLUDE:    
      case StatementAstNode::Kind::DEFER: 
      case StatementAstNode::Kind::MATCH:
      default: internal_error("Can't resolve the type of top-level {} statement.", statement->statement_kind);
      break;
      }

      if (type_parsing_result.error == OK) {
        log_assert(type_parsing_result.type != nullptr, "Type parsing produced a null type ");

        if (type_parsing_result.type->is_struct()) {
          log_assert(statement->statement_kind == StatementAstNode::Kind::STRUCT_DECLARATION, "The parser didn't mark a declaration whose type looks like a struct as a struct declaration.");
          auto struct_declaration = std::dynamic_pointer_cast<StructDeclaration> (statement);
          auto& struct_name = struct_declaration->name_token.contents;

          struct_declaration->type = type_parsing_result.type;

          auto struct_type = std::dynamic_pointer_cast<StructType>(struct_declaration->type);
          struct_type->name_token = struct_declaration->name_token;

          log_assert(typer.user_defined_types.find(struct_name) == typer.user_defined_types.end(), "We shouldn't be re-defining struct types.");
          typer.user_defined_types.insert({ struct_name, struct_type });

          // ast.struct_defs.push_back(struct_declaration);
        }
        else if (type_parsing_result.type->is_procedure()) {
          log_assert(statement->statement_kind == StatementAstNode::Kind::PROCEDURE_DECLARATION, "The parser didn't mark a declaration whose type looks like a procedure as a procedure declaration.");
          auto procedure_declaration = std::dynamic_pointer_cast<ProcedureDeclaration> (statement);

          procedure_declaration->type = type_parsing_result.type;

          auto &name = procedure_declaration->name_token;

          if (name.contents == "main")  entry_point_exists = true;

          Typer::Binding existing_binding{};
          if (get_bound_name(typer, name.contents, &existing_binding)) {
            typer_error_redeclaration(typer, procedure_declaration, existing_binding);
            continue;
          }

          // ast.proc_decls.push_back(procedure_declaration);

          set_binding(typer, name, Typer::Binding::Kind::VARIABLE, procedure_declaration->type);
        }
        else if (type_parsing_result.type->is_enum()) {
          log_assert(statement->statement_kind == StatementAstNode::Kind::ENUM_DECLARATION, "The parser didn't mark a declaration whose type looks like a enum as a enum declaration.");
          auto enum_declaration = std::dynamic_pointer_cast<EnumDeclaration> (statement);

          enum_declaration->type = type_parsing_result.type;

          typer.user_defined_types.insert({ enum_declaration->name_token.contents, enum_declaration->type });

          // ast.enum_declarations.push_back(enum_declaration);

          set_binding(typer, enum_declaration->name_token, Typer::Binding::Kind::USER_DEFINED_TYPE, enum_declaration->type);
        }
        else {
          log_assert(statement->statement_kind == StatementAstNode::Kind::VARIABLE_DECLARATION, "The parser didn't mark a declaration whose type looks like a variable as a variable declaration.");
          auto variable_declaration = std::dynamic_pointer_cast<VariableDeclaration> (statement);

          variable_declaration->type = type_parsing_result.type;

          if (variable_declaration->value) {
            auto value_expression = std::dynamic_pointer_cast<ExpressionAstNode> (variable_declaration->value);
            auto value_type = typecheck_expression(typer, value_expression);
            if (!is_assignment_possible(typer, value_type, variable_declaration->type)) {
              typer_error_at(typer, variable_declaration, "`{}` declared as `{}` but initialised with a {}.", variable_declaration->name_token, variable_declaration->type, value_type);
            }
          }

          // ast.global_variables.push_back(variable_declaration);

          set_binding(typer, variable_declaration->name_token, Typer::Binding::Kind::VARIABLE, variable_declaration->type);
        }
      }

      if (unresolved_declarations.size() == unresolved_declarations_count) {
        ++iterations_since_last_resolution;

        if (iterations_since_last_resolution >= max_iterations_between_resolutions) {
          break;
        }
      }

      unresolved_declarations_count = unresolved_declarations.size();
    }

    if (!unresolved_declarations.empty()) {
      typer_error_at(typer, ast.program_node, "{} top-level declarations had incomplete types.", unresolved_declarations.size());

      return { typer.had_error ? Typecheck_Code::HAD_ERROR : Typecheck_Code::OK, typer.ast };
    }
  }

  {
    // Parse the type aliases
    for (size_t i = 0; i < ast.type_aliases.size(); ++i) {
      auto type_alias = ast.type_aliases[i];

      Ref<Type> type = nullptr;
      if (!type_from_name(typer, type_alias->name.contents, type)) {
        size_t tokens_parsed = 0;
        auto type_alias_type_parsing_result = consume_type(typer, type_alias->name,type_alias->type_tokens, tokens_parsed);

        if (type_alias_type_parsing_result.error != OK) {
          internal_error("Couldn't parse the type of an alias."); // All types should be resolvable at this point
        }

        type_alias->type = type_alias_type_parsing_result.type;

        Typer::Binding existing_binding{};
        if (get_bound_name(typer, type_alias->type->name(), &existing_binding)) {
          set_binding(typer, type_alias->name, Typer::Binding::Kind::TYPE_ALIAS, existing_binding.type);
        }
        else {
          typer_error_at(typer, ast.type_aliases[i], "Type alias to unrecognized type `{}`", "@Todo: type name");
        }
      }
      else {
        typer_error_at(typer, ast.type_aliases[i], "Type name `{}` has already been defined. @Todo: where?", type_alias->name.contents);
      }
    }
  }

  if (!entry_point_exists) {
    typer_error_at(typer, ast.program_node, "Entry point (`main`) has not been declared.");
  }

  auto root = std::dynamic_pointer_cast<ProgramAstNode> (ast.program_node);
  for (size_t node_index = 0; node_index < root->top_level_statements.size(); ++node_index) {
    auto& node = root->top_level_statements[node_index];

    switch (node->node_kind) {
    case AstNode::Kind::EXPRESSION: {
      auto expression = std::dynamic_pointer_cast<ExpressionAstNode> (node);
      typecheck_expression(typer, expression);
    } break;
    case AstNode::Kind::STATEMENT: {
      typecheck_statement(typer, node);
    } break;
    case AstNode::Kind::PROGRAM:
      internal_error("Typechecker ran into a second program node.");
      break;

    case AstNode::Kind::INVALID:
    default:
      internal_error("Typechecker ran into invalid node kind: {}.", node->node_kind);
    }
  }

  return { typer.had_error ? Typecheck_Code::HAD_ERROR : Typecheck_Code::OK, typer.ast };
}

static Ref<Type> typecheck_procedure_call_expression(Typer& typer, Ref<ProcedureCallExpression> &call) {
    // Lookup proc call by name. Do not support `(x+1)()` yet
    log_assert(is_expression(call->called_node), "proc call should be an expression.");

    auto call_expr = std::dynamic_pointer_cast<ExpressionAstNode>(call->called_node);
    log_assert(call_expr->expression_kind == ExpressionAstNode::Kind::VARIABLE_REFERENCE, "@Incomplete: proc can only be called by name");

    auto call_name = std::dynamic_pointer_cast<VariableReferenceExpression>(call_expr);

    // @Todo: `print` is still a built-in
    // @Todo: typecheck `print` arguments
    if (call_name->name.contents == "print") {
      for (size_t arg_index = 0; arg_index < call->argument_nodes.size(); ++arg_index) {
        typecheck_expression(typer, call->argument_nodes[arg_index]);
      }

      call->type_checked = true;
      call->type = BuiltinType::make(BuiltinType::Kind::V0ID);
      return call->type;
    }

    if (call_name->name.contents == "array_push") {
      if (call->argument_nodes.size() != 2) {
        typer_error_at(typer, call, "array_push expects 2 arguments: the array and the element to push.");
        call->type = UnresolvedType::make();
      }
      else {
        auto arg0_type = typecheck_expression(typer, call->argument_nodes[0]);
        auto arg1_type = typecheck_expression(typer, call->argument_nodes[1]);
        if (!arg0_type->is_array()) {
          typer_error_at(typer, call, "array_push expects argument 1 to be an array.");
          call->type = UnresolvedType::make();
        }
        else if (*arg1_type != *std::dynamic_pointer_cast<ArrayType>(arg0_type)->element_type) {
          typer_error_at(typer, call, "array_push expects argument 2 to be the element type of the array in argument 1.");
          call->type = UnresolvedType::make();
        }
        else {
          call->type = BuiltinType::make(BuiltinType::Kind::V0ID);
        }
      }

      call->type_checked = true;
      return call->type;
    }

    auto& proc_decls = typer.ast->proc_decls;

    auto found_proc = false;
    Ref<AstNode> the_proc_decl_node = nullptr;
    for (size_t i = 0; i < proc_decls.size(); ++i) {
      auto decl = proc_decls[i];
      if (decl->name_token.contents == call_name->name.contents) {
        the_proc_decl_node = proc_decls[i];
        found_proc = true;
        break;
      }
    }

    if (!found_proc) {
      typer_error_at(typer, call, "Undefined procedure `{}`.", call_name->name.contents);
      call->type_checked = true;
      call->type = UnresolvedType::make();
      return call->type;
    }

    auto the_proc_decl = std::dynamic_pointer_cast<ProcedureDeclaration>(the_proc_decl_node);

    if (!the_proc_decl->type->is_procedure()) {
      typer_error_at(typer, call, "`{}` is not a procedure.", call_name->name.contents);
      call->type_checked = true;
      call->type = UnresolvedType::make();
      return call->type;
    }

    auto the_proc_type = std::dynamic_pointer_cast<ProcedureType>(the_proc_decl->type);

    if (the_proc_type->arguments.size() != call->argument_nodes.size()) {
      typer_error_at(typer, call, "`{}` expected {} arguments, have {}.", call_name->name.contents, the_proc_type->arguments.size(), call->argument_nodes.size());
      call->type_checked = true;
      call->type = the_proc_type->return_type;
      return call->type;
    }

    for (size_t i = 0; i < call->argument_nodes.size(); ++i) {
      auto arg_type = typecheck_expression(typer, call->argument_nodes[i]);

      log_assert(the_proc_type->arguments[i].type != nullptr, "Proc argument type should have been resolved already");
      if (!is_cast_possible(typer, arg_type, the_proc_type->arguments[i].type)) {
        typer_error_at(typer, call, "Expected argument {} to be a {}. It is a {}.", i + 1, the_proc_type->arguments[i].type, arg_type);
        call->type_checked = true;
        call->type = the_proc_type->return_type;
        return call->type;
      }
    }

    call->type_checked = true;
    call->type = the_proc_type->return_type;
    return call->type;
}

static Ref<Type> typecheck_assignment_expression(Typer& typer, Ref<AssignmentExpression>& assignment) {
  //
  // @Todo: coercion, implicit casting and all that jazz
  //
  assert(is_expression(assignment->rhs_node));
  assert(is_expression(assignment->lhs_node));

  Ref<Type> rhs_type = typecheck_expression(typer, assignment->rhs_node);
  Ref<Type> lhs_type = typecheck_expression(typer, assignment->lhs_node);

  // string += s8 (appending a character to a string.)
  if (assignment->operatorr.info.kind == TokenKind::ADD_EQ) {
    if (lhs_type->is_string() && rhs_type->is_builtin()) {
      auto builtin_type = std::dynamic_pointer_cast<BuiltinType>(rhs_type);
      if (builtin_type->builtin_kind == BuiltinType::Kind::S8) {
        assignment->type_checked = true;
        assignment->type = rhs_type;
        return assignment->type;
      }
    }
  }
  else if (is_assignment_possible(typer, rhs_type, lhs_type)) {
    assignment->type_checked = true;
    assignment->type = rhs_type;
    return assignment->type;
  }
  else {
    typer_error_at(typer, assignment, "{} cannot be assigned to {}", rhs_type, lhs_type);
  }

  assignment->type = rhs_type;
  assignment->type_checked = true;
  return assignment->type;
}

static Ref<Type> typecheck_binary_expression(Typer& typer, Ref<BinaryExpression>& binary) {
  auto resolve_expression = [](Ref<BinaryExpression>& expression, Ref<Type> type) -> Ref<Type> {
    log_assert(!expression->type_checked, "Expression should not have been type checked yet");

    expression->type = type;
    expression->type_checked = true;
    return type;
  };

  auto dereference_if_pointer = [](Ref<Type> type) -> Ref<Type> {
    if (type->is_pointer()) return std::dynamic_pointer_cast<PointerType>(type)->pointed;
    return type;
  };

  assert(is_expression(binary->lhs_expression));
  assert(is_expression(binary->rhs_expression));

  auto lhs_expr = std::dynamic_pointer_cast<ExpressionAstNode> (binary->lhs_expression);
  auto rhs_expr = std::dynamic_pointer_cast<ExpressionAstNode> (binary->rhs_expression);

  // Ref<Type> a_type = typecheck_expression(typer, binary->lhs_node);
  // Ref<Type> b_type = typecheck_expression(typer, binary->rhs_node);

  Ref<Type> lhs_type = typecheck_expression(typer, lhs_expr);

  if (binary->operatorr.info.kind == TokenKind::DOT) {
    lhs_type = dereference_if_pointer(lhs_type);
    if (lhs_type->is_enum()) {
      binary->is_enum_member_reference = true;

      auto enum_type = std::dynamic_pointer_cast<EnumType> (lhs_type);

      switch(enum_type->enum_kind) {
      case EnumType::Kind::SIMPLE: {
        auto simple_enum = std::dynamic_pointer_cast<SimpleEnumType>(enum_type);

        log_assert(rhs_expr->expression_kind == ExpressionAstNode::Kind::VARIABLE_REFERENCE, "Simple enum member can only be accessed by VariableReferenceExpression.");
        auto rhs_expr_as_var_ref = std::dynamic_pointer_cast<VariableReferenceExpression>(rhs_expr);

        auto enum_member = simple_enum->find_member(rhs_expr_as_var_ref->name.contents);
        
        if (enum_member == nullptr) {
          typer_error_at(typer, rhs_expr_as_var_ref, "`{}` is not a member of enum `{}`", rhs_expr_as_var_ref->name.contents, enum_type->name());
          return resolve_expression(binary, default_type());
        }

        return resolve_expression(binary, enum_type);
      } break;

      case EnumType::Kind::BOXED: {
        auto boxed_enum = std::dynamic_pointer_cast<BoxedEnumType>(enum_type);
        
        Token rhs_identifier = {};
        if(rhs_expr->expression_kind == ExpressionAstNode::Kind::PROCEDURE_CALL) { // , "Boxed enum member can only be accessed by ProcedureReferenceExpression.");
          auto rhs_as_proc_call = std::dynamic_pointer_cast<ProcedureCallExpression>(rhs_expr);

          log_assert(is_expression(rhs_as_proc_call->called_node), "Accessing boxed enum member, the fake proc call's called node is not an expression.");
          auto rhs_proc_call_called_node_as_expr = std::dynamic_pointer_cast<ExpressionAstNode>(rhs_as_proc_call->called_node);
          log_assert(rhs_proc_call_called_node_as_expr->expression_kind == ExpressionAstNode::Kind::VARIABLE_REFERENCE, "Accessing boxed enum member, the fake proc call's called node is not a variable reference.");
          rhs_identifier = std::dynamic_pointer_cast<VariableReferenceExpression>(rhs_proc_call_called_node_as_expr)->name;
        }
        else if (rhs_expr->expression_kind == ExpressionAstNode::Kind::VARIABLE_REFERENCE) {
          auto rhs_expr_as_var_ref = std::dynamic_pointer_cast<VariableReferenceExpression>(rhs_expr);
          rhs_identifier = rhs_expr_as_var_ref->name;
        }
        else {
          internal_error("Boxed enum member can only be accessed by VariableReferenceExpression or ProcedureReferenceExpression.");
        }

        auto enum_member = boxed_enum->find_member(rhs_identifier.contents);

        if (enum_member == nullptr) {
          typer_error_at(typer, rhs_expr, "`{}` is not a member of enum `{}`", rhs_identifier.contents, enum_type->name());
          return resolve_expression(binary, default_type());
        }

        return resolve_expression(binary, enum_type);
      } break;

      default: internal_error("Cannot get the type of enum member access for invalid EnumType::Kind {}", (int)enum_type->enum_kind);
      }

      internal_error("Enum access binary expression did not get resolved. Should have returned already.");
      return default_type();
    }

    Ref<Type> rhs_type = typecheck_expression(typer, rhs_expr);

    if(rhs_expr->expression_kind == ExpressionAstNode::Kind::VARIABLE_REFERENCE) {
      auto rhs_expr_as_var_ref = std::dynamic_pointer_cast<VariableReferenceExpression>(rhs_expr);

      if (lhs_type->is_array()) {
        // @Note: arrays have a .count and a .data member
        if ("count" == rhs_expr_as_var_ref->name.contents) {
          return resolve_expression(binary, BuiltinType::make(BuiltinType::Kind::U64));
        }
        else if ("data" == rhs_expr_as_var_ref->name.contents) {
          return resolve_expression(binary, PointerType::make(BuiltinType::make(BuiltinType::Kind::V0ID)));
        }
        else {
          typer_error_at(typer, binary, "`{}` is not a member of arrays. (Did you mean `.count` or `.data`?)", rhs_expr_as_var_ref->name.contents);
          return resolve_expression(binary, default_type());
        }
      }
      else if (lhs_type->is_string()) {
        if ("length" == rhs_expr_as_var_ref->name.contents) {
          return resolve_expression(binary, BuiltinType::make(BuiltinType::Kind::U64));
        }

        typer_error_at(typer, binary, "`{}` is not a member of string. (Did you mean `.length`?)", rhs_expr_as_var_ref->name.contents);
        return resolve_expression(binary, default_type());
      }
      else if (lhs_type->is_struct()) {
        binary->is_struct_member_reference = true;

        auto struct_type = std::dynamic_pointer_cast<StructType>(lhs_type);

        // If we are referencing a struct member (`pos.x`), we need to check
        // if the struct is parameterised. If it is, we derive the type from the parameters
        // passed to the struct when we declared the variable.

        // @Cleanup: this feels iffy
        Token var_name;
        if (!get_expr_binding_name_if_possible(lhs_expr, &var_name)) {
          internal_error("Couldn't get the binding name for the struct.");
        }

        Typer::Binding variable_binding;
        if (!get_bound_name(typer, var_name.contents, &variable_binding)) {
          typer_error_at(typer, binary, "Struct `{}` has not been declared.", var_name);
          return resolve_expression(binary, default_type());
        }

        // Resolve parameterized struct member reference
        if (struct_type->struct_kind == StructType::Kind::INSTANCE) {
          auto struct_instance = std::static_pointer_cast<StructInstanceType> (struct_type);
          for (size_t member_index = 0; member_index < struct_instance->members.size(); ++member_index) {
            auto &member = struct_instance->members[member_index];

            if (member.name.contents != rhs_expr_as_var_ref->name.contents)  continue;

            if (member.refers_to_parameter) {
              log_assert(member.parameter_index < struct_instance->instance_arguments.size(), "Member parameter points outside of the parameter list");
              return resolve_expression(binary, struct_instance->instance_arguments[member.parameter_index].type);
            }

            return resolve_expression(binary, member.type);
          }

          internal_error("Couln't find the struct member `{}`", rhs_expr_as_var_ref->name.contents);
        }
        else {
          for (size_t struct_index = 0; struct_index < typer.ast->struct_defs.size(); ++struct_index) {
            auto struct_def = std::dynamic_pointer_cast<StructDeclaration>(typer.ast->struct_defs[struct_index]);

            log_assert(!struct_def->type->is_unresolved(), "Struct def type should have been resolved.");

            if (struct_def->name_token.contents != struct_type->name_token.contents)  continue;

            for (size_t member_index = 0; member_index < struct_type->members.size(); ++member_index) {
              auto &member = struct_type->members[member_index];

              if (member.name.contents == rhs_expr_as_var_ref->name.contents) 
                return resolve_expression(binary, member.type);
            }
          }

          typer_error_at(typer, binary, "`{}` is not a member of `{}`", rhs_expr_as_var_ref->name.contents, struct_type->name_token.contents);
          return resolve_expression(binary, default_type());
        }

        internal_error("Struct access binary expression did not get resolved. Should have returned already.");
        return resolve_expression(binary, default_type());
      }
    }
    else {
      internal_error("RHS of `.` must be a VariableReferenceExpression.");
      return resolve_expression(binary, default_type());
    }

    internal_error("Binary expression `.` did not get resolved. Should have returned already.");
    return resolve_expression(binary, default_type());
  }

  Ref<Type> rhs_type = typecheck_expression(typer, binary->rhs_expression);
  for (auto& binary_operation : global_builtins.binary_operations) {
    if (binary_operation.op == binary->operatorr.info.kind) { // && is_cast_possible(typer, binary_operation.b, binary_operation.a)) {
      return resolve_expression(binary, binary_operation.result);
    }
  }

  typer_error_at(typer, binary, "Invalid operand types in binary expression `{}` `{}` `{}`.", lhs_type, binary->operatorr.info.name, rhs_type);
  return resolve_expression(binary, default_type());
}

static Ref<Type> typecheck_unary_expression(Typer& typer, Ref<UnaryExpression>& unary) {
  assert(is_expression(unary->operand_node));

  Ref<Type> expr_type = typecheck_expression(typer, unary->operand_node);

  if (unary->operatorr.info.kind == TokenKind::POINTER) {
    unary->type = PointerType::make(expr_type);
  }
  else if (unary->operatorr.info.kind == TokenKind::SUB) {
    if (!is_integral(expr_type)) {
      typer_error_at( typer, unary, "Unary `-` must be applied to an integral type. It was given a {}.", expr_type);
    }

    unary->type = expr_type;
  }
  else if (unary->operatorr.info.kind == TokenKind::BANG) {
    auto boolean_type = BuiltinType::make(BuiltinType::Kind::BOOLEAN);
    if (!is_cast_possible(typer, expr_type, boolean_type)) {
      typer_error_at( typer, unary, "Unary `!` must be applied to an integral type. It was given a {}", expr_type);
    }

    unary->type = expr_type;
  }
  else if (unary->operatorr.info.kind == TokenKind::DEREFERENCE) {
    if (!expr_type->is_pointer()) {
      typer_error_at(typer, unary, "Attempt to dereference non-pointer type (was given `{}`)", expr_type);
      unary->type = expr_type;
    }
    else {
      auto builtin_type = std::dynamic_pointer_cast<PointerType>(expr_type);
      if (builtin_type->pointed) {
        unary->type = builtin_type->pointed;
      }
      else {
        internal_error("Pointed type has not been resolved.{}", "");
      }
    }
  }
  else {
    internal_error("Unary expression's operator (`{}`) is not a valid unary operator.", unary->operatorr);
  }

  unary->type_checked = true;
  return unary->type;
}

static Ref<Type> typecheck_array_reference_expression(Typer& typer, Ref<ArrayReferenceExpression>& array_ref) {
  auto array_expression_type = typecheck_expression(typer, array_ref->array_expression);

  if (!array_expression_type->is_array() && !array_expression_type->is_string()) {
    // typer_error_at(typer, array_ref, "`{}` is not an array. (It is a {})", *array_ref->array_expression, array_expression_type);
    // typer_error_at(typer, array_ref, "`{}` is not an array.", *array_ref->array_expression);
    typer_error_at(typer, array_ref, "Expression not indexable with square brackets.");
    array_ref->type = default_type();
    array_ref->type_checked = true;
    return array_ref->type;
  }

  auto index_type = typecheck_expression(typer, array_ref->index_expression);

  if (!is_integral(index_type)) {
    typer_error_at(typer, array_ref, "Vecs can only be indexed by integers (index is currently a {}).", index_type);

    array_ref->type = default_type();
    array_ref->type_checked = true;
    return array_ref->type;
  }

  if (array_expression_type->is_array()) array_ref->type = std::dynamic_pointer_cast<ArrayType>(array_expression_type)->element_type;
  else if (array_expression_type->is_string()) array_ref->type = BuiltinType::make(BuiltinType::Kind::S8);
  else array_ref->type = array_expression_type;

  array_ref->type_checked = true;
  return array_ref->type;
}

static Ref<Type> typecheck_variable_reference_expression(Typer& typer, Ref<VariableReferenceExpression> &var_ref) {
  auto &var_name = var_ref->name;

  // Shouldn't need to type check a member reference?
  if (var_ref->is_struct_member_reference) {
    var_ref->type = UnresolvedType::make();
    return var_ref->type;
  }

  Typer::Binding binding{};
  if (get_bound_name(typer, var_name.contents, &binding)) {

    switch (binding.kind) {
    case Typer::Binding::Kind::USER_DEFINED_TYPE: {
      auto &variable_type = binding.type;
      var_ref->type = variable_type;
    } break;

    case Typer::Binding::Kind::VARIABLE: {
      auto &variable_type = binding.type;
      var_ref->type = variable_type;
    } break;

    case Typer::Binding::Kind::TYPE_ALIAS: {
      auto &alias_type = binding.type;
      var_ref->type = alias_type;
    } break;

    default: internal_error("Got invalid binding kind.");
    }
  }
  else {
    typer_error_at(typer, var_ref, "Variable `{}` has not been declared.", var_name.contents);

    // @Fixme: what is the type of `x` if `x` hasn't been declared yet?
    var_ref->type = UnresolvedType::make();
  }

  var_ref->type_checked = true;
  return var_ref->type;
}

static Ref<Type> typecheck_literal_expression([[maybe_unused]] Typer& typer, Ref<LiteralExpression>& literal) {
  auto literal_type = UnresolvedType::make();
  switch (literal->literal_kind) {
  case LiteralExpression::Kind::BOOLEAN:
    literal_type = BuiltinType::make(BuiltinType::Kind::BOOLEAN);
    break;

  case LiteralExpression::Kind::STRING:
    literal_type = BuiltinType::make(BuiltinType::Kind::STRING);
    break;

  case LiteralExpression::Kind::U64:
    literal_type = BuiltinType::make(BuiltinType::Kind::U64);
    break;

  case LiteralExpression::Kind::F64:
    literal_type = BuiltinType::make(BuiltinType::Kind::F64);
    break;

  case LiteralExpression::Kind::INVALID: 
  default:
    internal_error("Tried to typecheck an invalid literal");
    break;
  }

  literal->type = literal_type;
  literal->type_checked = true;
  return literal->type;
}

static Ref<Type> typecheck_array_literal_expression(Typer& typer, Ref<ArrayLiteralExpression>& array_literal) {
  auto last_type = UnresolvedType::make();

  for (size_t i = 0; i < array_literal->element_nodes.size(); ++i) {
    auto elem = array_literal->element_nodes[i];
    auto type = typecheck_expression(typer, elem);
    if (i > 0 && (*type) != (*last_type)) {
      typer_error_at( typer, elem, "All elements in array literal should have the same type. Element {} and {} have different types ({} and {}).", i, i - 1, type, last_type);
    }

    last_type = type;
  }

  // @Todo: what to do when the types in the array don't all match?
  Ref<Type> array_type = last_type;
  array_literal->type = ArrayType::make(array_literal->element_nodes.size(), array_type);

  array_literal->type_checked = true;
  return array_literal->type;
}

static Ref<Type> typecheck_cast_expression(Typer& typer, Ref<CastExpression>& cast) {
  auto from_type = typecheck_expression(typer, cast->casted_node);

  size_t tokens_parsed = 0;
  auto type_parse_result = consume_type(typer, make_builtin_token("cast"), cast->type_tokens, tokens_parsed);
  if (type_parse_result.error != OK) {
    internal_error("Couldn't parse the type being cast to."); // All types should be resolvable at this point
  }

  auto to_type = type_parse_result.type;

  if (to_type->is_unresolved()) {
    internal_error("Tried to cast to unresolved type.");
  }

  if (!is_cast_possible(typer, from_type, to_type)) {
    typer_error_at(typer, cast, "Cannot cast from {} to {}.", from_type, to_type);
  }

  cast->type = to_type;

  cast->type_checked = true;
  return cast->type;
}

static Ref<Type> typecheck_expression(Typer& typer, Ref<ExpressionAstNode> &expr) {
  if (expr->type_checked) return expr->type;

  switch (expr->expression_kind) {
  case ExpressionAstNode::Kind::GROUPING: {
    auto grouping = std::dynamic_pointer_cast<GroupingExpression>(expr);
    expr->type = typecheck_expression(typer, grouping->grouped_expression);
    expr->type_checked = true;
    return expr->type;
  } break;

  case ExpressionAstNode::Kind::PROCEDURE_CALL: {
    auto call = std::dynamic_pointer_cast<ProcedureCallExpression>(expr);
    return typecheck_procedure_call_expression(typer, call);
  } break;

  case ExpressionAstNode::Kind::ASSIGNMENT: {
    auto assignment = std::dynamic_pointer_cast<AssignmentExpression>(expr);
    return typecheck_assignment_expression(typer, assignment);
  } break;

  case ExpressionAstNode::Kind::BINARY: {
    auto binary = std::dynamic_pointer_cast<BinaryExpression>(expr);
    return typecheck_binary_expression(typer, binary);
  } break;

  case ExpressionAstNode::Kind::UNARY: {
    auto unary = std::dynamic_pointer_cast<UnaryExpression>(expr);
    return typecheck_unary_expression(typer, unary);
  } break;

  case ExpressionAstNode::Kind::ARRAY_REFERENCE: {
    auto array_ref = std::dynamic_pointer_cast<ArrayReferenceExpression>(expr);
    return typecheck_array_reference_expression(typer, array_ref);
  } break;

  case ExpressionAstNode::Kind::VARIABLE_REFERENCE: {
    auto var_ref = std::dynamic_pointer_cast<VariableReferenceExpression>(expr);
    return typecheck_variable_reference_expression(typer, var_ref);
  } break;

  case ExpressionAstNode::Kind::LITERAL: {
    auto literal = std::dynamic_pointer_cast<LiteralExpression>(expr);
    return typecheck_literal_expression(typer, literal);

  } break;

  case ExpressionAstNode::Kind::ARRAY_LITERAL: {
    auto array_literal = std::dynamic_pointer_cast<ArrayLiteralExpression>(expr);
    return typecheck_array_literal_expression(typer, array_literal);
  } break;

  case ExpressionAstNode::Kind::CAST: {
    auto cast = std::dynamic_pointer_cast<CastExpression>(expr);
    return typecheck_cast_expression(typer, cast);
  } break;

  case ExpressionAstNode::Kind::INVALID:
  default: internal_error("Tried to typecheck invalid expression kind.");
  }

  internal_error("Couldn't typecheck {} expression", expr->expression_kind);
  return UnresolvedType::make();
}


