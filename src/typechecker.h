
#ifndef DRAGON_TYPECHECKER_H_
#define DRAGON_TYPECHECKER_H_

#include <stack>

#include "defines.h"

#include "ast.h"
#include "parser.h"
#include "types.h"

struct Typer {
  struct Binding {
    enum class Kind { VARIABLE, USER_DEFINED_TYPE, TYPE_ALIAS };

    Binding::Kind kind;

    Token     name;
    Ref<Type> type = nullptr;
  };

  struct Type_Scope {
    Table<StringView, Binding> bindings = {};
    Ref<AstNode>               node     = nullptr;
  };

  Ast* ast = nullptr;
  bool had_error = false;

  Table<StringView, Ref<Type>> user_defined_types = {};

  Ref<AstNode>           current_proc = nullptr;
  std::stack<Type_Scope> environment  = {};
};

enum class Typecheck_Code {
  OK = 0,
  HAD_ERROR,
};

struct TypecheckResult {
  Typecheck_Code code;
  Ast*           ast;
};

TypecheckResult typecheck_ast(Ast& ast);

#endif  // TYPER_H_
