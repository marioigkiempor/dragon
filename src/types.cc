

#include "types.h"

#include "logging.h"

#include "builtins.h"

Ref<Type> default_type() { return BuiltinType::make(BuiltinType::Kind::S32); }

bool is_boolean(Ref<Type> type) {
  if (type->kind != Type::Kind::BUILTIN)  return false;

  auto builtin = std::dynamic_pointer_cast<BuiltinType>(type);

  return (builtin->builtin_kind == BuiltinType::Kind::BOOLEAN);
}

bool is_string(Ref<Type> type) {
  if (type->kind != Type::Kind::BUILTIN)  return false;

  auto builtin = std::dynamic_pointer_cast<BuiltinType>(type);

  return (builtin->builtin_kind == BuiltinType::Kind::STRING);
}

bool is_floating(Ref<Type> type) {
  if (type->kind != Type::Kind::BUILTIN) return false;

  auto builtin = std::dynamic_pointer_cast<BuiltinType>(type);

  if ((builtin->builtin_kind == BuiltinType::Kind::F64) || (builtin->builtin_kind == BuiltinType::Kind::F32))
    return true;

  return false;
}

bool is_integral(Ref<Type> type) {
  if (type->kind != Type::Kind::BUILTIN) return false;

  auto builtin = std::dynamic_pointer_cast<BuiltinType>(type);

  if ((builtin->builtin_kind == BuiltinType::Kind::S8) ||
      (builtin->builtin_kind == BuiltinType::Kind::S16) ||
      (builtin->builtin_kind == BuiltinType::Kind::S32) ||
      (builtin->builtin_kind == BuiltinType::Kind::S64) ||
      (builtin->builtin_kind == BuiltinType::Kind::U8) ||
      (builtin->builtin_kind == BuiltinType::Kind::U16) ||
      (builtin->builtin_kind == BuiltinType::Kind::U32) ||
      (builtin->builtin_kind == BuiltinType::Kind::U64))
    return true;

  return false;
}

bool operator==(const Type& a, const Type& b) {
  return typeid(a) == typeid(b) && a.equals(b);
}

bool operator!=(const Type& a, const Type& b) {
  return !(a == b);
}

std::string ProcedureType::name() const {
  std::string name = "(";
  auto &args = this->arguments;
  for (size_t i = 0; i < args.size(); ++i) {
    auto &arg = args[i];
    name += std::string(arg.name.contents);
    name += ": ";
    name += arg.type->name();

    if (i != args.size() - 1)
      name += ", ";
  }

  name += ") " + this->return_type->name();
  return name;
}

std::string BuiltinType::name() const {
  return std::string(builtin_type_name(this->builtin_kind));
}

std::string PointerType::name() const {
  std::string name = "^";
  if (this->pointed) name += this->pointed->name();
  return name;
}

std::string StructType::name()  const{
  return std::string(this->name_token.contents);
}

std::string EnumType::name()  const{
  return std::string(this->name_token.contents);
}

std::string ParameterisedStructType::name() const {
  auto name = std::string(this->name_token.contents);
  name += "(";
  for (size_t i = 0; i < this->expected_parameters.size(); ++i) {
    name += this->expected_parameters[i].name.contents;

    if (i != this->expected_parameters.size() - 1) name += ", ";
  }
  name += ")";
  return name;
}

std::string StructInstanceType::name() const {
  auto name = std::string(this->name_token.contents);
  name += "(";
  for (size_t i = 0; i < this->instance_arguments.size(); ++i) {
    name += this->instance_arguments[i].type->name();

    if (i != this->instance_arguments.size() - 1) name += ", ";
  }
  name += ")";
  return name;
}

std::string ArrayType::name() const {
  std::string name = "[";
  if (!this->is_vector) name += std::to_string(this->count);
  name += "]";
  name += this->element_type->name();
  return name;
}

std::string UnresolvedType::name() const { return "unresolved type"; }

std::string TypeInfoType::name() const {
  return std::string(this->name_token.contents);
}

bool UnresolvedType::equals(const Type &other) const {
  auto that = static_cast<const UnresolvedType &>(other);
  return Type::equals(other);
}

bool BuiltinType::equals(const Type &other) const {
  auto that = static_cast<const BuiltinType &>(other);

  if (builtin_kind != that.builtin_kind) return false;

  return Type::equals(other);
}

bool PointerType::equals(const Type &other) const {
  auto that = static_cast<const PointerType &>(other);

  if ((*pointed.get()) != (*that.pointed.get())) return false;

  return Type::equals(other);
}

bool ArrayType::equals(const Type &other) const {
  auto that = static_cast<const ArrayType &>(other);

  if ((*element_type.get()) != (*that.element_type.get())) return false;
  if (count != that.count) return false;

  return Type::equals(other);
}

bool ProcedureType::equals(const Type &other) const {
  auto that = static_cast<const ProcedureType &>(other);

  if ((*return_type.get()) != (*that.return_type.get())) return false;
  if (arguments.size() != that.arguments.size()) return false;
  for (size_t i = 0; i < arguments.size(); i++) {
    if ((*arguments[i].type.get()) != (*that.arguments[i].type.get())) return false;
  }

  return Type::equals(other);
}

bool EnumType::equals(const Type& other) const {
  auto that = static_cast<const EnumType&> (other);

  if (this->name_token != that.name_token) return false;

  return Type::equals(other);
}

bool StructType::equals(const Type &other) const {
  auto that = static_cast<const StructType&>(other);

  if (name_token != that.name_token) return false;

  if (members.size() != that.members.size()) return false;
  for (size_t i = 0; i < members.size(); i++) {
    if (members[i].name != that.members[i].name) return false;
    if ((*members[i].type.get()) != (*that.members[i].type.get())) return false;
  }

  return Type::equals(other);
}

bool ParameterisedStructType::equals(const Type &other) const {
  auto that = static_cast<const ParameterisedStructType &>(other);

  if (expected_parameters.size() != that.expected_parameters.size()) return false;
  for (size_t i = 0; i < expected_parameters.size(); i++) {
    if (this->expected_parameters[i].name != that.expected_parameters[i].name) return false;
    if ((*this->expected_parameters[i].type.get()) != (*that.expected_parameters[i].type.get())) return false;
  }
  
  return StructType::equals(other);

  // if (other.kind != Type::Kind::STRUCT) return false;

  // auto other_struct = static_cast<const Struct_Type &>(other);

  // if (this->name_token != other_struct.name_token) return false;
  // if (this->members.size() != other_struct.members.size()) return false;

  // for (size_t i = 0; i < members.size(); i++) {
  //   if (this->members[i].name != other_struct.members[i].name) return false;
  //   if (*this->members[i].type != *other_struct.members[i].type) return false;
  // }

  // if (other_struct.struct_kind == Struct_Type::Kind::PARAMETERISED) {
  //   auto other_parameterised_struct = static_cast<const Parameterised_Struct_Type&> (other_struct);
  //   if (this->expected_parameters.size() != other_parameterised_struct.expected_parameters.size()) return false;

  //   for (size_t i = 0; i < expected_parameters.size(); i++) {
  //     if (this->expected_parameters[i].name != other_parameterised_struct.expected_parameters[i].name) return false;
  //     if (*this->expected_parameters[i].type != *other_parameterised_struct.expected_parameters[i].type) return false;
  //   }
  // }
  // else if (other_struct.struct_kind == Struct_Type::Kind::INSTANCE) {
  //   auto other_instance_struct = static_cast<const Struct_Instance_Type&> (other_struct);
  //   if (*this != *other_instance_struct.instance_of) return false;
  // }

  // auto this_struct = static_cast<const Struct_Type&> (*this);
  // return this_struct.equals(other_struct);
}

bool StructInstanceType::equals(const Type &other) const {
  if (typeid(*this) != typeid(other)) return false;

  auto that = static_cast<const StructInstanceType &>(other);
  // if (expected_parameters.size() != that.expected_parameters.size()) return false;
  // for (size_t i = 0; i < expected_parameters.size(); i++) {
  //   if (this->expected_parameters[i].name != that.expected_parameters[i].name) return false;
  //   if (*this->expected_parameters[i].type != *that.expected_parameters[i].type) return false;
  // }
  
  return StructType::equals(other);

  // if (other.kind != Type::Kind::STRUCT) return false;

  // auto other_struct = static_cast<const Struct_Type &>(other);

  // if (this->name_token != other_struct.name_token) return false;
  // if (this->members.size() != other_struct.members.size()) return false;

  // for (size_t i = 0; i < members.size(); i++) {
  //   if (this->members[i].name != other_struct.members[i].name) return false;
  //   if (*this->members[i].type != *other_struct.members[i].type) return false;
  // }

  // if (other_struct.struct_kind == Struct_Type::Kind::INSTANCE) {
  //   // @Todo: check the struct instance arguments
  //   // auto other_struct_instance = dynamic_cast<const Struct_Instance_Type&> (other_struct); // This fails.
  //   // if (this->instance_arguments.size() != other_struct_instance.instance_arguments.size()) return false;

  //   // for (size_t i = 0; i < instance_arguments.size(); i++) {
  //   //   if (*this->instance_arguments[i].type != *other_struct_instance.instance_arguments[i].type) return false;
  //   // }
  // }
  // else if (other_struct.struct_kind == Struct_Type::Kind::PARAMETERISED) {
  //   auto other_parameterised_struct = static_cast<const Parameterised_Struct_Type&> (other_struct);
  //   if (*this->instance_of != other_parameterised_struct) return false;
  // }

  // auto this_struct = static_cast<const Struct_Type&> (*this);
  // return this_struct.equals(other_struct);
}

bool TypeInfoType::equals(const Type &other) const {
  if (typeid(*this) != typeid(other)) return false;

  auto that = static_cast<const TypeInfoType &>(other);
  if (name_token != that.name_token) return false;
  
  return Type::equals(other);
}
