

#pragma once

#include <memory>
#ifndef DRAGON_TYPES_H_
#define DRAGON_TYPES_H_

#include <assert.h>
#include <stdint.h>
#include <string>
#include <vector>
#include <typeinfo>
#include <typeindex>

#include "defines.h"

#include "lexer.h"

struct Type {
  enum class Kind {
    UNRESOLVED = 0,
    BUILTIN,
    POINTER,
    PROCEDURE,
    ARRAY,
    STRUCT,
    ENUM,
    TYPE_INFO,
  };

  Kind kind{ Kind::UNRESOLVED };

  size_t size_in_bytes{0};

  Type(Kind kind) {
    this->kind = kind;
  }

  virtual Ref<Type>    copy() = 0;
  virtual std::string  name() const = 0;

  // virtual ~Type() = 0;

  virtual bool is_unresolved() { return false; }
  virtual bool is_struct()     { return false; }
  virtual bool is_procedure()  { return false; }
  virtual bool is_builtin()    { return false; }
  virtual bool is_pointer()    { return false; }
  virtual bool is_string()     { return false; }
  virtual bool is_array()      { return false; }
  virtual bool is_enum()       { return false; }
  virtual bool is_boxed_enum() { return false; }
  virtual bool is_type_info()  { return false; }
  virtual bool is_void()       { return false; }
  
protected:
  friend bool operator==(const Type& a, const Type& b);
  friend bool operator!=(const Type& a, const Type& b);

  virtual bool equals(const Type &other) const {
    return this->kind == other.kind;
  }
};

struct UnresolvedType : public Type {
  UnresolvedType() : Type(Type::Kind::UNRESOLVED) { }
  static  Ref<Type>    make()          { return std::make_shared<UnresolvedType>(); }
  virtual Ref<Type>    copy() override { return std::make_shared<UnresolvedType> (*this); }
  virtual bool        equals(const Type &other) const override;
  virtual std::string name() const override;

  virtual ~UnresolvedType() = default;

  virtual bool is_unresolved() override { return true; }
};

struct BuiltinType : public Type {
  enum Kind {
    V0ID,

    U64, U32, U16, U8,
    S64, S32, S16, S8,

    F64, F32,

    BOOLEAN,
    STRING,

    COUNT_BUILTIN_TYPES,
  };

  BuiltinType::Kind builtin_kind = BuiltinType::Kind::V0ID;

  BuiltinType(Kind builtin_type_kind) : Type(Type::Kind::BUILTIN) {
    this->builtin_kind = builtin_type_kind;
  }

  static Ref<Type> make(Kind builtin_type_kind) {
    return std::make_shared<BuiltinType>(builtin_type_kind);
  }

  virtual Ref<Type> copy() override {
    return std::make_shared<BuiltinType> (*this);
  }

  virtual bool equals(const Type &other) const override;
  virtual std::string name() const override;

  virtual ~BuiltinType() { }

  virtual bool is_builtin() override { return true; }
  virtual bool is_void() override    { return builtin_kind == BuiltinType::Kind::V0ID; }
  virtual bool is_string() override    { return builtin_kind == BuiltinType::Kind::STRING; }
};

struct PointerType : public Type {
  Ref<Type> pointed = nullptr;

  PointerType(Ref<Type> pointed) : Type(Type::Kind::POINTER) {
    this->pointed = pointed;
  }

  static Ref<Type> make(Ref<Type> pointed) {
    return std::make_shared<PointerType> (pointed->copy());
  }

  virtual Ref<Type> copy() override {
    return std::make_shared<PointerType> (*this);
  }

  virtual bool equals(const Type& other) const override;
  virtual std::string name() const override;

  virtual ~PointerType() { }

  virtual bool is_pointer() override { return true; }
};




//
// Array types.
//


struct ArrayType : public Type {
  Ref<Type> element_type = nullptr;
  size_t element_size = 0;

  size_t count = 0;

  bool is_vector = false;

  ArrayType(size_t count, Ref<Type> element_type, bool is_vector = false) 
  : Type(Type::Kind::ARRAY) 
  , element_type(element_type)
  , element_size(element_type->size_in_bytes)
  , count(count)
  , is_vector(is_vector) {
  }

  static Ref<Type> make(size_t count, Ref<Type> element_type, bool is_vector = false) {
    return std::make_shared<ArrayType>(count, element_type, is_vector);
  }

  virtual Ref<Type> copy() override {
    return std::make_shared<ArrayType> (*this);
  }

  virtual bool equals(const Type &other) const override;
  virtual std::string name() const override;

  virtual ~ArrayType() { }

  virtual bool is_array() override { return true; }
};






//
// Enum types.
//



struct EnumType : public Type {
  struct Member {
    enum class Kind { SIMPLE, BOXED };

    EnumType::Member::Kind kind = EnumType::Member::Kind::SIMPLE;
    Token name_token = {};

    Member() = default;
    Member(Token name_token) : kind(EnumType::Member::Kind::SIMPLE), name_token(name_token) { }
    Member(EnumType::Member::Kind kind, Token name_token) : kind(kind), name_token(name_token) { }

    virtual ~Member() { }
  };

  enum class Kind { SIMPLE, BOXED };

  EnumType::Kind enum_kind = EnumType::Kind::SIMPLE;
  Token name_token = {};

  Vec<Ref<EnumType::Member>> members = {};

  Ref<EnumType::Member> find_member(StringView name) {
    for (auto& member : members) {
      if (member->name_token.contents == name) return member;
    }

    internal_error("{} is not a member of {}.", name, this->name());
    return nullptr;
  }


  EnumType(Token name_token, EnumType::Kind enum_kind, Vec<Ref<EnumType::Member>> members)
  : Type(Type::Kind::ENUM)
  , enum_kind(enum_kind)
  , name_token(name_token)
  , members(members) {
  }

  static Ref<Type> make(Token name_token, EnumType::Kind enum_kind, Vec<Ref<EnumType::Member>> members) {
    return std::make_shared<EnumType> (name_token, enum_kind, members);
  }

  virtual Ref<Type> copy() override {
    return std::make_shared<EnumType> (*this);
  }

  virtual bool equals(const Type& other) const override;
  virtual std::string name() const override;

  virtual ~EnumType() { }

  virtual bool is_enum() override { return true; }
};

struct SimpleEnumType : public EnumType {
  SimpleEnumType(Token name_token, Vec<Ref<EnumType::Member>> members) 
  : EnumType(name_token, EnumType::Kind::SIMPLE, members) {
  }

  static Ref<Type> make(Token name_token, Vec<Ref<EnumType::Member>> members) {
    return std::make_shared<SimpleEnumType>(name_token, members);
  }

  virtual Ref<Type> copy() override {
    return std::make_shared<SimpleEnumType>(*this);
  }
};

struct BoxedEnumType : public EnumType {
  struct Member : public EnumType::Member {
    struct BoxedValue {
      Token     name = {};
      Ref<Type> type = nullptr;
    };

    // Filled out if this member is a boxed member.
    Vec<BoxedValue> values = {};

    Member(Token name, Vec<BoxedValue> values)
    : EnumType::Member(EnumType::Member::Kind::BOXED, name)
    , values(values) {
    }

    Ref<Type> type_of_named_value(StringView name) {
      log_warning("Looking for named value {} in {}", name, name_token.contents);
      for (auto& value : values) {
        if (value.name.contents == name)  return value.type;
      }
      
      return nullptr;
    }
  }; 

  BoxedEnumType(Token name_token, Vec<Ref<EnumType::Member>> members) 
  : EnumType(name_token, EnumType::Kind::BOXED, members) {
  }

  static Ref<Type> make(Token name_token, Vec<Ref<SimpleEnumType::Member>> members) {
    return std::make_shared<BoxedEnumType>(name_token, members);
  }

  virtual Ref<Type> copy() override {
    return std::make_shared<BoxedEnumType>(*this);
  }

  virtual bool is_boxed_enum() override { return true; }
};






//
// Proceudre types
// 

struct ProcedureType : public Type {
  struct Argument {
    Ref<Type> type{ nullptr };
    Token name;

    Argument() = default;

    Argument(Token name, Ref<Type> type) {
      this->name = name;
      this->type = type->copy();
    }
  };

  Ref<Type> return_type{ nullptr };
  Vec<Argument> arguments{ };

  ProcedureType(Vec<Argument> arguments, Ref<Type> return_type) : Type(Type::Kind::PROCEDURE) {
    this->arguments = arguments;
    this->return_type = return_type;
  }

  static Ref<Type> make(Vec<Argument> arguments, Ref<Type> return_type) {
    return std::make_shared<ProcedureType>(arguments, return_type);
  }

  virtual Ref<Type> copy() override {
    return std::make_shared<ProcedureType> (*this);
  }

  virtual bool equals(const Type &other) const override;
  virtual std::string name() const override;

  virtual ~ProcedureType() { }

  virtual bool is_procedure() override { return true; }
};

struct StructType : public Type {
  enum class Kind { BASIC = 0, PARAMETERISED, INSTANCE, };

  struct Member {
    Token name = {};
    Ref<Type> type = nullptr;

    bool refers_to_parameter = false;
    Token referred_parameter_name = {};
    u32 parameter_index = 0;

    Member(Token name, Ref<Type> type)
    : name(name)
    , type(type) {
    }
  };

  StructType::Kind struct_kind = StructType::Kind::BASIC;
  Token name_token = {};
  Vec<Member> members = {};

  StructType(Token name_token, Vec<StructType::Member> members = {})
  : Type(Type::Kind::STRUCT)
  , struct_kind(StructType::Kind::BASIC)
  , name_token(name_token)
  , members(members) {
  }

  static Ref<Type> make(Token name, Vec<StructType::Member> members = {}) {
    return std::make_shared<StructType>(name, members);
  }

  virtual Ref<Type> copy() override {
    return std::make_shared<StructType> (*this);
  }

  virtual bool equals(const Type &other) const override;
  virtual std::string name() const override;

  virtual ~StructType() = default;

  virtual bool is_struct() override { return true; }
};

struct ParameterisedStructType : public StructType {
  struct Parameter {
    Token name = {};
    Ref<Type> type = nullptr;
  };

  Vec<Parameter> expected_parameters{};

  ParameterisedStructType(Token name, Vec<StructType::Member> members = {}, Vec<ParameterisedStructType::Parameter> parameters = {})
  : StructType(name, members) {
    this->struct_kind = StructType::Kind::PARAMETERISED;
    this->expected_parameters = parameters;
  }

  static Ref<Type> make(Token name, Vec<StructType::Member> members = {}, Vec<ParameterisedStructType::Parameter> parameters = {}) {
    return std::make_shared<ParameterisedStructType> (name, members, parameters);
  }

  virtual Ref<Type> copy() override {
    return std::make_shared<ParameterisedStructType> (*this);
  }

  virtual bool equals(const Type& other) const override;
  virtual std::string name() const override;

  virtual ~ParameterisedStructType() { }
};

struct StructInstanceType : public StructType {
  struct Argument {
    Ref<Type> type;
  };

  Ref<Type> instance_of{ nullptr };
  Vec<Argument> instance_arguments{ };

  StructInstanceType(Token name, Ref<Type> instance_of, Vec<StructType::Member> members = {}, Vec<Argument> instance_arguments = {})
  : StructType(name, members) {
    this->struct_kind = StructType::Kind::INSTANCE;
    this->instance_arguments = instance_arguments;
    assert(instance_of->is_struct());
    auto instance_of_struct = std::dynamic_pointer_cast<StructType> (instance_of);
    assert(instance_of_struct->struct_kind == StructType::Kind::PARAMETERISED);
    this->instance_of = instance_of->copy();

    for (size_t i = 0; i < this->members.size(); ++i) {
      if (this->members[i].refers_to_parameter) {
        assert(this->members[i].parameter_index < this->instance_arguments.size());
        this->members[i].type = this->instance_arguments[this->members[i].parameter_index].type->copy();
      }
    }
  }

  static Ref<Type> make(Token name, Ref<ParameterisedStructType> instance_of, Vec<StructType::Member> members = {}, Vec<Argument> instance_arguments = {}) {
    return std::make_shared<StructInstanceType>(name, instance_of, members, instance_arguments);
  }

  virtual Ref<Type> copy() override {
    return std::make_shared<StructInstanceType> (*this);
  }

  virtual bool equals(const Type& other) const override;
  virtual std::string name() const override;

  virtual ~StructInstanceType() { }
};

struct TypeInfoType : public Type {
  Token name_token{};

  TypeInfoType(Token name_token) : Type(Type::Kind::TYPE_INFO) {
    this->name_token = name_token;
  }

  static Ref<Type> make(Token name_token) {
    return std::make_shared<TypeInfoType>(name_token);
  }

  virtual Ref<Type> copy() override {
    return TypeInfoType::make(this->name_token);
  }

  virtual bool equals(const Type &other) const override;
  virtual std::string name() const override;

  virtual ~TypeInfoType() { }

  virtual bool is_type_info() override { return true; }
};

Ref<Type> default_type();

bool is_boolean(Ref<Type> type);
bool is_string(Ref<Type> type);
bool is_floating(Ref<Type> type);
bool is_integral(Ref<Type> type);

template <>
struct fmt::formatter<Ref<Type>> {
  constexpr auto parse(fmt::format_parse_context &ctx) { return ctx.end(); }

  template <typename FormatContext>
  auto format(Ref<Type> type, FormatContext &ctx) {
    auto name = type->name();
    return fmt::format_to(ctx.out(), "{}", name);
  }
};





#endif // DRAGON_TYPES_H_
